<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>

    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="app-url" content="{{ getBaseURL() }}">
    <meta name="file-base-url" content="{{ getFileBaseURL() }}">

    <title>@yield('meta_title')</title>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="robots" content="index, follow">
    <meta name="description" content="" />
    <meta name="keywords" content="">

    @yield('meta')

    @if(!isset($detailedProduct) && !isset($shop) && !isset($page))
    <!-- Schema.org markup for Google+ -->
    <meta itemprop="name" content="{{ config('app.name', 'Laravel') }}">
    <meta itemprop="description" content="">
    <meta itemprop="image" content="">

    <!-- Twitter Card data -->
    <meta name="twitter:card" content="product">
    <meta name="twitter:site" content="@publisher_handle">
    <meta name="twitter:title" content="{{ config('app.name', 'Laravel') }}">
    <meta name="twitter:description" content="">
    <meta name="twitter:creator" content="@author_handle">
    <meta name="twitter:image" content="">

    <!-- Open Graph data -->
    <meta property="og:title" content="{{ config('app.name', 'Laravel') }}" />
    <meta property="og:type" content="website" />
    <meta property="og:url" content="{{ route('home') }}" />
    <meta property="og:image" content="" />
    <meta property="og:description" content="" />
    <meta property="og:site_name" content="{{ env('APP_NAME') }}" />
    <meta property="fb:app_id" content="{{ env('FACEBOOK_PIXEL_ID') }}">
    @endif

    <!-- Favicon -->
    <link rel="icon" href="">

    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i,800,800i&display=swap" rel="stylesheet">

    <!-- CSS Files -->
    <link rel="stylesheet" href="{{ static_asset('css/fontawesome.min.css') }}">

    <link rel="stylesheet" href="{{ static_asset('css/themify-icons.css') }}">

    <link rel="stylesheet" href="{{ static_asset('css/elegant-line-icons.css') }}">

    <link rel="stylesheet" href="{{ static_asset('css/elegant-font-icons.css') }}">

    <link rel="stylesheet" href="{{ static_asset('css/flaticon.css') }}">

    <link rel="stylesheet" href="{{ static_asset('css/animate.min.css') }}">

    <link rel="stylesheet" href="{{ static_asset('css/bootstrap.min.css') }}">

    <link rel="stylesheet" href="{{ static_asset('css/slick.css') }}">

    <link rel="stylesheet" href="{{ static_asset('css/slider.css') }}">

    <link rel="stylesheet" href="{{ static_asset('css/odometer.min.css') }}">

    <link rel="stylesheet" href="{{ static_asset('css/venobox/venobox.css') }}">

    <link rel="stylesheet" href="{{ static_asset('css/owl.carousel.css') }}">

    <link rel="stylesheet" href="{{ static_asset('css/main.css') }}">

    <link rel="stylesheet" href="{{ static_asset('css/responsive.css') }}">
    <script src="{{ static_asset('js/vendor/modernizr-2.8.3-respond-1.4.2.min.js') }}"></script>
    <link rel="stylesheet" href="{{ static_asset('assets/css/custom-style.css') }}">

    <script type="text/javascript">
        var base_url = "{!! url('') !!}/";
    </script>

</head>
<body>
    <!--[if lt IE 8]>
        <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
    <![endif]-->
    <div class="site-preloader-wrap">
        <div class="spinner"></div>
    </div>
    @include('frontend.inc.header')
    @yield('content')
    @include('frontend.inc.footer')

    @yield('modal')

    <!-- SCRIPTS -->
    <script src="{{ static_asset('js/vendor/jquery-1.12.4.min.js') }}"></script>

    <script src="{{ static_asset('js/vendor/bootstrap.min.js') }}"></script>

    <script src="{{ static_asset('js/vendor/tether.min.js') }}"></script>

    <script src="{{ static_asset('js/vendor/headroom.min.js') }}"></script>

    <script src="{{ static_asset('js/vendor/owl.carousel.min.js') }}"></script>

    <script src="{{ static_asset('js/vendor/smooth-scroll.min.js') }}"></script>

    <script src="{{ static_asset('js/vendor/venobox.min.js') }}"></script>

    <script src="{{ static_asset('js/vendor/jquery.ajaxchimp.min.js') }}"></script>

    <script src="{{ static_asset('js/vendor/slick.min.js') }}"></script>

    <script src="{{ static_asset('js/vendor/waypoints.min.js') }}"></script>

    <script src="{{ static_asset('js/vendor/odometer.min.js') }}"></script>

    <script src="{{ static_asset('js/vendor/wow.min.js') }}"></script>

    <script src="{{ static_asset('js/main.js') }}"></script>
    @yield('script')

</body>
</html>
