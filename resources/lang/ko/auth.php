<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'failed' => '이 자격 증명은 우리의 기록과 일치하지 않습니다.',
    'throttle' => '로그인 시도가 너무 많습니다. 초 후에 다시 시도하십시오.',
    'register_success'  =>  '회원가입에 완료되었습니다. 이 계정으로 로그인 해주세요.',
    'token_invalid' =>  "사용할 수 없는 토큰입니다.",
    'token_expired' =>  "만료된 토큰입니다.",
    'token_not_found'   =>  "토큰을 찾을 수 없습니다.",
    'unauthorized'  =>  "이메일 또는 비밀번호가 올바르지 않습니다.",
    'change_password_success'   =>  "비밀번호 변경이 완료되었습니다.",
    'verify_account'   =>  "회원님의 계정을 인증해 주세요.",
    'success_logout'    =>  "성공적으로 로그 아웃되었습니다.",
    'not_register'  =>  "등록되어 있지 않습니다",
    'current_password_not_correct'  =>  "현재 입력한 비밀번호가 정확하지 않습니다.",
    'phone_not_register'    =>  "전화번호가 등록되어 있지 않습니다.",
    'phone_register'    =>  "전화번호가 등록되었습니다.",
    'user_deactive' =>  "이 회원은 비활동 상태로 변경되었습니다..",
    'email_not_register'    =>  "이메일이 등록되어 있지 않습니다.",
    'email_register'    =>  "이메일이 등록되었습니다.",
    'sns_not_register'    =>  "SNS가 등록되어 있지 않습니다.",
    'sns_register'    =>  "SNS가 등록되었습니다.",
    'update_success'    =>  "업데이트 되었습니다.",
    'invalid_email_password' =>  "잘못된 이메일 또는 비밀번호"

];
