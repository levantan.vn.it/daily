<?php

return [
    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */
    'noti' => '알림관리',
    'add' => '새 알림 추가',
    'id' => '알림 ID',
    'img' => '이미지',
    'title' => '타이틀',
    'type' => '알림 유형 ',
    'created_at' => '생성일',
    'status' => '상태',
    'search_place' => '알림 ID',
    // 'search_place' => '알림 ID / 알림 제목 검색',
    'btn_search' => '검색',
    'btn_reset' => '초기화',
    'view' => '조회',
    'send_noti' => '알림 보내기',
    'delete' => '삭제',
    'btn_cancel' => '취소',
    'btn_save' => '저장',

    'created' => '생성일',
    'active' => '액티브',
    'inactive' => '비활성',
    'noti-img' => '알림 이미지 ',
    'noti-des' => '알림 내용 ',
    'noti-push' => '알림 추가',
    'noti-information' => '알림 정보 추가',
    'noti-images' => '알림 이미지 추가',
    'noti-content' => '알림 내용 추가',
    'noti_id' => '알림 ID',
    'noti_title' => '알림 타이틀 ',
    'delete-noti'=> '알림 삭제',
    'change-status'=>'상태 알림 변경',
    'noti_push_success' => '푸시 알림이 추가되었습니다.',
    'msg_descript_success' => '알림 설명이 업데이트 되었습니다.',
    'msg_infor_success' => '알림 내용이 업데이트 되었습니다.',
    'msg_status_success' => '알림 상태가 변경되었습니다.',
    'msg_delete_success' => '알림이 삭제되었습니다.',
    'msg_push_success' => '푸시 알림이 추가되었습니다.',
    'select_type' => "알림 타입 선택",
    'announcement' => '공지',
    'promotion' => '프로모션',
    'system' => '시스템',
    'popup_status' => '알림 상태 변경',
    'confirm_popup' => '계속 하시겠습니까?',
    'yes' => '예',
    'no' => '아니오',
    'update_image' => '알림 이미지를 업데이트 하였습니다.',
    'noti_detail' => '알림 상세',
];
