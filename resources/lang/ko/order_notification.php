<?php

return [
    'unpaid' => [
        'title' => '주문(#:ORDER_ID)이 완료 되었습니다.',
        'content' => '주문(#:ORDER_ID)이 완료 되었습니다. 자세한 내용은 주문 상세 정보를 확인해 주세요.'
    ],
    'paid' => [
        'title' => '주문(#:ORDER_ID)이 결제 되었습니다.',
        'content' => '주문(#:ORDER_ID)이 결제 되었습니다. 자세한 내용은 주문 상세 정보를 확인해 주세요.'
    ],
    'delivering' => [
        'title' => '주문(#:ORDER_ID)이 배송중입니다.',
        'content' => '주문(#:ORDER_ID)은 [:DELIVERY_NAME]로 배송중입니다. 배송 ID(#:DELIVERY_ID). 자세한 내용은 주문 상세 정보를 확인해 주세요.'
    ],
    'arrived' => [
        'title' => '주문(#:ORDER_ID)이 배송완료 되었습니다.',
        'content' => '주문(#:ORDER_ID)은 [:DELIVERY_NAME]로 배송완료 되었습니다. 배송 ID(#:DELIVERY_ID). 자세한 내용은 주문 상세 정보를 확인해 주세요.'
    ],
    'cancelled' => [
        'title' => '주문(#:ORDER_ID)이 취소 되었습니다.',
        'content' => '취소된 주문(#:ORDER_ID)에 대해 죄송하다는 말씀 드립니다. 이 제품은 재고가 없거나 다른 이유로 인해 배송할 수 없습니다. 결제된 금액은 며칠 내로  환불 처리 될 예정입니다.'
    ],
    'refund' => [
        'title' => 'Your order :ORDER_ID was refunded',
        'content' => 'Your order :ORDER_ID was refunded'
    ]
];
