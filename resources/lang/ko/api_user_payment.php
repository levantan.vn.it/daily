<?php

return [
  'make_primary' => '주 결제수단으로 결제하는 데 실패했습니다.',
  'add_failed' => '결제수단 추가에 실패했습니다.',
  'update_failed' => '결제수단 업데이트에 실패했습니다.',
  'delete_failed' => '결제수단 삭제에 실패했습니다.'
];
