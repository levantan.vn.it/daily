<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */
    'transaction' => '결제 내역',
    'tran_search' => "거래 ID / 주문 ID",

    'id' => "거래 ID",
    'date' => "데이트",
    'us_id' => "회원 ID",
    'od_id' => "주문 ID",
    'money' => "금액",
    'method'  =>  "결제수단",
    'status'  =>  "상태",
    'view'  =>  "조회",
    'active'  =>  "액티브",
    'del'  =>  "삭제",



];
