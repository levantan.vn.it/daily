<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */
    'dashboard' => 'DASHBOARD',
    'day' => "Day",
    'week' => "Week",
    'month' => "Month",
    'db_revenue' => "Total Revenue",
    'db_order' => "Total Order",
    'total'  =>  "Total",
    'price' => '원',
    'success' => 'Success',
    'sold' => 'Sold',
    't_product'  =>  "Top Products",
    't_order'  =>  "Top Orders",
    't_order'  =>  "Top Orders",
    'latest'  =>  "Latest Transactions",
    'view'  =>  "View all",
    'active'  =>  "Active",
    'del'  =>  "Delete",
    'nodata_trans' => 'There is no recent payment history',
    'nodata_order' => 'There is no recent top order',
    'nodata_product' => 'There is no recent top product',
    'number_revenue' => 'Number Revenue',
    'number_order' => 'Number Order',


];
