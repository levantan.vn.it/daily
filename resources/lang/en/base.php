<?php
return [
    'create_success'  => ":entity has been inserted successfully",
    'update_sucess'    =>  ":entity has been updated successfully",
    'delete_success'    =>  ":entity has been deleted successfully",
    'active_success'    =>  ":entity has been active successfully",
    'deactive_success'    =>  ":entity has been deactive successfully",
    'error' =>  "Have error. Please try again",

];

