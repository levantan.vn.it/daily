<?php

return [

    // index
    'title' => 'Upload file management',
    'title_create' => 'Upload new file',
    'all_upload' => 'All uploaded files',
    'all_file'  =>  "All files",
    'upload_new'  =>  "Upload New File",
    'sort_newest'  =>  "Sort by newest",
    'sort_oldest'  =>  "Sort by oldest",
    'sort_smallest'  =>  "Sort by smallest",
    'sort_largest'  =>  "Sort by largest",
    'search_file'  =>  "Search your files",
    'search'  =>  "Search",
    'unknown'  =>  "Unknown",
    'detail_info'  =>  "Details Info",
    'down'  =>  "Download",
    'copy_link'  =>  "Copy Link",
    'delete'  =>  "Delete",
    'confirm_delete'  =>  "Delete Confirmation",
    'question_delete'  =>  "Are you sure to delete this file?",
    'cancel'  =>  "Cancel",
    'file_info'  =>  "File Info",
    'link_copy'  =>  "Link copied to clipboard",
    'oops'  =>  "Oops, unable to copy",

    // create
    'back'  =>  "Back to uploaded files",
    'drag_file'  =>  "Drag & drop your files",

    // info
    'file_name'  =>  "File Name",
    'file_type'  =>  "File Type",
    'file_size'  =>  "File Size",
    'upload_at'  =>  "Uploaded At",

    //notification
    'del_success'  =>  "File deleted successfully",
    'opp_wrong'  =>  "Opp! Something went wrong.",

    'nothing_found' => 'Nothing Found',
];
