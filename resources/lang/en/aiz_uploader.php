
<?php

return [

    'select_file' => 'Select File',
    'add'  =>  "Add Files",
    'upload_new'  =>  "Upload New",
    'sort_newest'  =>  "Sort by newest",
    'sort_oldest'  =>  "Sort by oldest",
    'sort_smallest'  =>  "Sort by smallest",
    'sort_largest'  =>  "Sort by largest",
    'selected_only'  =>  "Selected Only",
    'search_file'  =>  "Search your files",
    'files_found'  =>  "No files found",
    'file_selected'  =>  "File selected",
    'clear'  =>  "Clear",
    'prev'  =>  "Prev",
    'next'  =>  "Next",
    'image' => 'Image',
    

];
