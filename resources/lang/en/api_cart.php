<?php

return [
  'not_qty_in_stock' => 'There are not enough products in stock.',
  'product_unavailable' => 'The product is not available for purchase.',
  'sold_out' => 'This product is sold out.'
];
