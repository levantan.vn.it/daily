<?php

return [
  'validation' => [
    'card_number_exist' => 'The card number has already been taken.'
  ]
];
