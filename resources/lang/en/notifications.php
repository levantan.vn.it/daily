<?php

return [
    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */
    'noti' => 'NOTIFICATIONS',
    'add' => 'ADD NEW NOTIFICATION',
    'id' => 'Notification ID',
    'img' => 'Images',
    'title' => 'Title',
    'type' => 'Type',
    'created_at' => 'Created At',
    'status' => 'Status',
    'search_place' => 'Notification ID / Notification Title',
    'search_place' => 'Notification ID',
    'btn_search' => 'SEARCH',
    'btn_reset' => 'RESET',
    'view' => 'View',
    'send_noti' => 'Send Notification',
    'delete' => 'Delete',
    'btn_cancel' => 'Cancel',
    'btn_save' => 'Save',

    'created' => 'Created Date',
    'active' => 'Active',
    'inactive' => 'InActive',
    'noti-img' => 'Notification Images',
    'noti-des' => 'Notification Description',
    'noti-push' => 'Push Notification',
    'noti-information' => 'Add Notification Information',
    'noti-images' => 'Add Notification Images',
    'noti-content' => 'Add Notification Content',
    'noti_id' => 'Notification ID',
    'noti_title' => 'Notification Title ',
    'delete-noti'=> 'Delete Notification',
    'change-status'=>'Change status notification',
    'noti_push_success' => 'Create Notification Success',
    'msg_descript_success' => 'Update Notifications Description Success',
    'msg_infor_success' => 'Update Notifications Information Success',
    'msg_status_success' => 'Change Notification Status Success',
    'msg_delete_success' => 'Delete Notification Success',
    'msg_push_success' => 'Push Notification Success',
    'popup_status' => 'Change status notification',
    'confirm_popup' => 'Do you want to continue',
    'yes' => 'Yes',
    'no' => 'No',
    'select_type' => "Select Type",
    'announcement' => 'Announcement',
    'promotion' => 'Promotion',
    'system' => 'System',
    'update_image' => 'The notification image has been updated.',
    'noti_detail' => 'Notification Detail',



];
