-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: Mar 16, 2021 at 06:39 AM
-- Server version: 8.0.19
-- PHP Version: 7.3.17

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `avocadoo`
--

-- --------------------------------------------------------

--
-- Table structure for table `banners`
--

CREATE TABLE `banners` (
  `id` int NOT NULL,
  `position` int NOT NULL DEFAULT '1',
  `start_date` date NOT NULL,
  `end_date` date NOT NULL,
  `title` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `status` int NOT NULL DEFAULT '1',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `banners`
--

INSERT INTO `banners` (`id`, `position`, `start_date`, `end_date`, `title`, `status`, `created_at`, `updated_at`) VALUES
(1, 2, '2021-03-09', '2021-03-30', 'Banner 1', 2, '2021-03-09 01:59:09', '2021-03-09 01:59:09'),
(2, 2, '2021-03-09', '2021-03-30', 'Banner 2', 2, '2021-03-09 01:59:09', '2021-03-09 01:59:09'),
(3, 2, '2021-03-09', '2021-03-30', 'Banner 3', 2, '2021-03-09 01:59:09', '2021-03-09 01:59:09'),
(4, 1, '2021-03-09', '2021-03-30', 'Banner 4', 2, '2021-03-09 01:59:09', '2021-03-09 01:59:09'),
(5, 2, '2021-03-09', '2021-03-30', 'Banner 5', 2, '2021-03-09 01:59:09', '2021-03-09 01:59:09'),
(6, 2, '2021-03-09', '2021-03-30', 'Banner 6', 2, '2021-03-09 01:59:09', '2021-03-09 01:59:09'),
(7, 3, '2021-03-09', '2021-03-30', 'Banner 7', 2, '2021-03-09 01:59:09', '2021-03-09 01:59:09'),
(8, 1, '2021-03-09', '2021-03-30', 'Banner 8', 2, '2021-03-09 01:59:09', '2021-03-09 01:59:09'),
(9, 3, '2021-03-09', '2021-03-30', 'Banner 9', 2, '2021-03-09 01:59:09', '2021-03-09 01:59:09');

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
  `id` bigint NOT NULL,
  `title` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `type` enum('1','2') COLLATE utf8_unicode_ci NOT NULL DEFAULT '1',
  `parent_id` int NOT NULL DEFAULT '0',
  `image` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`id`, `title`, `type`, `parent_id`, `image`, `updated_at`) VALUES
(1, 'Category 1', '1', 0, 'uploads/all/AJZpoShWAV3Ho2FWqIZxzuadxGNhWjxazmkEhL53.png', '2021-03-09 01:20:13'),
(2, 'Category 2', '1', 0, 'uploads/all/oknrbyio2c520s8YsFfcA02THs5edNTyd7HuApaf.png', '2021-03-09 01:20:13'),
(3, 'Category 3', '1', 0, 'uploads/all/L37mVULWRqsyaW0OruURxw4mmDV2sAokyFBJ4BoS.png', '2021-03-09 01:20:13'),
(4, 'Category 4', '1', 0, 'uploads/all/7YuyL6Hv9ouIMcxL5jf0WPZC9ncd9c3IwKW8ikzI.png', '2021-03-09 01:20:13'),
(5, 'Category 5', '2', 3, 'uploads/all/AJZpoShWAV3Ho2FWqIZxzuadxGNhWjxazmkEhL53.png', '2021-03-09 01:20:13'),
(6, 'Category 6', '2', 1, 'uploads/all/1AltaWzbe42mqSIAtKPFNSJvNXpGXTv4p4TV9vl9.png', '2021-03-09 01:20:13'),
(7, 'Category 7', '2', 3, 'uploads/all/4uD1E1B8D0WJrTSCny6ogAeJKjd8FY6cLsv39OpJ.png', '2021-03-09 01:20:14'),
(8, 'Category 8', '2', 4, 'uploads/all/7YuyL6Hv9ouIMcxL5jf0WPZC9ncd9c3IwKW8ikzI.png', '2021-03-09 01:20:14'),
(9, 'Category 9', '2', 1, 'uploads/all/HreldpMpglaz1pF2skU66aIRa0XOB3JfmIY90Hhf.png', '2021-03-09 01:20:14');

-- --------------------------------------------------------

--
-- Table structure for table `collections`
--

CREATE TABLE `collections` (
  `id` bigint NOT NULL,
  `name` varchar(100) COLLATE utf8_bin NOT NULL,
  `section` enum('1','2','3') COLLATE utf8_bin NOT NULL DEFAULT '1',
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Table structure for table `collection_products`
--

CREATE TABLE `collection_products` (
  `id` bigint NOT NULL,
  `product_id` bigint NOT NULL,
  `collection_id` bigint NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Table structure for table `consignment_products`
--

CREATE TABLE `consignment_products` (
  `id` bigint NOT NULL,
  `product_id` bigint NOT NULL,
  `sku` varchar(100) COLLATE utf8_bin NOT NULL,
  `qty` int NOT NULL DEFAULT '0',
  `sales` int NOT NULL DEFAULT '0',
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Table structure for table `delivery_books`
--

CREATE TABLE `delivery_books` (
  `id` int NOT NULL,
  `location_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `user_id` bigint UNSIGNED NOT NULL,
  `address` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `receiver` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `phone` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `type` enum('1','0') COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `entity_images`
--

CREATE TABLE `entity_images` (
  `id` bigint NOT NULL,
  `file_name` varchar(255) COLLATE utf8_bin NOT NULL,
  `file_original_name` varchar(255) COLLATE utf8_bin NOT NULL,
  `file_size` int NOT NULL,
  `extension` varchar(5) COLLATE utf8_bin NOT NULL,
  `type` varchar(100) COLLATE utf8_bin NOT NULL,
  `entity_type` varchar(100) COLLATE utf8_bin NOT NULL,
  `entity_id` bigint NOT NULL,
  `zone` varchar(100) COLLATE utf8_bin NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `entity_images`
--

INSERT INTO `entity_images` (`id`, `file_name`, `file_original_name`, `file_size`, `extension`, `type`, `entity_type`, `entity_id`, `zone`, `created_at`, `updated_at`) VALUES
(1, 'uploads/all/7YuyL6Hv9ouIMcxL5jf0WPZC9ncd9c3IwKW8ikzI.png', 'img_grape', 19243, 'png', 'image', 'App/Banner', 1, 'banner', '2021-03-09 08:59:09', '2021-03-15 02:24:16'),
(2, 'uploads/all/7YuyL6Hv9ouIMcxL5jf0WPZC9ncd9c3IwKW8ikzI.png', 'img_grape', 19243, 'png', 'image', 'App/Banner', 2, 'banner', '2021-03-09 08:59:09', '2021-03-15 02:24:16'),
(3, 'uploads/all/L37mVULWRqsyaW0OruURxw4mmDV2sAokyFBJ4BoS.png', 'img_deal_mango', 24515, 'png', 'image', 'App/Banner', 3, 'banner', '2021-03-09 08:59:09', '2021-03-15 02:24:16'),
(4, 'uploads/all/U4ToWDgGaM4UDeBFBwoi6xzhoCN2ZZd1qeBa46qW.png', 'img_peach', 16684, 'png', 'image', 'App/Banner', 4, 'banner', '2021-03-09 08:59:09', '2021-03-15 02:24:16'),
(5, 'uploads/all/U4ToWDgGaM4UDeBFBwoi6xzhoCN2ZZd1qeBa46qW.png', 'img_peach', 16684, 'png', 'image', 'App/Banner', 5, 'banner', '2021-03-09 08:59:09', '2021-03-15 02:24:16'),
(6, 'uploads/all/7YuyL6Hv9ouIMcxL5jf0WPZC9ncd9c3IwKW8ikzI.png', 'img_grape', 19243, 'png', 'image', 'App/Banner', 6, 'banner', '2021-03-09 08:59:09', '2021-03-15 02:24:16'),
(7, 'uploads/all/oknrbyio2c520s8YsFfcA02THs5edNTyd7HuApaf.png', 'img_small_apple', 2699, 'png', 'image', 'App/Banner', 7, 'banner', '2021-03-09 08:59:09', '2021-03-15 02:24:16'),
(8, 'uploads/all/U4ToWDgGaM4UDeBFBwoi6xzhoCN2ZZd1qeBa46qW.png', 'img_peach', 16684, 'png', 'image', 'App/Banner', 8, 'banner', '2021-03-09 08:59:09', '2021-03-15 02:24:16'),
(9, 'uploads/all/U4ToWDgGaM4UDeBFBwoi6xzhoCN2ZZd1qeBa46qW.png', 'img_peach', 16684, 'png', 'image', 'App/Banner', 9, 'banner', '2021-03-09 08:59:10', '2021-03-15 02:24:16'),
(10, 'uploads/all/5CM9gVA8sFJVur80L41jYEz53QVEK9wRlCr10Rlv.png', 'img_mango', 19191, 'png', 'image', 'App/Product', 1, 'image', '2021-03-12 07:22:20', '2021-03-15 02:24:16'),
(11, 'uploads/all/HreldpMpglaz1pF2skU66aIRa0XOB3JfmIY90Hhf.png', 'img_deal_blueberry', 26935, 'png', 'image', 'App/Product', 1, 'image', '2021-03-12 07:22:20', '2021-03-15 02:24:16'),
(12, 'uploads/all/4uD1E1B8D0WJrTSCny6ogAeJKjd8FY6cLsv39OpJ.png', 'img_banana', 9667, 'png', 'image', 'App/Product', 1, 'image', '2021-03-12 07:22:20', '2021-03-15 02:24:16'),
(13, 'uploads/all/AJZpoShWAV3Ho2FWqIZxzuadxGNhWjxazmkEhL53.png', 'img_logo', 12234, 'png', 'image', 'App/Product', 2, 'image', '2021-03-12 07:22:20', '2021-03-15 02:24:16'),
(14, 'uploads/all/5CM9gVA8sFJVur80L41jYEz53QVEK9wRlCr10Rlv.png', 'img_mango', 19191, 'png', 'image', 'App/Product', 2, 'image', '2021-03-12 07:22:20', '2021-03-15 02:24:16'),
(15, 'uploads/all/oknrbyio2c520s8YsFfcA02THs5edNTyd7HuApaf.png', 'img_small_apple', 2699, 'png', 'image', 'App/Product', 2, 'image', '2021-03-12 07:22:20', '2021-03-15 02:24:16'),
(16, 'uploads/all/oknrbyio2c520s8YsFfcA02THs5edNTyd7HuApaf.png', 'img_small_apple', 2699, 'png', 'image', 'App/Product', 3, 'image', '2021-03-12 07:22:20', '2021-03-15 02:24:16'),
(17, 'uploads/all/wCeMnIMQRXBpUr47KpQPDfXss6KiO9IEZxi0qsvE.png', 'img_orange', 14040, 'png', 'image', 'App/Product', 3, 'image', '2021-03-12 07:22:20', '2021-03-15 02:24:16'),
(18, 'uploads/all/7YuyL6Hv9ouIMcxL5jf0WPZC9ncd9c3IwKW8ikzI.png', 'img_grape', 19243, 'png', 'image', 'App/Product', 3, 'image', '2021-03-12 07:22:20', '2021-03-15 02:24:16'),
(19, 'uploads/all/5CM9gVA8sFJVur80L41jYEz53QVEK9wRlCr10Rlv.png', 'img_mango', 19191, 'png', 'image', 'App/Product', 4, 'image', '2021-03-12 07:22:20', '2021-03-15 02:24:16'),
(20, 'uploads/all/L37mVULWRqsyaW0OruURxw4mmDV2sAokyFBJ4BoS.png', 'img_deal_mango', 24515, 'png', 'image', 'App/Product', 4, 'image', '2021-03-12 07:22:20', '2021-03-15 02:24:16'),
(21, 'uploads/all/oknrbyio2c520s8YsFfcA02THs5edNTyd7HuApaf.png', 'img_small_apple', 2699, 'png', 'image', 'App/Product', 4, 'image', '2021-03-12 07:22:20', '2021-03-15 02:24:16'),
(22, 'uploads/all/4uD1E1B8D0WJrTSCny6ogAeJKjd8FY6cLsv39OpJ.png', 'img_banana', 9667, 'png', 'image', 'App/Product', 5, 'image', '2021-03-12 07:22:20', '2021-03-15 02:24:16'),
(23, 'uploads/business_document/KKoJjzkUj50qyX9evpYxwUTU94qpXqY5VIgtPT71.docx', 'Le_Van_Tan', 42422, 'docx', 'document', 'App/Product', 5, 'image', '2021-03-12 07:22:20', '2021-03-15 02:24:16'),
(24, 'uploads/all/L37mVULWRqsyaW0OruURxw4mmDV2sAokyFBJ4BoS.png', 'img_deal_mango', 24515, 'png', 'image', 'App/Product', 5, 'image', '2021-03-12 07:22:20', '2021-03-15 02:24:16'),
(25, 'uploads/all/AJZpoShWAV3Ho2FWqIZxzuadxGNhWjxazmkEhL53.png', 'img_logo', 12234, 'png', 'image', 'App/Product', 6, 'image', '2021-03-12 07:22:20', '2021-03-15 02:24:16'),
(26, 'uploads/all/5CM9gVA8sFJVur80L41jYEz53QVEK9wRlCr10Rlv.png', 'img_mango', 19191, 'png', 'image', 'App/Product', 6, 'image', '2021-03-12 07:22:20', '2021-03-15 02:24:16'),
(27, 'uploads/all/U4ToWDgGaM4UDeBFBwoi6xzhoCN2ZZd1qeBa46qW.png', 'img_peach', 16684, 'png', 'image', 'App/Product', 6, 'image', '2021-03-12 07:22:20', '2021-03-15 02:24:16'),
(28, 'uploads/all/AJZpoShWAV3Ho2FWqIZxzuadxGNhWjxazmkEhL53.png', 'img_logo', 12234, 'png', 'image', 'App/Product', 7, 'image', '2021-03-12 07:22:21', '2021-03-15 02:24:17'),
(29, 'uploads/all/5CM9gVA8sFJVur80L41jYEz53QVEK9wRlCr10Rlv.png', 'img_mango', 19191, 'png', 'image', 'App/Product', 7, 'image', '2021-03-12 07:22:21', '2021-03-15 02:24:17'),
(30, 'uploads/all/HreldpMpglaz1pF2skU66aIRa0XOB3JfmIY90Hhf.png', 'img_deal_blueberry', 26935, 'png', 'image', 'App/Product', 7, 'image', '2021-03-12 07:22:21', '2021-03-15 02:24:17'),
(31, 'uploads/all/L37mVULWRqsyaW0OruURxw4mmDV2sAokyFBJ4BoS.png', 'img_deal_mango', 24515, 'png', 'image', 'App/Product', 8, 'image', '2021-03-12 07:22:21', '2021-03-15 02:24:17'),
(32, 'uploads/all/AJZpoShWAV3Ho2FWqIZxzuadxGNhWjxazmkEhL53.png', 'img_logo', 12234, 'png', 'image', 'App/Product', 8, 'image', '2021-03-12 07:22:21', '2021-03-15 02:24:17'),
(33, 'uploads/all/U4ToWDgGaM4UDeBFBwoi6xzhoCN2ZZd1qeBa46qW.png', 'img_peach', 16684, 'png', 'image', 'App/Product', 8, 'image', '2021-03-12 07:22:21', '2021-03-15 02:24:17'),
(34, 'uploads/all/5CM9gVA8sFJVur80L41jYEz53QVEK9wRlCr10Rlv.png', 'img_mango', 19191, 'png', 'image', 'App/Product', 9, 'image', '2021-03-12 07:22:21', '2021-03-15 02:24:17'),
(35, 'uploads/all/oknrbyio2c520s8YsFfcA02THs5edNTyd7HuApaf.png', 'img_small_apple', 2699, 'png', 'image', 'App/Product', 9, 'image', '2021-03-12 07:22:21', '2021-03-15 02:24:17'),
(36, 'uploads/all/HreldpMpglaz1pF2skU66aIRa0XOB3JfmIY90Hhf.png', 'img_deal_blueberry', 26935, 'png', 'image', 'App/Product', 9, 'image', '2021-03-12 07:22:21', '2021-03-15 02:24:17'),
(37, 'uploads/business_document/aMnWaPm7Tht1Aae7dYaj0WeHQuVZgxExlcMC2dfR.docx', 'Le_Van_Tan', 42422, 'docx', 'document', 'App/Product', 1, 'image', '2021-03-12 08:25:32', '2021-03-15 02:24:17'),
(38, 'uploads/all/7YuyL6Hv9ouIMcxL5jf0WPZC9ncd9c3IwKW8ikzI.png', 'img_grape', 19243, 'png', 'image', 'App/Product', 1, 'image', '2021-03-12 08:25:32', '2021-03-15 02:24:17'),
(39, 'uploads/business_document/KKoJjzkUj50qyX9evpYxwUTU94qpXqY5VIgtPT71.docx', 'Le_Van_Tan', 42422, 'docx', 'document', 'App/Product', 1, 'image', '2021-03-12 08:25:32', '2021-03-15 02:24:17'),
(40, 'uploads/all/7YuyL6Hv9ouIMcxL5jf0WPZC9ncd9c3IwKW8ikzI.png', 'img_grape', 19243, 'png', 'image', 'App/Product', 2, 'image', '2021-03-12 08:25:32', '2021-03-15 02:24:17'),
(41, 'uploads/business_document/aMnWaPm7Tht1Aae7dYaj0WeHQuVZgxExlcMC2dfR.docx', 'Le_Van_Tan', 42422, 'docx', 'document', 'App/Product', 2, 'image', '2021-03-12 08:25:32', '2021-03-15 02:24:17'),
(42, 'uploads/all/wCeMnIMQRXBpUr47KpQPDfXss6KiO9IEZxi0qsvE.png', 'img_orange', 14040, 'png', 'image', 'App/Product', 2, 'image', '2021-03-12 08:25:32', '2021-03-15 02:24:17'),
(43, 'uploads/business_document/KKoJjzkUj50qyX9evpYxwUTU94qpXqY5VIgtPT71.docx', 'Le_Van_Tan', 42422, 'docx', 'document', 'App/Product', 3, 'image', '2021-03-12 08:25:32', '2021-03-15 02:24:17'),
(44, 'uploads/all/oknrbyio2c520s8YsFfcA02THs5edNTyd7HuApaf.png', 'img_small_apple', 2699, 'png', 'image', 'App/Product', 3, 'image', '2021-03-12 08:25:32', '2021-03-15 02:24:17'),
(45, 'uploads/all/4uD1E1B8D0WJrTSCny6ogAeJKjd8FY6cLsv39OpJ.png', 'img_banana', 9667, 'png', 'image', 'App/Product', 3, 'image', '2021-03-12 08:25:32', '2021-03-15 02:24:17'),
(46, 'uploads/all/5CM9gVA8sFJVur80L41jYEz53QVEK9wRlCr10Rlv.png', 'img_mango', 19191, 'png', 'image', 'App/Product', 4, 'image', '2021-03-12 08:25:32', '2021-03-15 02:24:17'),
(47, 'uploads/all/U4ToWDgGaM4UDeBFBwoi6xzhoCN2ZZd1qeBa46qW.png', 'img_peach', 16684, 'png', 'image', 'App/Product', 4, 'image', '2021-03-12 08:25:32', '2021-03-15 02:24:17'),
(48, 'uploads/business_document/aMnWaPm7Tht1Aae7dYaj0WeHQuVZgxExlcMC2dfR.docx', 'Le_Van_Tan', 42422, 'docx', 'document', 'App/Product', 4, 'image', '2021-03-12 08:25:32', '2021-03-15 02:24:17'),
(49, 'uploads/all/7YuyL6Hv9ouIMcxL5jf0WPZC9ncd9c3IwKW8ikzI.png', 'img_grape', 19243, 'png', 'image', 'App/Product', 5, 'image', '2021-03-12 08:25:33', '2021-03-15 02:24:17'),
(50, 'uploads/all/4uD1E1B8D0WJrTSCny6ogAeJKjd8FY6cLsv39OpJ.png', 'img_banana', 9667, 'png', 'image', 'App/Product', 5, 'image', '2021-03-12 08:25:33', '2021-03-15 02:24:17'),
(51, 'uploads/all/wCeMnIMQRXBpUr47KpQPDfXss6KiO9IEZxi0qsvE.png', 'img_orange', 14040, 'png', 'image', 'App/Product', 5, 'image', '2021-03-12 08:25:33', '2021-03-15 02:24:17'),
(52, 'uploads/all/1AltaWzbe42mqSIAtKPFNSJvNXpGXTv4p4TV9vl9.png', 'img_avocado_home', 8118, 'png', 'image', 'App/Product', 6, 'image', '2021-03-12 08:25:33', '2021-03-15 02:24:17'),
(53, 'uploads/business_document/aMnWaPm7Tht1Aae7dYaj0WeHQuVZgxExlcMC2dfR.docx', 'Le_Van_Tan', 42422, 'docx', 'document', 'App/Product', 6, 'image', '2021-03-12 08:25:33', '2021-03-15 02:24:17'),
(54, 'uploads/all/L37mVULWRqsyaW0OruURxw4mmDV2sAokyFBJ4BoS.png', 'img_deal_mango', 24515, 'png', 'image', 'App/Product', 6, 'image', '2021-03-12 08:25:33', '2021-03-15 02:24:17'),
(55, 'uploads/all/wCeMnIMQRXBpUr47KpQPDfXss6KiO9IEZxi0qsvE.png', 'img_orange', 14040, 'png', 'image', 'App/Product', 7, 'image', '2021-03-12 08:25:33', '2021-03-15 02:24:17'),
(56, 'uploads/all/5CM9gVA8sFJVur80L41jYEz53QVEK9wRlCr10Rlv.png', 'img_mango', 19191, 'png', 'image', 'App/Product', 7, 'image', '2021-03-12 08:25:33', '2021-03-15 02:24:17'),
(57, 'uploads/all/1AltaWzbe42mqSIAtKPFNSJvNXpGXTv4p4TV9vl9.png', 'img_avocado_home', 8118, 'png', 'image', 'App/Product', 7, 'image', '2021-03-12 08:25:33', '2021-03-15 02:24:17'),
(58, 'uploads/all/AJZpoShWAV3Ho2FWqIZxzuadxGNhWjxazmkEhL53.png', 'img_logo', 12234, 'png', 'image', 'App/Product', 8, 'image', '2021-03-12 08:25:33', '2021-03-15 02:24:17'),
(59, 'uploads/business_document/KKoJjzkUj50qyX9evpYxwUTU94qpXqY5VIgtPT71.docx', 'Le_Van_Tan', 42422, 'docx', 'document', 'App/Product', 8, 'image', '2021-03-12 08:25:33', '2021-03-15 02:24:17'),
(60, 'uploads/all/4uD1E1B8D0WJrTSCny6ogAeJKjd8FY6cLsv39OpJ.png', 'img_banana', 9667, 'png', 'image', 'App/Product', 8, 'image', '2021-03-12 08:25:33', '2021-03-15 02:24:17'),
(61, 'uploads/all/5CM9gVA8sFJVur80L41jYEz53QVEK9wRlCr10Rlv.png', 'img_mango', 19191, 'png', 'image', 'App/Product', 9, 'image', '2021-03-12 08:25:33', '2021-03-15 02:24:17'),
(62, 'uploads/all/1AltaWzbe42mqSIAtKPFNSJvNXpGXTv4p4TV9vl9.png', 'img_avocado_home', 8118, 'png', 'image', 'App/Product', 9, 'image', '2021-03-12 08:25:33', '2021-03-15 02:24:17'),
(63, 'uploads/all/wCeMnIMQRXBpUr47KpQPDfXss6KiO9IEZxi0qsvE.png', 'img_orange', 14040, 'png', 'image', 'App/Product', 9, 'image', '2021-03-12 08:25:33', '2021-03-15 02:24:17'),
(64, 'uploads/business_document/KKoJjzkUj50qyX9evpYxwUTU94qpXqY5VIgtPT71.docx', 'Le_Van_Tan', 42422, 'docx', 'document', 'App/Product', 1, 'image', '2021-03-12 08:26:29', '2021-03-15 02:24:17'),
(65, 'uploads/all/wCeMnIMQRXBpUr47KpQPDfXss6KiO9IEZxi0qsvE.png', 'img_orange', 14040, 'png', 'image', 'App/Product', 1, 'image', '2021-03-12 08:26:29', '2021-03-15 02:24:17'),
(66, 'uploads/all/4uD1E1B8D0WJrTSCny6ogAeJKjd8FY6cLsv39OpJ.png', 'img_banana', 9667, 'png', 'image', 'App/Product', 1, 'image', '2021-03-12 08:26:29', '2021-03-15 02:24:17'),
(67, 'uploads/all/L37mVULWRqsyaW0OruURxw4mmDV2sAokyFBJ4BoS.png', 'img_deal_mango', 24515, 'png', 'image', 'App/Product', 2, 'image', '2021-03-12 08:26:29', '2021-03-15 02:24:17'),
(68, 'uploads/business_document/aMnWaPm7Tht1Aae7dYaj0WeHQuVZgxExlcMC2dfR.docx', 'Le_Van_Tan', 42422, 'docx', 'document', 'App/Product', 2, 'image', '2021-03-12 08:26:29', '2021-03-15 02:24:18'),
(69, 'uploads/all/oknrbyio2c520s8YsFfcA02THs5edNTyd7HuApaf.png', 'img_small_apple', 2699, 'png', 'image', 'App/Product', 2, 'image', '2021-03-12 08:26:29', '2021-03-15 02:24:18'),
(70, 'uploads/all/oknrbyio2c520s8YsFfcA02THs5edNTyd7HuApaf.png', 'img_small_apple', 2699, 'png', 'image', 'App/Product', 3, 'image', '2021-03-12 08:26:29', '2021-03-15 02:24:18'),
(71, 'uploads/all/AJZpoShWAV3Ho2FWqIZxzuadxGNhWjxazmkEhL53.png', 'img_logo', 12234, 'png', 'image', 'App/Product', 3, 'image', '2021-03-12 08:26:29', '2021-03-15 02:24:18'),
(72, 'uploads/all/U4ToWDgGaM4UDeBFBwoi6xzhoCN2ZZd1qeBa46qW.png', 'img_peach', 16684, 'png', 'image', 'App/Product', 3, 'image', '2021-03-12 08:26:29', '2021-03-15 02:24:18'),
(73, 'uploads/all/U4ToWDgGaM4UDeBFBwoi6xzhoCN2ZZd1qeBa46qW.png', 'img_peach', 16684, 'png', 'image', 'App/Product', 4, 'image', '2021-03-12 08:26:29', '2021-03-15 02:24:18'),
(74, 'uploads/all/7YuyL6Hv9ouIMcxL5jf0WPZC9ncd9c3IwKW8ikzI.png', 'img_grape', 19243, 'png', 'image', 'App/Product', 4, 'image', '2021-03-12 08:26:29', '2021-03-15 02:24:18'),
(75, 'uploads/all/HreldpMpglaz1pF2skU66aIRa0XOB3JfmIY90Hhf.png', 'img_deal_blueberry', 26935, 'png', 'image', 'App/Product', 4, 'image', '2021-03-12 08:26:29', '2021-03-15 02:24:18'),
(76, 'uploads/all/wCeMnIMQRXBpUr47KpQPDfXss6KiO9IEZxi0qsvE.png', 'img_orange', 14040, 'png', 'image', 'App/Product', 5, 'image', '2021-03-12 08:26:30', '2021-03-15 02:24:18'),
(77, 'uploads/all/1AltaWzbe42mqSIAtKPFNSJvNXpGXTv4p4TV9vl9.png', 'img_avocado_home', 8118, 'png', 'image', 'App/Product', 5, 'image', '2021-03-12 08:26:30', '2021-03-15 02:24:18'),
(78, 'uploads/all/HreldpMpglaz1pF2skU66aIRa0XOB3JfmIY90Hhf.png', 'img_deal_blueberry', 26935, 'png', 'image', 'App/Product', 5, 'image', '2021-03-12 08:26:30', '2021-03-15 02:24:18'),
(79, 'uploads/all/oknrbyio2c520s8YsFfcA02THs5edNTyd7HuApaf.png', 'img_small_apple', 2699, 'png', 'image', 'App/Product', 6, 'image', '2021-03-12 08:26:30', '2021-03-15 02:24:18'),
(80, 'uploads/all/AJZpoShWAV3Ho2FWqIZxzuadxGNhWjxazmkEhL53.png', 'img_logo', 12234, 'png', 'image', 'App/Product', 6, 'image', '2021-03-12 08:26:30', '2021-03-15 02:24:18'),
(81, 'uploads/all/1AltaWzbe42mqSIAtKPFNSJvNXpGXTv4p4TV9vl9.png', 'img_avocado_home', 8118, 'png', 'image', 'App/Product', 6, 'image', '2021-03-12 08:26:30', '2021-03-15 02:24:18'),
(82, 'uploads/all/AJZpoShWAV3Ho2FWqIZxzuadxGNhWjxazmkEhL53.png', 'img_logo', 12234, 'png', 'image', 'App/Product', 7, 'image', '2021-03-12 08:26:30', '2021-03-15 02:24:18'),
(83, 'uploads/business_document/aMnWaPm7Tht1Aae7dYaj0WeHQuVZgxExlcMC2dfR.docx', 'Le_Van_Tan', 42422, 'docx', 'document', 'App/Product', 7, 'image', '2021-03-12 08:26:30', '2021-03-15 02:24:18'),
(84, 'uploads/all/wCeMnIMQRXBpUr47KpQPDfXss6KiO9IEZxi0qsvE.png', 'img_orange', 14040, 'png', 'image', 'App/Product', 7, 'image', '2021-03-12 08:26:30', '2021-03-15 02:24:18'),
(85, 'uploads/all/7YuyL6Hv9ouIMcxL5jf0WPZC9ncd9c3IwKW8ikzI.png', 'img_grape', 19243, 'png', 'image', 'App/Product', 8, 'image', '2021-03-12 08:26:30', '2021-03-15 02:24:18'),
(86, 'uploads/business_document/KKoJjzkUj50qyX9evpYxwUTU94qpXqY5VIgtPT71.docx', 'Le_Van_Tan', 42422, 'docx', 'document', 'App/Product', 8, 'image', '2021-03-12 08:26:30', '2021-03-15 02:24:18'),
(87, 'uploads/all/1AltaWzbe42mqSIAtKPFNSJvNXpGXTv4p4TV9vl9.png', 'img_avocado_home', 8118, 'png', 'image', 'App/Product', 8, 'image', '2021-03-12 08:26:30', '2021-03-15 02:24:18'),
(88, 'uploads/all/1AltaWzbe42mqSIAtKPFNSJvNXpGXTv4p4TV9vl9.png', 'img_avocado_home', 8118, 'png', 'image', 'App/Product', 9, 'image', '2021-03-12 08:26:30', '2021-03-15 02:24:18'),
(89, 'uploads/all/7YuyL6Hv9ouIMcxL5jf0WPZC9ncd9c3IwKW8ikzI.png', 'img_grape', 19243, 'png', 'image', 'App/Product', 9, 'image', '2021-03-12 08:26:31', '2021-03-15 02:24:18'),
(90, 'uploads/all/oknrbyio2c520s8YsFfcA02THs5edNTyd7HuApaf.png', 'img_small_apple', 2699, 'png', 'image', 'App/Product', 9, 'image', '2021-03-12 08:26:31', '2021-03-15 02:24:18');

-- --------------------------------------------------------

--
-- Table structure for table `faqs`
--

CREATE TABLE `faqs` (
  `id` bigint NOT NULL,
  `title` text COLLATE utf8_bin NOT NULL,
  `content` text COLLATE utf8_bin NOT NULL,
  `status` enum('1','2') COLLATE utf8_bin NOT NULL DEFAULT '1',
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `faqs`
--

INSERT INTO `faqs` (`id`, `title`, `content`, `status`, `created_at`, `updated_at`) VALUES
(1, 'What is Lorem Ipsum 1', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.', '1', '2021-03-09 08:59:24', '2021-03-09 08:59:24'),
(2, 'What is Lorem Ipsum 2', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.', '1', '2021-03-09 08:59:24', '2021-03-09 08:59:24'),
(3, 'What is Lorem Ipsum 3', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.', '1', '2021-03-09 08:59:24', '2021-03-09 08:59:24'),
(4, 'What is Lorem Ipsum 4', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.', '1', '2021-03-09 08:59:24', '2021-03-09 08:59:24'),
(5, 'What is Lorem Ipsum 5', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.', '1', '2021-03-09 08:59:24', '2021-03-09 08:59:24'),
(6, 'What is Lorem Ipsum 6', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.', '1', '2021-03-09 08:59:24', '2021-03-09 08:59:24'),
(7, 'What is Lorem Ipsum 7', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.', '1', '2021-03-09 08:59:24', '2021-03-09 08:59:24'),
(8, 'What is Lorem Ipsum 8', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.', '1', '2021-03-09 08:59:24', '2021-03-09 08:59:24'),
(9, 'What is Lorem Ipsum 9', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.', '1', '2021-03-09 08:59:24', '2021-03-09 08:59:24');

-- --------------------------------------------------------

--
-- Table structure for table `files`
--

CREATE TABLE `files` (
  `id` bigint NOT NULL,
  `file_original_name` varchar(255) COLLATE utf8_bin NOT NULL,
  `file_name` varchar(100) COLLATE utf8_bin NOT NULL,
  `member_id` bigint NOT NULL,
  `user_id` bigint DEFAULT '0',
  `file_size` int NOT NULL,
  `extension` varchar(5) COLLATE utf8_bin NOT NULL,
  `type` varchar(100) COLLATE utf8_bin NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `files`
--

INSERT INTO `files` (`id`, `file_original_name`, `file_name`, `member_id`, `user_id`, `file_size`, `extension`, `type`, `created_at`, `updated_at`) VALUES
(1, 'img_avocado_home', 'uploads/all/1AltaWzbe42mqSIAtKPFNSJvNXpGXTv4p4TV9vl9.png', 1, 0, 8118, 'png', 'image', '2021-03-09 08:17:20', '2021-03-09 08:17:20'),
(2, 'img_deal_mango', 'uploads/all/L37mVULWRqsyaW0OruURxw4mmDV2sAokyFBJ4BoS.png', 1, 0, 24515, 'png', 'image', '2021-03-09 08:17:20', '2021-03-09 08:17:20'),
(3, 'img_grape', 'uploads/all/7YuyL6Hv9ouIMcxL5jf0WPZC9ncd9c3IwKW8ikzI.png', 1, 0, 19243, 'png', 'image', '2021-03-09 08:17:20', '2021-03-09 08:17:20'),
(4, 'img_banana', 'uploads/all/4uD1E1B8D0WJrTSCny6ogAeJKjd8FY6cLsv39OpJ.png', 1, 0, 9667, 'png', 'image', '2021-03-09 08:17:20', '2021-03-09 08:17:20'),
(5, 'img_logo', 'uploads/all/AJZpoShWAV3Ho2FWqIZxzuadxGNhWjxazmkEhL53.png', 1, 0, 12234, 'png', 'image', '2021-03-09 08:17:20', '2021-03-09 08:17:20'),
(6, 'img_deal_blueberry', 'uploads/all/HreldpMpglaz1pF2skU66aIRa0XOB3JfmIY90Hhf.png', 1, 0, 26935, 'png', 'image', '2021-03-09 08:17:21', '2021-03-09 08:17:21'),
(7, 'img_mango', 'uploads/all/5CM9gVA8sFJVur80L41jYEz53QVEK9wRlCr10Rlv.png', 1, 0, 19191, 'png', 'image', '2021-03-09 08:17:21', '2021-03-09 08:17:21'),
(8, 'img_orange', 'uploads/all/wCeMnIMQRXBpUr47KpQPDfXss6KiO9IEZxi0qsvE.png', 1, 0, 14040, 'png', 'image', '2021-03-09 08:17:21', '2021-03-09 08:17:21'),
(9, 'img_small_apple', 'uploads/all/oknrbyio2c520s8YsFfcA02THs5edNTyd7HuApaf.png', 1, 0, 2699, 'png', 'image', '2021-03-09 08:17:21', '2021-03-09 08:17:21'),
(10, 'img_peach', 'uploads/all/U4ToWDgGaM4UDeBFBwoi6xzhoCN2ZZd1qeBa46qW.png', 1, 0, 16684, 'png', 'image', '2021-03-09 08:17:21', '2021-03-09 08:17:21'),
(12, 'Le_Van_Tan', 'uploads/business_document/KKoJjzkUj50qyX9evpYxwUTU94qpXqY5VIgtPT71.docx', 19, 19, 42422, 'docx', 'document', '2021-03-10 10:03:14', '2021-03-10 10:03:14'),
(13, 'Le_Van_Tan', 'uploads/business_document/aMnWaPm7Tht1Aae7dYaj0WeHQuVZgxExlcMC2dfR.docx', 19, 19, 42422, 'docx', 'document', '2021-03-10 10:06:24', '2021-03-10 10:06:24');

-- --------------------------------------------------------

--
-- Table structure for table `like_products`
--

CREATE TABLE `like_products` (
  `id` bigint UNSIGNED NOT NULL,
  `user_id` bigint UNSIGNED NOT NULL,
  `product_id` bigint NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `log_activities`
--

CREATE TABLE `log_activities` (
  `id` bigint NOT NULL,
  `user_id` bigint UNSIGNED NOT NULL,
  `content` text COLLATE utf8_bin NOT NULL,
  `entity_type` varchar(100) COLLATE utf8_bin NOT NULL,
  `entity_id` bigint NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int UNSIGNED NOT NULL,
  `migration` varchar(191) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `batch` int NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `oauth_access_tokens`
--

CREATE TABLE `oauth_access_tokens` (
  `id` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` int DEFAULT NULL,
  `client_id` int UNSIGNED NOT NULL,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `scopes` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `expires_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `oauth_auth_codes`
--

CREATE TABLE `oauth_auth_codes` (
  `id` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` int NOT NULL,
  `client_id` int UNSIGNED NOT NULL,
  `scopes` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `oauth_clients`
--

CREATE TABLE `oauth_clients` (
  `id` int UNSIGNED NOT NULL,
  `user_id` int DEFAULT NULL,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `secret` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `redirect` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `personal_access_client` tinyint(1) NOT NULL,
  `password_client` tinyint(1) NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `oauth_clients`
--

INSERT INTO `oauth_clients` (`id`, `user_id`, `name`, `secret`, `redirect`, `personal_access_client`, `password_client`, `revoked`, `created_at`, `updated_at`) VALUES
(1, NULL, 'Laravel Personal Access Client', 'eR2y7WUuem28ugHKppFpmss7jPyOHZsMkQwBo1Jj', 'http://localhost', 1, 0, 0, '2019-07-13 06:17:34', '2019-07-13 06:17:34'),
(2, NULL, 'Laravel Password Grant Client', 'WLW2Ol0GozbaXEnx1NtXoweYPuKEbjWdviaUgw77', 'http://localhost', 0, 1, 0, '2019-07-13 06:17:34', '2019-07-13 06:17:34');

-- --------------------------------------------------------

--
-- Table structure for table `oauth_personal_access_clients`
--

CREATE TABLE `oauth_personal_access_clients` (
  `id` int UNSIGNED NOT NULL,
  `client_id` int UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `oauth_personal_access_clients`
--

INSERT INTO `oauth_personal_access_clients` (`id`, `client_id`, `created_at`, `updated_at`) VALUES
(1, 1, '2019-07-13 06:17:34', '2019-07-13 06:17:34');

-- --------------------------------------------------------

--
-- Table structure for table `oauth_refresh_tokens`
--

CREATE TABLE `oauth_refresh_tokens` (
  `id` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `access_token_id` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

CREATE TABLE `orders` (
  `id` bigint NOT NULL,
  `displayname` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `phone` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `delivery_address` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `sub_total` double NOT NULL DEFAULT '0',
  `delivery_fee` double NOT NULL DEFAULT '0',
  `total` double NOT NULL DEFAULT '0',
  `delivery_method` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `delivery_id` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `shipper_name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `shipper_phone` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `delivery_date` date NOT NULL,
  `status` enum('1','2','3','4','5','6','7') COLLATE utf8_unicode_ci NOT NULL DEFAULT '1',
  `user_id` bigint UNSIGNED DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `order_product`
--

CREATE TABLE `order_product` (
  `id` bigint NOT NULL,
  `order_id` bigint NOT NULL,
  `product_id` bigint NOT NULL,
  `variant` text COLLATE utf8_bin NOT NULL,
  `qty` int NOT NULL DEFAULT '0',
  `subtotal` double NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Table structure for table `pages`
--

CREATE TABLE `pages` (
  `id` int NOT NULL,
  `title` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `content` longtext CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  `status` enum('1','2') COLLATE utf8_unicode_ci NOT NULL DEFAULT '1',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(191) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `id` bigint NOT NULL,
  `name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `category_id` bigint NOT NULL,
  `supplier_id` bigint NOT NULL,
  `variant` text CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  `sku` text CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `status` enum('1','2','3','4') COLLATE utf8_unicode_ci NOT NULL DEFAULT '1',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`id`, `name`, `category_id`, `supplier_id`, `variant`, `sku`, `description`, `status`, `created_at`, `updated_at`) VALUES
(1, 'Product 1', 4, 1, 'a:2:{s:5:\"title\";a:2:{i:0;s:6:\"Origin\";i:1;s:4:\"Size\";}s:5:\"value\";a:2:{i:0;a:2:{i:0;s:3:\"USA\";i:1;s:5:\"KOREA\";}i:1;a:3:{i:0;s:8:\"77p/17kg\";i:1;s:7:\"8p/17kg\";i:2;s:8:\"12p/12kg\";}}}', NULL, 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.', '1', '2021-03-12 01:26:29', '2021-03-12 01:26:29'),
(2, 'Product 2', 9, 1, 'a:2:{s:5:\"title\";a:2:{i:0;s:6:\"Origin\";i:1;s:4:\"Size\";}s:5:\"value\";a:2:{i:0;a:2:{i:0;s:3:\"USA\";i:1;s:5:\"KOREA\";}i:1;a:3:{i:0;s:8:\"77p/17kg\";i:1;s:7:\"8p/17kg\";i:2;s:8:\"12p/12kg\";}}}', NULL, 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.', '1', '2021-03-12 01:26:29', '2021-03-12 01:26:29'),
(3, 'Product 3', 4, 1, 'a:2:{s:5:\"title\";a:2:{i:0;s:6:\"Origin\";i:1;s:4:\"Size\";}s:5:\"value\";a:2:{i:0;a:2:{i:0;s:3:\"USA\";i:1;s:5:\"KOREA\";}i:1;a:3:{i:0;s:8:\"77p/17kg\";i:1;s:7:\"8p/17kg\";i:2;s:8:\"12p/12kg\";}}}', NULL, 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.', '1', '2021-03-12 01:26:29', '2021-03-12 01:26:29'),
(4, 'Product 4', 8, 1, 'a:2:{s:5:\"title\";a:2:{i:0;s:6:\"Origin\";i:1;s:4:\"Size\";}s:5:\"value\";a:2:{i:0;a:2:{i:0;s:3:\"USA\";i:1;s:5:\"KOREA\";}i:1;a:3:{i:0;s:8:\"77p/17kg\";i:1;s:7:\"8p/17kg\";i:2;s:8:\"12p/12kg\";}}}', NULL, 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.', '1', '2021-03-12 01:26:29', '2021-03-12 01:26:29'),
(5, 'Product 5', 2, 1, 'a:2:{s:5:\"title\";a:2:{i:0;s:6:\"Origin\";i:1;s:4:\"Size\";}s:5:\"value\";a:2:{i:0;a:2:{i:0;s:3:\"USA\";i:1;s:5:\"KOREA\";}i:1;a:3:{i:0;s:8:\"77p/17kg\";i:1;s:7:\"8p/17kg\";i:2;s:8:\"12p/12kg\";}}}', NULL, 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.', '1', '2021-03-12 01:26:29', '2021-03-12 01:26:29'),
(6, 'Product 6', 1, 1, 'a:2:{s:5:\"title\";a:2:{i:0;s:6:\"Origin\";i:1;s:4:\"Size\";}s:5:\"value\";a:2:{i:0;a:2:{i:0;s:3:\"USA\";i:1;s:5:\"KOREA\";}i:1;a:3:{i:0;s:8:\"77p/17kg\";i:1;s:7:\"8p/17kg\";i:2;s:8:\"12p/12kg\";}}}', NULL, 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.', '1', '2021-03-12 01:26:30', '2021-03-12 01:26:30'),
(7, 'Product 7', 4, 1, 'a:2:{s:5:\"title\";a:2:{i:0;s:6:\"Origin\";i:1;s:4:\"Size\";}s:5:\"value\";a:2:{i:0;a:2:{i:0;s:3:\"USA\";i:1;s:5:\"KOREA\";}i:1;a:3:{i:0;s:8:\"77p/17kg\";i:1;s:7:\"8p/17kg\";i:2;s:8:\"12p/12kg\";}}}', NULL, 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.', '1', '2021-03-12 01:26:30', '2021-03-12 01:26:30'),
(8, 'Product 8', 5, 1, 'a:2:{s:5:\"title\";a:2:{i:0;s:6:\"Origin\";i:1;s:4:\"Size\";}s:5:\"value\";a:2:{i:0;a:2:{i:0;s:3:\"USA\";i:1;s:5:\"KOREA\";}i:1;a:3:{i:0;s:8:\"77p/17kg\";i:1;s:7:\"8p/17kg\";i:2;s:8:\"12p/12kg\";}}}', NULL, 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.', '1', '2021-03-12 01:26:30', '2021-03-12 01:26:30'),
(9, 'Product 9', 9, 1, 'a:2:{s:5:\"title\";a:2:{i:0;s:6:\"Origin\";i:1;s:4:\"Size\";}s:5:\"value\";a:2:{i:0;a:2:{i:0;s:3:\"USA\";i:1;s:5:\"KOREA\";}i:1;a:3:{i:0;s:8:\"77p/17kg\";i:1;s:7:\"8p/17kg\";i:2;s:8:\"12p/12kg\";}}}', NULL, 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.', '1', '2021-03-12 01:26:30', '2021-03-12 01:26:30');

-- --------------------------------------------------------

--
-- Table structure for table `product_sku`
--

CREATE TABLE `product_sku` (
  `id` bigint NOT NULL,
  `product_id` bigint NOT NULL,
  `sku` varchar(100) COLLATE utf8_bin NOT NULL,
  `price` double NOT NULL,
  `qty` int NOT NULL,
  `status` enum('1','2') COLLATE utf8_bin NOT NULL DEFAULT '1',
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `product_sku`
--

INSERT INTO `product_sku` (`id`, `product_id`, `sku`, `price`, `qty`, `status`, `created_at`, `updated_at`) VALUES
(1, 1, '#PRO1USA77p/17kg', 1500, 40, '2', '2021-03-12 08:26:29', '2021-03-12 08:26:29'),
(2, 1, '#PRO1USA8p/17kg', 1500, 40, '1', '2021-03-12 08:26:29', '2021-03-12 08:26:29'),
(3, 1, '#PRO1USA12p/12kg', 1500, 40, '2', '2021-03-12 08:26:29', '2021-03-12 08:26:29'),
(4, 1, '#PRO1KOREA77p/17kg', 1500, 40, '2', '2021-03-12 08:26:29', '2021-03-12 08:26:29'),
(5, 1, '#PRO1KOREA8p/17kg', 1500, 40, '1', '2021-03-12 08:26:29', '2021-03-12 08:26:29'),
(6, 1, '#PRO1KOREA12p/12kg', 1500, 40, '1', '2021-03-12 08:26:29', '2021-03-12 08:26:29'),
(7, 2, '#PRO2USA77p/17kg', 1500, 40, '2', '2021-03-12 08:26:29', '2021-03-12 08:26:29'),
(8, 2, '#PRO2USA8p/17kg', 1500, 40, '1', '2021-03-12 08:26:29', '2021-03-12 08:26:29'),
(9, 2, '#PRO2USA12p/12kg', 1500, 40, '1', '2021-03-12 08:26:29', '2021-03-12 08:26:29'),
(10, 2, '#PRO2KOREA77p/17kg', 1500, 40, '1', '2021-03-12 08:26:29', '2021-03-12 08:26:29'),
(11, 2, '#PRO2KOREA8p/17kg', 1500, 40, '1', '2021-03-12 08:26:29', '2021-03-12 08:26:29'),
(12, 2, '#PRO2KOREA12p/12kg', 1500, 40, '2', '2021-03-12 08:26:29', '2021-03-12 08:26:29'),
(13, 3, '#PRO3USA77p/17kg', 1500, 40, '2', '2021-03-12 08:26:29', '2021-03-12 08:26:29'),
(14, 3, '#PRO3USA8p/17kg', 1500, 40, '2', '2021-03-12 08:26:29', '2021-03-12 08:26:29'),
(15, 3, '#PRO3USA12p/12kg', 1500, 40, '1', '2021-03-12 08:26:29', '2021-03-12 08:26:29'),
(16, 3, '#PRO3KOREA77p/17kg', 1500, 40, '2', '2021-03-12 08:26:29', '2021-03-12 08:26:29'),
(17, 3, '#PRO3KOREA8p/17kg', 1500, 40, '2', '2021-03-12 08:26:29', '2021-03-12 08:26:29'),
(18, 3, '#PRO3KOREA12p/12kg', 1500, 40, '1', '2021-03-12 08:26:29', '2021-03-12 08:26:29'),
(19, 4, '#PRO4USA77p/17kg', 1500, 40, '2', '2021-03-12 08:26:29', '2021-03-12 08:26:29'),
(20, 4, '#PRO4USA8p/17kg', 1500, 40, '1', '2021-03-12 08:26:29', '2021-03-12 08:26:29'),
(21, 4, '#PRO4USA12p/12kg', 1500, 40, '2', '2021-03-12 08:26:29', '2021-03-12 08:26:29'),
(22, 4, '#PRO4KOREA77p/17kg', 1500, 40, '2', '2021-03-12 08:26:29', '2021-03-12 08:26:29'),
(23, 4, '#PRO4KOREA8p/17kg', 1500, 40, '2', '2021-03-12 08:26:29', '2021-03-12 08:26:29'),
(24, 4, '#PRO4KOREA12p/12kg', 1500, 40, '1', '2021-03-12 08:26:29', '2021-03-12 08:26:29'),
(25, 5, '#PRO5USA77p/17kg', 1500, 40, '1', '2021-03-12 08:26:30', '2021-03-12 08:26:30'),
(26, 5, '#PRO5USA8p/17kg', 1500, 40, '2', '2021-03-12 08:26:30', '2021-03-12 08:26:30'),
(27, 5, '#PRO5USA12p/12kg', 1500, 40, '1', '2021-03-12 08:26:30', '2021-03-12 08:26:30'),
(28, 5, '#PRO5KOREA77p/17kg', 1500, 40, '2', '2021-03-12 08:26:30', '2021-03-12 08:26:30'),
(29, 5, '#PRO5KOREA8p/17kg', 1500, 40, '2', '2021-03-12 08:26:30', '2021-03-12 08:26:30'),
(30, 5, '#PRO5KOREA12p/12kg', 1500, 40, '2', '2021-03-12 08:26:30', '2021-03-12 08:26:30'),
(31, 6, '#PRO6USA77p/17kg', 1500, 40, '1', '2021-03-12 08:26:30', '2021-03-12 08:26:30'),
(32, 6, '#PRO6USA8p/17kg', 1500, 40, '1', '2021-03-12 08:26:30', '2021-03-12 08:26:30'),
(33, 6, '#PRO6USA12p/12kg', 1500, 40, '2', '2021-03-12 08:26:30', '2021-03-12 08:26:30'),
(34, 6, '#PRO6KOREA77p/17kg', 1500, 40, '2', '2021-03-12 08:26:30', '2021-03-12 08:26:30'),
(35, 6, '#PRO6KOREA8p/17kg', 1500, 40, '1', '2021-03-12 08:26:30', '2021-03-12 08:26:30'),
(36, 6, '#PRO6KOREA12p/12kg', 1500, 40, '2', '2021-03-12 08:26:30', '2021-03-12 08:26:30'),
(37, 7, '#PRO7USA77p/17kg', 1500, 40, '1', '2021-03-12 08:26:30', '2021-03-12 08:26:30'),
(38, 7, '#PRO7USA8p/17kg', 1500, 40, '1', '2021-03-12 08:26:30', '2021-03-12 08:26:30'),
(39, 7, '#PRO7USA12p/12kg', 1500, 40, '2', '2021-03-12 08:26:30', '2021-03-12 08:26:30'),
(40, 7, '#PRO7KOREA77p/17kg', 1500, 40, '1', '2021-03-12 08:26:30', '2021-03-12 08:26:30'),
(41, 7, '#PRO7KOREA8p/17kg', 1500, 40, '2', '2021-03-12 08:26:30', '2021-03-12 08:26:30'),
(42, 7, '#PRO7KOREA12p/12kg', 1500, 40, '2', '2021-03-12 08:26:30', '2021-03-12 08:26:30'),
(43, 8, '#PRO8USA77p/17kg', 1500, 40, '2', '2021-03-12 08:26:30', '2021-03-12 08:26:30'),
(44, 8, '#PRO8USA8p/17kg', 1500, 40, '1', '2021-03-12 08:26:30', '2021-03-12 08:26:30'),
(45, 8, '#PRO8USA12p/12kg', 1500, 40, '1', '2021-03-12 08:26:30', '2021-03-12 08:26:30'),
(46, 8, '#PRO8KOREA77p/17kg', 1500, 40, '2', '2021-03-12 08:26:30', '2021-03-12 08:26:30'),
(47, 8, '#PRO8KOREA8p/17kg', 1500, 40, '2', '2021-03-12 08:26:30', '2021-03-12 08:26:30'),
(48, 8, '#PRO8KOREA12p/12kg', 1500, 40, '1', '2021-03-12 08:26:30', '2021-03-12 08:26:30'),
(49, 9, '#PRO9USA77p/17kg', 1500, 40, '2', '2021-03-12 08:26:30', '2021-03-12 08:26:30'),
(50, 9, '#PRO9USA8p/17kg', 1500, 40, '2', '2021-03-12 08:26:30', '2021-03-12 08:26:30'),
(51, 9, '#PRO9USA12p/12kg', 1500, 40, '1', '2021-03-12 08:26:30', '2021-03-12 08:26:30'),
(52, 9, '#PRO9KOREA77p/17kg', 1500, 40, '1', '2021-03-12 08:26:30', '2021-03-12 08:26:30'),
(53, 9, '#PRO9KOREA8p/17kg', 1500, 40, '2', '2021-03-12 08:26:30', '2021-03-12 08:26:30'),
(54, 9, '#PRO9KOREA12p/12kg', 1500, 40, '1', '2021-03-12 08:26:30', '2021-03-12 08:26:30');

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE `roles` (
  `id` bigint NOT NULL,
  `name` varchar(30) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `permissions` mediumtext CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id`, `name`, `permissions`, `created_at`, `updated_at`) VALUES
(1, 'Manager', '[\"1\",\"2\",\"4\"]', '2018-10-10 04:39:47', '2018-10-10 04:51:37'),
(2, 'Accountant', '[\"2\",\"3\"]', '2018-10-10 04:52:09', '2018-10-10 04:52:09');

-- --------------------------------------------------------

--
-- Table structure for table `searches`
--

CREATE TABLE `searches` (
  `id` int NOT NULL,
  `query` varchar(1000) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `count` int NOT NULL DEFAULT '1',
  `user_id` bigint UNSIGNED NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `settings`
--

CREATE TABLE `settings` (
  `id` int NOT NULL,
  `type` varchar(30) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `value` longtext CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `settings`
--

INSERT INTO `settings` (`id`, `type`, `value`, `created_at`, `updated_at`) VALUES
(37, 'email_verification', '0', '2019-04-30 07:30:07', '2019-04-30 07:30:07'),
(40, 'current_version', '3.5', '2019-06-11 09:46:18', '2019-06-11 09:46:18'),
(64, 'term_service', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.', '2021-03-09 01:59:18', '2021-03-09 01:59:18'),
(65, 'policy_service', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.', '2021-03-09 01:59:18', '2021-03-09 01:59:18'),
(66, 'thank_you_order', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.', '2021-03-09 01:59:18', '2021-03-09 01:59:18');

-- --------------------------------------------------------

--
-- Table structure for table `suppliers`
--

CREATE TABLE `suppliers` (
  `id` bigint NOT NULL,
  `name` varchar(100) COLLATE utf8_bin NOT NULL,
  `address` varchar(255) COLLATE utf8_bin NOT NULL,
  `location` varchar(100) COLLATE utf8_bin NOT NULL,
  `email` varchar(100) COLLATE utf8_bin NOT NULL,
  `phone` varchar(20) COLLATE utf8_bin NOT NULL,
  `representative` varchar(100) COLLATE utf8_bin NOT NULL,
  `status` enum('1','2') COLLATE utf8_bin NOT NULL DEFAULT '1',
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `suppliers`
--

INSERT INTO `suppliers` (`id`, `name`, `address`, `location`, `email`, `phone`, `representative`, `status`, `created_at`, `updated_at`) VALUES
(1, 'Suppliers 1', 'Hue', 'Hue', 'suppliers1@gmail.com', '123123', 'suppliers', '1', '2021-03-12 10:00:16', '2021-03-12 10:00:16');

-- --------------------------------------------------------

--
-- Table structure for table `team_members`
--

CREATE TABLE `team_members` (
  `id` bigint NOT NULL,
  `name` varchar(100) COLLATE utf8_bin NOT NULL,
  `birthday` date DEFAULT NULL,
  `phone` varchar(20) COLLATE utf8_bin NOT NULL,
  `password` varchar(100) COLLATE utf8_bin NOT NULL,
  `email` varchar(100) COLLATE utf8_bin NOT NULL,
  `address` varchar(255) COLLATE utf8_bin NOT NULL,
  `id_number` varchar(100) COLLATE utf8_bin NOT NULL,
  `status` enum('1','2','3') COLLATE utf8_bin NOT NULL DEFAULT '1',
  `date_of_issue` date NOT NULL,
  `role_id` bigint DEFAULT '0',
  `avatar` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `team_members`
--

INSERT INTO `team_members` (`id`, `name`, `birthday`, `phone`, `password`, `email`, `address`, `id_number`, `status`, `date_of_issue`, `role_id`, `avatar`, `created_at`, `updated_at`) VALUES
(1, 'Admin', NULL, '123123', '$2y$10$McNZLabHHVBYNY1pF1JS5enDAMUF3sC0yEl52RF8cfZ0OLxe3Tfx.', 'admin@avocadoo.com', 'Hue', '123123', '1', '2020-02-20', NULL, NULL, '2021-03-05 14:25:56', '2021-03-05 14:25:59');

-- --------------------------------------------------------

--
-- Table structure for table `transactions`
--

CREATE TABLE `transactions` (
  `id` bigint NOT NULL,
  `user_id` bigint UNSIGNED NOT NULL,
  `order_id` bigint NOT NULL,
  `money` double NOT NULL,
  `method` varchar(100) COLLATE utf8_bin NOT NULL,
  `status` enum('1','2') COLLATE utf8_bin NOT NULL DEFAULT '1',
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint UNSIGNED NOT NULL,
  `displayname` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(191) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `password` varchar(191) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `remember_token` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `fullname` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `phone` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `birthday` date DEFAULT NULL,
  `gender` enum('1','2') COLLATE utf8_unicode_ci NOT NULL DEFAULT '1',
  `verify_phone` enum('0','1') COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  `code_phone` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `status` enum('1','2','3') COLLATE utf8_unicode_ci NOT NULL DEFAULT '2',
  `business_name` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `tax_invoice` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `representative` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `store_name` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `store_address` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `avatar` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `kakao_id` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `naver_id` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `displayname`, `email`, `password`, `remember_token`, `fullname`, `phone`, `birthday`, `gender`, `verify_phone`, `code_phone`, `status`, `business_name`, `tax_invoice`, `representative`, `store_name`, `store_address`, `avatar`, `kakao_id`, `naver_id`, `created_at`, `updated_at`) VALUES
(10, 'user1', 'email1@gmail.com', '$2y$10$dV7Rks8AnlfPQwWgRdFdjuNJNIM0i48LbZOPNiLtoV5JTKfIWVUca', NULL, 'User Test 1', '090929291', '1994-08-01', '2', '1', NULL, '1', 'Company 1', '090929291', 'representative', 'Store name1', 'Address 1', NULL, NULL, NULL, '2021-03-09 01:20:07', '2021-03-09 01:20:07'),
(11, 'user2', 'email2@gmail.com', '$2y$10$je2eW18kTd7vrUXvkvQ/Ae3dL2SWQBusHoM38It6.oKkzxNWrY632', NULL, 'User Test 2', '090929292', '1994-08-02', '2', '1', NULL, '1', 'Company 2', '090929292', 'representative', 'Store name2', 'Address 2', NULL, NULL, NULL, '2021-03-09 01:20:07', '2021-03-09 01:20:07'),
(12, 'user3', 'email3@gmail.com', '$2y$10$BGYJTrJ829A/AnkCbIHgk.INhGLUlTKJIkCRLblQ.raf9MSui.HLW', NULL, 'User Test 3', '090929293', '1994-08-03', '2', '1', NULL, '1', 'Company 3', '090929293', 'representative', 'Store name3', 'Address 3', NULL, NULL, NULL, '2021-03-09 01:20:07', '2021-03-09 01:20:07'),
(13, 'user4', 'email4@gmail.com', '$2y$10$CDSAI95TFHLkRhKSFJk1ceQUhvoYuvfM/gfN1XFx1wyQD1lYzyxku', NULL, 'User Test 4', '090929294', '1994-08-04', '2', '1', NULL, '1', 'Company 4', '090929294', 'representative', 'Store name4', 'Address 4', NULL, NULL, NULL, '2021-03-09 01:20:07', '2021-03-09 01:20:07'),
(14, 'user5', 'email5@gmail.com', '$2y$10$cV.tPE.ihUZvUIrctB9wlunwMe1kAzhZdHL82s2IIkwofQrehIBBW', NULL, 'User Test 5', '090929295', '1994-08-05', '2', '1', NULL, '1', 'Company 5', '090929295', 'representative', 'Store name5', 'Address 5', NULL, NULL, NULL, '2021-03-09 01:20:07', '2021-03-09 01:20:07'),
(15, 'user6', 'email6@gmail.com', '$2y$10$mk6vX4FkP7R9XgMKs74Q..aPiaNfytAE72Ed/H9R.oB8A5sBYXgMu', NULL, 'User Test 6', '090929296', '1994-08-06', '1', '1', NULL, '1', 'Company 6', '090929296', 'representative', 'Store name6', 'Address 6', NULL, NULL, NULL, '2021-03-09 01:20:07', '2021-03-09 01:20:07'),
(16, 'user7', 'email7@gmail.com', '$2y$10$qSvlDBd8WroULqv7HfZ7hOC8SsTHNf9a751Og8hDMP6IBV5KhFEzG', NULL, 'User Test 7', '090929297', '1994-08-07', '1', '1', NULL, '1', 'Company 7', '090929297', 'representative', 'Store name7', 'Address 7', NULL, NULL, NULL, '2021-03-09 01:20:07', '2021-03-09 01:20:07'),
(17, 'user8', 'email8@gmail.com', '$2y$10$4Lep22RdZhQmkKNB488w4O2HoktjcriRSb4i.u7f2wtiE3aq2sv22', NULL, 'User Test 8', '090929298', '1994-08-08', '1', '1', NULL, '1', 'Company 8', '090929298', 'representative', 'Store name8', 'Address 8', NULL, NULL, NULL, '2021-03-09 01:20:07', '2021-03-09 01:20:07'),
(18, 'user9', 'email9@gmail.com', '$2y$10$LPAxOpuK5F0zwOOoWcijWO3CwGdIz/Fnnh17iPJ05mec8EhvqD.b2', NULL, 'User Test 9', '090929299', '1994-08-09', '2', '1', NULL, '1', 'Company 9', '090929299', 'representative', 'Store name9', 'Address 9', NULL, NULL, NULL, '2021-03-09 01:20:07', '2021-03-09 01:20:07'),
(19, 'Test', 'dev1test2019@gmail.com', NULL, NULL, 'Le Van Test', '123123', '1994-01-12', '1', '0', NULL, '1', 'VITPR', '123123', '123123', 'VITPR', 'Huesss', 'uploads/avatar/YTmGqXWQNMcusjHDvRHY3lc2qLQWCvP8Bc5ecpYm.png', '1111', NULL, '2021-03-10 02:12:03', '2021-03-10 20:12:28');

-- --------------------------------------------------------

--
-- Table structure for table `user_device_fcms`
--

CREATE TABLE `user_device_fcms` (
  `id` bigint UNSIGNED NOT NULL,
  `user_id` bigint UNSIGNED NOT NULL,
  `token` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `device` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'ios/android/website',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `user_payments`
--

CREATE TABLE `user_payments` (
  `id` bigint NOT NULL,
  `user_id` bigint UNSIGNED NOT NULL,
  `card_number` varchar(50) COLLATE utf8_bin NOT NULL,
  `card_name` varchar(100) COLLATE utf8_bin NOT NULL,
  `expiry_date` varchar(6) COLLATE utf8_bin NOT NULL,
  `ccv` varchar(6) COLLATE utf8_bin NOT NULL,
  `type` enum('1','0') COLLATE utf8_bin NOT NULL DEFAULT '0',
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `banners`
--
ALTER TABLE `banners`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `collections`
--
ALTER TABLE `collections`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `collection_products`
--
ALTER TABLE `collection_products`
  ADD PRIMARY KEY (`id`),
  ADD KEY `collection_id` (`collection_id`),
  ADD KEY `product_id` (`product_id`);

--
-- Indexes for table `consignment_products`
--
ALTER TABLE `consignment_products`
  ADD PRIMARY KEY (`id`),
  ADD KEY `product_id` (`product_id`);

--
-- Indexes for table `delivery_books`
--
ALTER TABLE `delivery_books`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_id` (`user_id`);

--
-- Indexes for table `entity_images`
--
ALTER TABLE `entity_images`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `faqs`
--
ALTER TABLE `faqs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `files`
--
ALTER TABLE `files`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `like_products`
--
ALTER TABLE `like_products`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `user_id_product_id` (`user_id`,`product_id`),
  ADD KEY `like_products_ibfk_2` (`product_id`);

--
-- Indexes for table `log_activities`
--
ALTER TABLE `log_activities`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_id` (`user_id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `oauth_access_tokens`
--
ALTER TABLE `oauth_access_tokens`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_access_tokens_user_id_index` (`user_id`);

--
-- Indexes for table `oauth_auth_codes`
--
ALTER TABLE `oauth_auth_codes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `oauth_clients`
--
ALTER TABLE `oauth_clients`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_clients_user_id_index` (`user_id`);

--
-- Indexes for table `oauth_personal_access_clients`
--
ALTER TABLE `oauth_personal_access_clients`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_personal_access_clients_client_id_index` (`client_id`);

--
-- Indexes for table `oauth_refresh_tokens`
--
ALTER TABLE `oauth_refresh_tokens`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_refresh_tokens_access_token_id_index` (`access_token_id`);

--
-- Indexes for table `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_id` (`user_id`);

--
-- Indexes for table `order_product`
--
ALTER TABLE `order_product`
  ADD PRIMARY KEY (`id`),
  ADD KEY `order_id` (`order_id`),
  ADD KEY `product_id` (`product_id`);

--
-- Indexes for table `pages`
--
ALTER TABLE `pages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`),
  ADD KEY `category_id` (`category_id`);

--
-- Indexes for table `product_sku`
--
ALTER TABLE `product_sku`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `searches`
--
ALTER TABLE `searches`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_id` (`user_id`);

--
-- Indexes for table `settings`
--
ALTER TABLE `settings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `suppliers`
--
ALTER TABLE `suppliers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `team_members`
--
ALTER TABLE `team_members`
  ADD PRIMARY KEY (`id`),
  ADD KEY `role_id` (`role_id`);

--
-- Indexes for table `transactions`
--
ALTER TABLE `transactions`
  ADD PRIMARY KEY (`id`),
  ADD KEY `order_id` (`order_id`),
  ADD KEY `user_id` (`user_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- Indexes for table `user_device_fcms`
--
ALTER TABLE `user_device_fcms`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_payments`
--
ALTER TABLE `user_payments`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_id` (`user_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `banners`
--
ALTER TABLE `banners`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `id` bigint NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `collections`
--
ALTER TABLE `collections`
  MODIFY `id` bigint NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `collection_products`
--
ALTER TABLE `collection_products`
  MODIFY `id` bigint NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `consignment_products`
--
ALTER TABLE `consignment_products`
  MODIFY `id` bigint NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `delivery_books`
--
ALTER TABLE `delivery_books`
  MODIFY `id` int NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `entity_images`
--
ALTER TABLE `entity_images`
  MODIFY `id` bigint NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=91;

--
-- AUTO_INCREMENT for table `faqs`
--
ALTER TABLE `faqs`
  MODIFY `id` bigint NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `files`
--
ALTER TABLE `files`
  MODIFY `id` bigint NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `like_products`
--
ALTER TABLE `like_products`
  MODIFY `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `log_activities`
--
ALTER TABLE `log_activities`
  MODIFY `id` bigint NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `oauth_clients`
--
ALTER TABLE `oauth_clients`
  MODIFY `id` int UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `oauth_personal_access_clients`
--
ALTER TABLE `oauth_personal_access_clients`
  MODIFY `id` int UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `orders`
--
ALTER TABLE `orders`
  MODIFY `id` bigint NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `order_product`
--
ALTER TABLE `order_product`
  MODIFY `id` bigint NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `pages`
--
ALTER TABLE `pages`
  MODIFY `id` int NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `id` bigint NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `product_sku`
--
ALTER TABLE `product_sku`
  MODIFY `id` bigint NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=55;

--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
  MODIFY `id` bigint NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `searches`
--
ALTER TABLE `searches`
  MODIFY `id` int NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `settings`
--
ALTER TABLE `settings`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=67;

--
-- AUTO_INCREMENT for table `suppliers`
--
ALTER TABLE `suppliers`
  MODIFY `id` bigint NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `team_members`
--
ALTER TABLE `team_members`
  MODIFY `id` bigint NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `transactions`
--
ALTER TABLE `transactions`
  MODIFY `id` bigint NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- AUTO_INCREMENT for table `user_device_fcms`
--
ALTER TABLE `user_device_fcms`
  MODIFY `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `user_payments`
--
ALTER TABLE `user_payments`
  MODIFY `id` bigint NOT NULL AUTO_INCREMENT;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `collection_products`
--
ALTER TABLE `collection_products`
  ADD CONSTRAINT `collection_products_ibfk_1` FOREIGN KEY (`collection_id`) REFERENCES `collections` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `collection_products_ibfk_2` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `consignment_products`
--
ALTER TABLE `consignment_products`
  ADD CONSTRAINT `consignment_products_ibfk_1` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `delivery_books`
--
ALTER TABLE `delivery_books`
  ADD CONSTRAINT `delivery_books_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `like_products`
--
ALTER TABLE `like_products`
  ADD CONSTRAINT `like_products_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `like_products_ibfk_2` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `log_activities`
--
ALTER TABLE `log_activities`
  ADD CONSTRAINT `log_activities_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `orders`
--
ALTER TABLE `orders`
  ADD CONSTRAINT `orders_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `order_product`
--
ALTER TABLE `order_product`
  ADD CONSTRAINT `order_product_ibfk_1` FOREIGN KEY (`order_id`) REFERENCES `orders` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `order_product_ibfk_2` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `products`
--
ALTER TABLE `products`
  ADD CONSTRAINT `products_ibfk_1` FOREIGN KEY (`category_id`) REFERENCES `categories` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `searches`
--
ALTER TABLE `searches`
  ADD CONSTRAINT `searches_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `team_members`
--
ALTER TABLE `team_members`
  ADD CONSTRAINT `team_members_ibfk_1` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE SET NULL ON UPDATE SET NULL;

--
-- Constraints for table `transactions`
--
ALTER TABLE `transactions`
  ADD CONSTRAINT `transactions_ibfk_1` FOREIGN KEY (`order_id`) REFERENCES `orders` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `transactions_ibfk_2` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `user_payments`
--
ALTER TABLE `user_payments`
  ADD CONSTRAINT `user_payments_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
