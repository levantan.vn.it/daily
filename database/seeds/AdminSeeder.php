<?php

use Illuminate\Database\Seeder;

class AdminSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('roles')->insert([
            'name' => 'Admin',
            'permissions' => '["Dashboard","Users","Orders","Suppliers","Products","Categories","Collections","Banners","Notifications","Messages","Members","Settings"]'
        ]);
        \DB::table('team_members')->insert([
            'name' => 'Admin',
            'email' => 'admin@avocadoo.com',
            'password' => bcrypt('123123'),
            'phone' =>  '123123',
            'address'   =>  "Hue",
            'id_number' =>  '123123',
            'status'    =>  1,
            'date_of_issue' =>  '2020-02-20',
            'role_id'   =>  1
        ]);
    }
}
