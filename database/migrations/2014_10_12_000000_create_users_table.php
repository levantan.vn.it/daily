<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('displayname')->nullable();
            $table->string('email')->unique()->nullable();
            $table->string('password')->nullable();
            $table->string('phone',20)->unique()->nullable();
            $table->string('fullname')->nullable();
            $table->date('birthday')->nullable();
            $table->enum('gender',[1,2])->nullable();
            $table->enum('verify_phone',[0,1])->nullable();
            $table->string('code_phone')->nullable();
            $table->enum('status',[1,2,3])->default(2)->nullable();
            $table->string('business_name')->nullable();
            $table->string('tax_invoice')->nullable();
            $table->string('representative')->nullable();
            $table->string('store_name')->nullable();
            $table->string('store_address',255)->nullable();
            $table->string('avatar',255)->nullable();
            $table->text('document')->nullable();
            $table->string('document_id',255)->nullable();
            $table->string('kakao_id',255)->nullable();
            $table->string('naver_id',255)->nullable();
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
