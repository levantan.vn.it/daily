<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTableDeliveryManagementTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('delivery_management', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->tinyInteger('type_delivery');
            $table->tinyInteger('distance_type');
            $table->integer('delivery_from');
            $table->integer('delivery_to');
            $table->integer('price');
            $table->text('list_area');
            $table->tinyInteger('interprovincial_type');
            $table->text('list_city');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('delivery_management');
    }
}
