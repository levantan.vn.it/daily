<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
use Illuminate\Support\Facades\Route;

Route::get('','FE\HomeController@index')->name('home');
Route::get('tin-tuc','FE\HomeController@news')->name('news');
Route::get('danh-muc-tin-tuc/{slug}','FE\HomeController@categoryNews')->name('news.category');
Route::get('tin-tuc/{slug}','FE\HomeController@detailNews')->name('news.detail');
Route::get('san-pham','FE\HomeController@products')->name('products');
Route::get('san-pham/{slug}','FE\HomeController@productsDetail')->name('products.detail');
Route::get('danh-muc-san-pham','FE\HomeController@categoryProduct')->name('products.category');
Route::get('danh-muc-san-pham/{slug}','FE\HomeController@categoryProductDetail')->name('products.category.detail');
Route::get('lien-he','FE\HomeController@contact')->name('contact');

Route::get('du-an','FE\HomeController@project')->name('projects');
Route::get('du-an/{slug}','FE\HomeController@projectDetail')->name('project.detail');

Route::get('logs', '\Rap2hpoutre\LaravelLogViewer\LogViewerController@index');
