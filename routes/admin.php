<?php

/*
|--------------------------------------------------------------------------
| Admin Routes
|--------------------------------------------------------------------------
|
| Here is where you can register admin routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use App\Http\Controllers\ProductController;
use App\Http\Resources\OrderCollection;
use Illuminate\Support\Facades\Route;
Auth::routes(['verify' => true]);
Route::post('/aiz-uploader', 'AizUploadController@show_uploader');
Route::post('/aiz-uploader/upload', 'AizUploadController@upload');
Route::get('/aiz-uploader/get_uploaded_files', 'AizUploadController@get_uploaded_files');
Route::post('/aiz-uploader/get_file_by_ids', 'AizUploadController@get_preview_files');
Route::get('/aiz-uploader/download/{id}', 'AizUploadController@attachment_download')->name('download_attachment');
Route::post('/aiz-uploader/uploadSummernote', 'AizUploadController@uploadSummernote');

Route::get('/admin/dashboard', 'HomeController@admin_dashboard')->name('admin.dashboard')->middleware(['auth', 'admin','permission']);
Route::group(['prefix' => 'admin', 'middleware' => ['auth', 'admin','preventBackHistory']], function () {
    //Update Routes
    Route::resource('categories', 'CategoryController')->middleware('permission');
    Route::resource('categories', 'CategoryController')->middleware('permission');
    Route::get('/categories/edit/{id}', 'CategoryController@edit')->name('categories.edit')->middleware('permission');
    Route::post('/categories/editpri/{id}', 'CategoryController@updatePrimary')->name('categories.editpri')->middleware('permission');
    Route::post('/categories/editchild/{id}', 'CategoryController@updateChild')->name('categories.editchild')->middleware('permission');
    Route::get('/categories/destroy/{id}', 'CategoryController@destroy')->name('categories.destroy')->middleware('permission');
    Route::get('/categories/deletechild/{id}', 'CategoryController@deletechild')->name('categories.deletechild')->middleware('permission');
    Route::post('/categories/status/{id}', 'CategoryController@updateStatus')->name('categories.status')->middleware('permission');;
    Route::post('/categories/create', 'CategoryController@CreatePrimaryCategory')->name('categories.createprimary')->middleware('permission');
    Route::post('/categories/createchild', 'CategoryController@CreateChildCategory')->name('categories.createchild')->middleware('permission');
    Route::get('/categories/view/{id}', 'CategoryController@view')->name('categories.view')->middleware('permission');
    Route::get('/category/view/{id}/primary', 'CategoryController@getCategory')->name('categories.view.primary');
    Route::post('/category/check-validate', 'CategoryController@checkValidate')->name('categories.check-validate');

	//Route Products
	Route::prefix('products')->middleware('permission')->name('products.')->group(function(){
		Route::get('/', 'ProductController@admin_products')->name('index');
		Route::get('/{id}/detail', 'ProductController@detail')->name('detail');
		// Route::get('/12', 'UserController@index')->name('create');
		Route::get('/122', 'UserController@index')->name('todays_deal');
		Route::get('/1223', 'UserController@index')->name('published');
		Route::get('/12223', 'UserController@index')->name('featured');
		Route::get('/export', 'ProductController@export')->name('export');
		Route::delete('/products-delete/{id}', 'ProductController@delete')->name('delete');
		Route::post('/{id}/update', 'ProductController@update')->name('update');
		Route::post('/update-infor/{id}','ProductController@updateInfor')->name('updateInfor');
		Route::get('/status/{id}', 'ProductController@changeStatus')->name('status');
		Route::get('/status2/{id}', 'ProductController@changeStatus2')->name('status2');
		Route::post('/toggle-status/{id}','ProductController@toggleStatus')->name('toggle-status');
		Route::post('/update-skus/{id}','ProductController@updateSkus')->name('updateSkus');
		Route::post('/update-desc{id}','ProductController@updateDesc')->name('updateDesc');
		Route::post('validate-sku', 'ProductController@validateSku')->name('validateSku');
		Route::post('store','ProductController@store')->name('store');
		Route::get('create','ProductController@create')->name('create');
		Route::post('validate-detail-product','ProductController@validateDetailProduct')->name('validateDetailProduct');
		Route::post('validate-description','ProductController@validateDescription')->name('validateDescription');
		Route::delete('/delete-title/{id}', 'ProductController@deleteTitle')->name('deleteTitle');
		Route::post('update-image/{id}','ProductController@updateImageProduct')->name('updateImageProduct');
		Route::post('validated','ProductController@validatedProduct')->name('validatedProduct');

	});

	//Route User
	Route::get('users/{id}/detail', 'UserController@detail')->name('users.detail')->middleware('permission');
  	Route::get('users/export', 'UserController@export')->name('users.export')->middleware('permission');
	// Route::get('users/detail', 'UserController@detail')->name('users.detail');
	Route::get('users/export', 'UserController@export')->name('users.export')->middleware('permission');
	Route::resource('users', 'UserController')->middleware('permission');
	Route::get('users/ban/{customer}', 'UserController@ban')->name('users.ban')->middleware('permission');
	Route::delete('/users/destroy/{id}', 'UserController@destroy')->name('users.destroy')->middleware('permission');
	Route::get('/transactions', 'UserController@transactions')->name('users.transactions')->middleware('permission');
	Route::post('users/update-status/{id}', 'UserController@changeStatus')->name('users.updatestatus')->middleware('permission');
	Route::post('users/update-status-block/{id}', 'UserController@changeStatus2')->name('users.updatestatusblock')->middleware('permission');
	Route::post('users/update-information/{id}', 'UserController@update')->name('users.update')->middleware('permission');
	Route::post('users/update-business/{id}', 'UserController@updateimg')->name('users.updateimg')->middleware('permission');
	Route::delete('/users/{id}/detail', 'UserController@deleteImg')->name('users.deleteImg')->middleware('permission');



	//Route Order
	// Route::get('orders2', 'OrderController@index2') -> name ('orders2.index');
	Route::prefix('orders')->middleware('permission')->name('orders.')->group(function(){
		Route::get('/detail/{id}', 'OrderController@detail')->name('detail');
		Route::get('/', 'OrderController@index')->name('index');
		Route::get('/create', 'OrderController@create')->name('create');
		Route::get('/export', 'OrderController@export')->name('export');
		Route::delete('/delete-order/{id}', 'OrderController@deleteOrder')->name('deleteOrder');
		Route::post('/change-status-order/{id}','OrderController@changeStatusOrder')->name('changeStatusOrder');
		Route::post('/update-delivery/{id}','OrderController@updateDelivery')->name('updateDelivery');
		Route::post('store', 'OrderController@store')->name('store');
		Route::post('update-nfor/{id}', 'OrderController@updateInfor')->name('updateInfor');
		Route::post('update-delivery-method/{id}', 'OrderController@updateDeliveryMethod')->name('updateDeliveryMethod');
		Route::post('update-reason-cancel/{id}', 'OrderController@updateReasonCancel')->name('updateReasonCancel');
		Route::post('validate-order', 'OrderController@validateOrder')->name('validateOrder');
		Route::post('validate-delivery', 'OrderController@validateDelivery')->name('validateDelivery');
		Route::post('upload-imgsend','OrderController@uploadImgSend')->name('uploadImgSend');
		Route::post('/update-info-order/{id}','OrderController@updateInfoOrder')->name('updateInfoOrder');
        Route::post('refund','OrderController@refund')->name('refund');
		Route::post('/change-status-cancel/{id}','OrderController@changeStatusCancel')->name('changeStatusCancel');
		Route::post('/update-img-reason/{id}','OrderController@updateImgReason')->name('updateImgReason');
		Route::post('change-status-all-cancel/{id}','OrderController@changeStatusAllCancel')->name('changeStatusAllCancel');
		Route::post('validate-product','OrderCollection@validateProduct')->name('validateProduct');
		Route::post('/change-status-confirm-admin/{id}','OrderController@changestatusAdminConfirm')->name('changestatusAdminConfirm');
		Route::post('/change-status-unpaid/{id}','OrderController@changeStatusUnpaid')->name('changeStatusUnpaid');
		Route::post('validated','OrderController@validatedOrder')->name('validatedOrder');
	});




	//Route Banners
	Route::prefix('banners')->middleware('permission')->name('banners.')->group(function(){
		Route::get('/', 'BannerController@index')->name('index');
		Route::get('/{id}/detail', 'BannerController@detail')->name('detail');
		Route::get('/export', 'BannerController@export')->name('export');
		Route::delete('/delete-banners/{id}', 'BannerController@delete')->name('bannersDelete');
		Route::post('/status/{id}', 'BannerController@changeStatus')->name('status');
		Route::post('/update-info/{id}','BannerController@updateInfor')->name('updateInfor');
		Route::post('/update-collection/{id}','BannerController@updateCollection')->name('updateCollection');
		Route::get('/create','BannerController@create')->name('create');
		Route::post('/store','BannerController@store')->name('store');
		Route::post('check-store','BannerController@checkStore')->name('checkStore');
		Route::post('validate-banner-info','BannerController@validateBannerInfor')->name('validateBannerInfor');
		Route::post('validate-banner-collection','BannerController@validateBannerCollection')->name('validateBannerCollection');
		Route::post('change-status-1/{id}','BannerController@changeStatus1')->name('changeStatus1');
		Route::post('change-status-2/{id}','BannerController@changeStatus2')->name('changeStatus2');
		Route::post('change-status-3/{id}','BannerController@changeStatus3')->name('changeStatus3');
		Route::post('change-status-4/{id}','BannerController@changeStatus4')->name('changeStatus4');
		Route::post('/update-images/{id}','BannerController@updateImageBanner')->name('updateImageBanner');
		Route::post('validate','BannerController@validated')->name('validated');
	});

	





	//Route Notifications
	Route::group(['prefix' => 'notifications','middleware' => 'permission'],function(){
		Route::get('/', 'NotificationController@index')->name('notifications.index');
		Route::get('detail/{id}', 'NotificationController@detail')->name('notifications.detail');
		Route::post('status/{id}', 'NotificationController@changeStatus')->name('notifications.status');
		Route::delete('destroy/{id}', 'NotificationController@destroy')->name('notifications.destroy');
		Route::get('create', 'NotificationController@create')->name('notifications.create');
		Route::post('store', 'NotificationController@store')->name('notifications.store');
		Route::post('infor/{id}', 'NotificationController@updateInfor')->name('notifications.infor');
		Route::post('descript/{id}', 'NotificationController@updateDescript')->name('notifications.descript');
		Route::post('send-notification/{id}', 'NotificationController@sendNotification')->name('notifications.send');
		Route::get('export', 'NotificationController@export')->name('notifications.export');
		Route::post('validate-infor', 'NotificationController@checkNotiInfor')->name('notifications.validateinfor');
		Route::post('validate-descript', 'NotificationController@checkNotiDescript')->name('notifications.validatedescript');
		Route::post('validate/image', 'NotificationController@validateImage')->name('notifications.validate_image');
		Route::post('validate', 'NotificationController@validated')->name('notifications.validated');
	});


	//Route Team members
	Route::get('members','TeamMembersController@index')->name('members.index')->middleware('permission');
	Route::get('members/create','TeamMembersController@create')->name('members.create')->middleware('permission');
	Route::post('members/store','TeamMembersController@store')->name('members.store')->middleware('permission');
	Route::get('members/edit/{id}','TeamMembersController@edit')->name('members.edit')->middleware('permission');
	Route::post('members/updateInfor/{id}','TeamMembersController@updateInfor')->name('members.updateInfor')->middleware('permission');
	Route::post('members/updateRole/{id}','TeamMembersController@updateRole')->name('members.updateRole')->middleware('permission');
	Route::get('members/export','TeamMembersController@export')->name('members.export')->middleware('permission');
	Route::post('members/status/{id}', 'TeamMembersController@changeStatus')->name('members.status')->middleware('permission');
	Route::post('members/block/{id}', 'TeamMembersController@blockMember')->name('members.blocked')->middleware('permission');
	Route::delete('members/destroy/{id}', 'TeamMembersController@destroy')->name('members.destroy')->middleware('permission');
	Route::post('members/change-password', 'TeamMembersController@changePassword')->name('members.change-password')->middleware('permission');
	Route::post('members/validate-infor', 'TeamMembersController@validateMemberInfor')->name('members.validate-infor')->middleware('permission');
	Route::post('members/validate-roles', 'TeamMembersController@validateMemberRole')->name('members.validate-roles')->middleware('permission');
	Route::post('members/validate/image', 'TeamMembersController@validateImage')->name('members.validate_image')->middleware('permission');
	Route::post('members/validate', 'TeamMembersController@validated')->name('members.validated')->middleware('permission');






    Route::resource('profile', 'ProfileController');


    Route::resource('roles', 'RoleController')->middleware('permission');
    Route::get('/roles/edit/{id}', 'RoleController@edit')->name('roles.edit')->middleware('permission');
    Route::get('/roles/destroy/{id}', 'RoleController@destroy')->name('roles.destroy')->middleware('permission');

    // uploaded files
    Route::any('/uploaded-files/file-info', 'AizUploadController@file_info')->name('uploaded-files.info');
    Route::resource('/uploaded-files', 'AizUploadController');
    Route::get('/uploaded-files/destroy/{id}', 'AizUploadController@destroy')->name('uploaded-files.destroy');

    Route::resource('collections', 'CollectionController')->middleware('permission');
    Route::group(['prefix' => 'collections','middleware' => 'permission'],function(){
        Route::get('information/{id}', 'CollectionController@information')->name('collections.information');
        Route::post('status/{id}', 'CollectionController@changeStatus')->name('collections.status');
        Route::post('infor/{id}', 'CollectionController@updateInfo')->name('collections.info');
        Route::post('product/{id}', 'CollectionController@addProduct')->name('collections.product');
        Route::post('product/destroy/{id}', 'CollectionController@deleteProduct')->name('collections.producDel');
        Route::post('validate', 'CollectionController@validateCollection')->name('collections.validate');
		Route::post('validate/image', 'CollectionController@validateImage')->name('collections.validate_image');
        Route::get('export/download', 'CollectionController@export')->name('collections.export');
		Route::post('validate-general', 'CollectionController@validated')->name('collections.validated');
    });


    // Suppliers
    Route::get('suppliers/{id}/detail', 'SupplierController@detail')->name('suppliers.detail')->middleware('permission');
    Route::post('suppliers/supplieraddinfo', 'SupplierController@supplieraddinfo')->name('suppliers.supplieraddinfo')->middleware('permission');
    Route::get('suppliers/export/', 'SupplierController@export')->name('suppliers.export')->middleware('permission');
    Route::resource('suppliers', 'SupplierController')->middleware('permission');
    Route::post('suppliers/update-status/{id}', 'SupplierController@toggleStatus')->name('suppliers.updatestatus')->middleware('permission');
    // Route::post('suppliers/update-detail-status/{id}', 'SupplierController@changeStatus')->name('suppliers.updatedetailstatus');
    Route::post('suppliers/update-information/{id}', 'SupplierController@update')->name('suppliers.update')->middleware('permission');
    Route::post('suppliers/update-image/{id}', 'SupplierController@updateImage')->name('suppliers.updateimg')->middleware('permission');
    Route::post('suppliers/create-info/', 'SupplierController@createinfo')->name('suppliers.createinfo');
    Route::post('suppliers/check-create', 'SupplierController@checkCreate')->name('suppliers.check-create');
	Route::post('suppliers/validate-info', 'SupplierController@validateInfo')->name('suppliers.validateInfo');


    //Setting
    Route::group(['prefix' => 'settings','middleware' => 'permission'], function () {
        Route::get('/', 'SettingsController@index')->name('settings.index');
        Route::get('company-information', 'SettingsController@companyInfo')->name('settings.company');
        Route::post('company-information', 'SettingsController@updateCompany')->name('settings.company.update');
        Route::get('term-service', 'SettingsController@termService')->name('settings.service');
        Route::post('term-service', 'SettingsController@termServiceUpdate')->name('settings.service.update');
        Route::get('privacy-policies', 'SettingsController@privacyPolicies')->name('settings.policies');
        Route::post('privacy-policies', 'SettingsController@privacyPoliciesUpdate')->name('settings.policies.update');
        Route::get('faq', 'SettingsController@faqIndex')->name('settings.faq');
		Route::post('faq/title/{id}', 'SettingsController@updateFaqTitle')->name('settings.faq.updateFaqTitle');
        Route::post('faq', 'SettingsController@storeTitleFaq')->name('settings.faq.store');
        Route::delete('faq/destroy/{id}', 'SettingsController@destroyFaq')->name('settings.faq.destroy');
        Route::post('faq/{id}', 'SettingsController@updateAnswerFaq')->name('settings.faq.update');
        Route::get('role', 'SettingsController@roleIndex')->name('settings.role');
        Route::post('role', 'SettingsController@roleStore')->name('settings.role.store');
        Route::post('role/title/{id}', 'SettingsController@roleUpdateTitle')->name('settings.role.updateTitle');
        Route::delete('role/destroy/{id}', 'SettingsController@destroyRole')->name('settings.role.destroy');
        Route::post('role/{id}', 'SettingsController@roleUpdate')->name('settings.role.update');
        Route::get('weight-formula', 'SettingsController@weightManagement')->name('settings.weight');
        Route::get('distance-formula', 'SettingsController@distanceManagement')->name('settings.distance');
        Route::post('distance-formula/km', 'SettingsController@updateOrInsertDistance')->name('settings.updateOrInsertDistance');
        Route::post('distance-formula/area', 'SettingsController@updateOrInsertArea')->name('settings.updateOrInsertArea');
        Route::get('interprovincial-delivery', 'SettingsController@interprovincialManagement')->name('settings.interprovincial');
        Route::post('interprovincial-delivery', 'SettingsController@updateOrInsertInter')->name('settings.updateOrInsertInter');
        Route::post('weight-formula', 'SettingsController@updateOrInsert')->name('settings.updateOrInsert');
        Route::delete('delivery-management/destroy/{id}', 'SettingsController@destroyRange')->name('settings.destroyRange');

        Route::get('delivery-company', 'SettingsController@deliveryCompany')->name('settings.delivery-company');
        Route::post('delivery-company', 'SettingsController@deliveryCompanyPost')->name('settings.delivery-company.post');

        Route::get('delivery-condition', 'SettingsController@deliveryCondition')->name('settings.delivery-condition');
        Route::post('delivery-condition', 'SettingsController@deliveryConditionPost')->name('settings.delivery-condition.post');
        Route::delete('delivery-condition/destroy/{id}', 'SettingsController@destroyCondition')->name('settings.destroyCondition');
    });

    Route::post('upload_tmp', 'AizUploadController@upload_tmp')->name('upload_tmp');
    Route::post('update-image', 'AizUploadController@updateImage')->name('upload.update-image');

    // message
    Route::get('/messages', 'MessageController@index')->name('messages.index');
    Route::post('/message/{id}',  'MessageController@sendMessge');
    // get user detail
    Route::get('/user/detail/{id}', 'MessageController@detail');
    // api get list user
    Route::get('/listUser', 'MessageController@listOfUser');
    Route::get('/message/getUser', 'MessageController@getUser');
    // api get product user
    Route::get('products/detail/{id}', 'MessageController@detailProduct')->name('detailproduct');
    Route::get('members/detail-member/{id}', 'MessageController@detailMember')->name('members.detail');

});
Route::group(['prefix' => 'ajax'], function(){
    Route::post('get-districts', 'AjaxController@getDistrict')->name('ajax.get.district');
    Route::post('get-wards', 'AjaxController@getWard')->name('ajax.get.ward');
});
