<?php

use Illuminate\Support\Facades\Route;


Route::prefix('v1')->group(function () {
    Route::post('login', 'Api\AuthController@login');
    Route::post('register', 'Api\AuthController@signup');
    Route::post('verify-phone', 'Api\AuthController@verifyPhone');
    Route::post('login-sns', 'Api\AuthController@loginSNS');
    Route::post('change-password', 'Api\AuthController@changePassword');
    Route::post('check-phone', 'Api\AuthController@checkPhone');
    Route::post('check-email', 'Api\AuthController@checkEmail');
    Route::post('check-sns', 'Api\AuthController@checkSNS');


    Route::middleware(['auth:api', 'jwt_auth'])->group(function () {
        Route::post('upload-image', 'Api\UploadImageController@upload');
        Route::get('logout', 'Api\AuthController@logout');
        Route::get('user-detail', 'Api\UserController@user');
        Route::post('fcm-token', 'Api\UserController@addFcmToken');
        Route::delete('fcm-token/{token}', 'Api\UserController@deleteFcmToken');
        Route::post('create-business', 'Api\AuthController@createBusiness');
        Route::delete('delete-document/{id}', 'Api\UserController@deleteDocument');
        Route::get('user-info/{id}', 'Api\UserController@info');
        Route::post('update-avatar', 'Api\UserController@updateAvatar');
        Route::post('update-profile', 'Api\UserController@updateProfile');
        Route::post('update-business', 'Api\UserController@updateBusiness');
        Route::get('notification/unread/count', 'Api\UserController@countNotificationUnread');
        Route::post('user/change-password', 'Api\UserController@changePassword');
        Route::get('notifications', 'Api\UserController@notifications');
        Route::post('notifications/read', 'Api\UserController@markAsReadAllNotification');
        Route::post('notifications/read/{id}', 'Api\UserController@markAsReadNotification');

        // User payment method
        Route::get('payment-method', 'Api\UserPaymentController@getPaymentMethod');
        Route::post('payment-method', 'Api\UserPaymentController@addPaymentMethod');
        Route::put('payment-method/{id}', 'Api\UserPaymentController@editPaymentMethod')->where('id', '[0-9]+');;
        Route::delete('payment-method/{id}', 'Api\UserPaymentController@deletePaymentMethod')->where('id', '[0-9]+');;
        Route::post('make-primary-payment-method/{id}', 'Api\UserPaymentController@makePrimaryPaymentMethod')->where('id', '[0-9]+');

        // User delivery-books
        Route::get('location', 'Api\DeliveryBookController@getLocation');
        Route::post('delivery-book', 'Api\DeliveryBookController@addDeliveryBook');
        Route::put('delivery-book/{id}', 'Api\DeliveryBookController@editDeliveryBook')->where('id', '[0-9]+');
        Route::delete('delivery-book/{id}', 'Api\DeliveryBookController@deleteDeliveryBook')->where('id', '[0-9]+');
        Route::post('make-primary-delivery-book/{id}', 'Api\DeliveryBookController@makePrimaryDeliveryBook')->where('id', '[0-9]+');

        // Searches
        Route::get('recent-search', 'Api\SearchController@recent');
        Route::post('recent-search', 'Api\SearchController@addRecent');
        Route::delete('recent-search', 'Api\SearchController@deleteRecent');

        // Products
        Route::post('like-product/{id}', 'Api\ProductController@likeProductAction')->where('id', '[0-9]+');
        Route::get('like-product', 'Api\ProductController@getLikeProduct');

        // Order
        Route::post('add-order', 'Api\OrderController@store');
        Route::get('order-product/{id}', 'Api\OrderController@orderProduct');
        Route::get('history-order', 'Api\OrderController@historyOrder');
        Route::get('order-detail/{id}', 'Api\OrderController@orderDetail');
        Route::get('bought-product', 'Api\ProductController@getBoughtProduct');
        Route::get('orders-processing/count', 'Api\ProductController@countOrdersProcessing');

        Route::post('create-order', 'Api\OrderController@createOrder');
        Route::post('check-payment', 'Api\OrderController@checkPayment');

        Route::post('deliveried-order', 'Api\OrderController@deliveried');

        // Cart
        Route::get('cart', 'Api\CartController@getCart');
        Route::post('cart', 'Api\CartController@add');
        Route::put('cart/{id}', 'Api\CartController@update')->where('id', '[0-9]+');
        Route::delete('cart/{id}', 'Api\CartController@delete')->where('id', '[0-9]+');
        Route::get('delivery-type', 'Api\CartController@deliveryType');
        Route::post('delivery-fee', 'Api\CartController@deliveryFee');

        // Repurchase
        Route::post('repurchase', 'Api\CartController@repurchase');

        Route::get('check-stock', 'Api\OrderController@checkStock');
    });
    Route::get('collections', 'Api\HomeController@collections');
    Route::get('collection-detail/{id}', 'Api\HomeController@collectionDetail');
    Route::get('banners', 'Api\HomeController@banners');
    Route::get('categories', 'Api\HomeController@categories');
    Route::get('category-detail/{id}', 'Api\HomeController@categoryDetail');
    Route::get('brands', 'Api\HomeController@brands');
    Route::get('brand-detail/{id}', 'Api\HomeController@brandDetail');
    Route::get('settings', 'Api\HomeController@settings');
    Route::get('products', 'Api\ProductController@index');
    Route::get('product-detail/{id}', 'Api\ProductController@show');
    Route::get('price-range', 'Api\ProductController@getRangePrice');

    Route::get('suggest-search', 'Api\SearchController@suggest');

    Route::get('popular-search', 'Api\SearchController@popular');
    Route::post('popular-search/{id}', 'Api\SearchController@addPopular');
    // Faq
    Route::get('faqs', 'Api\FaqController@index');

    Route::get('cities', 'Api\HomeController@cities');
    Route::get('districts/{city_id}', 'Api\HomeController@districts');
    Route::get('wards/{district_id}', 'Api\HomeController@wards');

    Route::get('delivery-company', 'Api\HomeController@deliveryCompany');

    Route::get('delivery-condition', 'Api\CartController@getDeliveryCondition');
});

Route::fallback(function () {
    return response()->json([
        'data' => [],
        'success' => false,
        'status' => 404,
        'message' => 'Invalid Route'
    ]);
});
