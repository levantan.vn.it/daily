<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ConsignmentProduct extends Model
{
    protected $table = 'consignment_products';
}
