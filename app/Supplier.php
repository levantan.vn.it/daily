<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Supplier extends Model
{
    protected $table = 'suppliers';

    protected $fillable = [
        'status',
        'name',
        'address',
        'location',
        'email',
        'phone',
        'representative',
        'images',
        'id_images',
        'main_image_id',
        'main_image',
    ];

    public function products()
    {
        return $this->hasMany(Product::class, 'supplier_id');
    }

    public function statusText()
    {
        if ($this->status == 1) {
            return __('suppliers.active');
        } elseif ($this->status == 2) {
            return __('suppliers.inactive');
        }
        return "";
    }

    public function isActive()
    {
        return intval($this->status) === 1;
    }
    public function isInActive()
    {
        return intval($this->status) === 2;
    }

    public function getImagesAttribute($value)
    {
        return $value ? get_asset_from_list($value) : [];
    }

    public function orders()
    {
        return $this->products()
            ->select('orders.*')
            ->rightJoin('order_product', 'order_product.product_id', 'products.id')
            ->rightJoin('orders', 'orders.id', 'order_product.order_id')->distinct();
    }
}
