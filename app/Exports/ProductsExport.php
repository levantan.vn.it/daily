<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithMapping;


class ProductsExport implements FromCollection,WithHeadings,ShouldAutoSize,WithMapping
{

    private $data;

    public function __construct($data)
    {
        $this->data = $data;
    }

    /**
     * @return \Illuminate\Support\Collection
     */
    public function collection()
    {
        return $this->data;
    }
        /**
     * Set header columns
     *
     * @return array
     */
    public function headings(): array
    {
        return [
            '상품 ID',
            '상품명',
            'SKUs',
            '종류',
            '가격',
            '마지막 수정일',
            '수량',
            '상태',
        ];
    }
    /**
     * Mapping data
     *
     * @return array 
     */
    public function map($product): array
    {
        $variants = $product->parse_variant;
        // dd(count($variants['title'])?? []);
        return [
            $product->id,
            $product->name,
            $product->skus->count('sku'),
            count($variants['title'] ?? []),
            $product->skus->min('price'),
            $product->updated_at->format('Y/m/d'),
            $product->skus->sum('qty'),
            optional($product)->checkStatus(),
        ];
    }

}
