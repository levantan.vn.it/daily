<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cart extends Model
{
    protected $table = 'carts';
    protected $fillable = [
        'user_id',
        'product_id',
        'product_sku_id',
        'qty',
        'delivery_condition_id'
    ];

    public function product()
    {
        return $this->belongsTo(Product::class, 'product_id')->withTrashed();
    }

    public function delivery_condition()
    {
        return $this->belongsTo(DeliveryCondition::class, 'delivery_condition_id');
    }

    public function productSku()
    {
        return $this->belongsTo(ProductSKU::class, 'product_sku_id')->withTrashed();
    }
}
