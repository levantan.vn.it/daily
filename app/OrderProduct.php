<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OrderProduct extends Model
{
    protected $table = 'order_product';

    protected $fillable = [
        'product_id',
        'order_id',
        'product_sku_id',
        'qty',
        'subtotal',
        'variant',
        'delivery_condition_id'
    ];

    public function order()
    {
        return $this->belongsTo(Order::class);
    }

    public function product()
    {
        return $this->belongsTo(Product::class)->withDefault();
    }

    public function productSku()
    {
        return $this->belongsTo(ProductSKU::class)->withDefault();
    }

    public function getParseVariantAttribute()
    {
        try {
            return $this->variant ? unserialize($this->variant) : ['title'=>[], 'value'=>[]];
        } catch (\Exception $e) {
            return ['title'=>[], 'value'=>[]];
        }
    }
    public function getSkuAttribute()
    {
        // dd($this->parse_variant);

        try {
            return $this->productSku->sku ?? ('#PRO' . $this->product_id . join('', $this->parse_variant['value'] ?? []));
        } catch (\Exception $e) {
            return '#PRO' . $this->product_id;
        }

        // return $this->productSku()->sku ?? '';
        // return '#' . $this->product_id . ($this->parse_variant['Origin']??'') . ($this->parse_variant['Size']??'');
    }
    public function getSizeAttribute()
    {
        // try {
        //     return (join('', $this->parse_variant['value'] ?? [])) ;
        // } catch (\Exception $e) {
        //     return '';
        // }
        try {
            return $this->productSku->price ;
        } catch (\Exception $e) {
            return '';
        }
        // // dd($this->parse_variant);
        // // return $this->parse_variant['value'][1] ?? [];
        // try {
        //     return $this->productSku->sku ?? ('#PRO' . $this->product_id . join('', $this->parse_variant['value'] ?? []));
        // } catch (\Exception $e) {
        //     return '#PRO' . $this->product_id;
        // }
        // return implode($this->parse_variant['value'] ?? []);

    }
    public function getOriginAttribute()
    {
        return array_key_exists('origin', $this->parse_variant) ? $this->parse_variant['origin'] : '';
    }

    public function scopeWithAllRelation($query)
    {
        return $query->with(['product' => function ($q) {
            $q->withoutGlobalScope('skuMinPrice')->with(['skus' => function ($query) {
                $query->withTrashed();
            }])->withTrashed();
        }, 'productSku' => function ($q) {
            $q->withTrashed();
        }]);
    }
}
