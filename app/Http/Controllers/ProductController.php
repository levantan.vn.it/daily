<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Product;
use App\ProductTranslation;
use App\ProductStock;
use App\Category;
use App\Exports\ProductsExport;
use App\Http\Requests\ProductRequest;
use App\Http\Requests\ProductSkuRequest;
use App\Http\Requests\ProductsRequest;
use App\Http\Requests\ProductDescriptionRequest;
use App\Http\Requests\ProductImage;
use Maatwebsite\Excel\Facades\Excel;
use App\Language;
use App\ProductSKU;
use Auth;
use App\SubSubCategory;
use App\Supplier;
use Session;
use ImageOptimizer;
use DB;
use CoreComponentRepository;
use Illuminate\Support\Str;
use Artisan;

class ProductController extends Controller
{
    public $entity2;
    public $product_sku;
    public function __construct()
    {
        $this->entity2 = "Product";
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('backend.products.index', compact('data'));
    }

     public function admin_products(Request $request)
    {
        //CoreComponentRepository::instantiateShopRepository();
        $type = 'In House';
        $col_name = null;
        $query = null;
        $sort_search = null;
        // $products = optional();
        $customers = optional();
        // $products = Product::table('products')
        //         ->orderBy('id', 'desc')
        //         ->get();
        // $products = Product::with(['skus'])->paginate(15);


        // $products = Product::where('added_by', 'admin');

        // if ($request->type != null){
        //     $var = explode(",", $request->type);
        //     $col_name = $var[0];
        //     $query = $var[1];
        //     $products = $products->orderBy($col_name, $query);
        //     $sort_type = $request->type;
        // }
        // if ($request->search != null){
        //     $products = $products
        //                 ->where('name', 'like', '%'.$request->search.'%');
        //     $sort_search = $request->search;
        // }

        // $products = $products->where('digital', 0)->orderBy('created_at', 'desc')->paginate(15);

        $products = $this->getData($request, $sort_search, 1);


        return view('backend.products.index', compact('products', 'customers', 'type', 'col_name', 'query', 'sort_search'));
    }


    public function export(Request $request)
    {
        $data = $this->getData($request, 1);
        return Excel::download(new ProductsExport($data), '상품관리.xlsx');
    }


    public function detail($id)
    {
        $supplier = Supplier::all();
        $category = Category::all();
        $products = Product::findOrFail($id);
        $product_sku = ProductSKU::where('product_id',$id)->get();
        $check_sku_title = ProductSKU::where('product_id',$id)->first();
        $this->product_sku = [];
        $this->product_sku_title = [];
        $product_sku_title = [];
        $this->variants = [];
        $variant_sku = [];

        if(!count($product_sku)){
            $new_variant = unserialize($products->variant);
            $count = count($new_variant['title']);
            foreach (($new_variant['value'][0] ?? []) as $value) {
                // SKU
                $sku = '#PRO'.$products->id.$value;
                $variant = [];
                $variant[] = $value;
                $this->renderSku($sku,$new_variant['value'],$count,1,$sku,$variant,$variant);
                // title
                $title = $new_variant['title'][0].": ".$value;
                $this->renderSkuTitle($title,$new_variant['value'],$count,1,$title,$new_variant['title']);
            }
            $product_sku = $this->product_sku;
            $product_sku_title = $this->product_sku_title;
            // $variants = $this->variants;
        }elseif(empty($check_sku_title->title)){
            $new_variant = unserialize($products->variant);
            $count = count($new_variant['title']);
            foreach (($new_variant['value'][0] ?? []) as $value) {
                // list variant
                $sku = '#PRO'.$products->id.$value;
                $variant = [];
                $variant[] = $value;
                $this->renderSku($sku,$new_variant['value'],$count,1,$sku,$variant,$variant);
                // title
                $title = $new_variant['title'][0].": ".$value;
                $this->renderSkuTitle($title,$new_variant['value'],$count,1,$title,$new_variant['title']);
            }
            $product_sku_title = $this->product_sku_title;
            // $variants = $this->variants;
        }elseif(empty($check_sku_title->variant)){
            $new_variant = unserialize($products->variant);
            $count = count($new_variant['title']);
            foreach (($new_variant['value'][0] ?? []) as $value) {
                // list variant
                $sku = '#PRO'.$products->id.$value;
                $variant = [];
                $variant[] = $value;
                $this->renderSku($sku,$new_variant['value'],$count,1,$sku,$variant,$variant);
            }
            // $variants = $this->variants;
        }
        if(!empty($this->variants) && !empty($new_variant)){
            $title = $new_variant['title'];
            foreach ($this->variants as $key) {
                $item = [
                    'title' =>  $title,
                    'value' =>  $key
                ];
                $variant_sku[] = serialize($item);
            }
        }
        // dd($product_sku);
        // $variants = $products->parseVariant;
        // dd($variants[0]);
        $images = explode(",", $products->images);
        $data = [
            'products' => $products,
            'category' => $category,
            'supplier' => $supplier,
            'images'    =>  $images,
            'product_sku'=> $product_sku,
            'product_sku_title' =>  $product_sku_title,
            'variant_sku'  =>  $variant_sku
        ];
        return view('backend.products.detail',$data );
        // return view('backend.products.detail');
    }
    public function renderSku($sku,$new_variant,$count,$index,$old_sku,$variant,$old_variant){
        if($index >= $count){
            $this->product_sku[] = $sku;
            $this->variants[] = $variant;
            return true;
        }
        $old_sku = $sku;
        $old_variant = $variant;
        foreach (($new_variant[$index]?? []) as $value) {
            $sku = $old_sku;
            $variant = $old_variant;
            $sku .= $value;
            $variant[] = $value;
            $this->renderSku($sku,$new_variant,$count,$index+1,$old_sku,$variant,$old_variant);
        }
    }
    public function renderSkuTitle($title,$new_variant,$count,$index,$old_title,$new_title){
        if($index >= $count){
            $this->product_sku_title[] = $title;
            return true;
        }
        $old_title = $title;
        foreach (($new_variant[$index]?? []) as $value) {
            $title = $old_title;
            $title .= ", ".$new_title[$index].": ".$value;
            $this->renderSkuTitle($title,$new_variant,$count,$index+1,$old_title,$new_title);
        }        
    }

    private function recursiveRemoveNullArray($array){
        $newArr = $array;
        foreach(($newArr ?? []) as $key => $item){
            if(gettype($item) == 'array'){
                $newArr[$key] = $this->recursiveRemoveNullArray($item);
            }else{
                if(!$item){
                    unset($newArr[$key]);
                }
            }
        }
        return $newArr;
    }

    public function updateInfor(ProductRequest $request, $id)
    {
        $product = Product::findOrFail($id);
        $product->update($request->only('category_id','name','supplier_id'));
        $product->save();
        $arrNotNull = $this->recursiveRemoveNullArray($request->variant ?? []);
        $title = $arrNotNull['title'];
        $value = $arrNotNull['value'];

        $newValue = $request->value;
        foreach( ($newValue ?? []) as $key => $item){
            foreach( ($value ?? []) as $k => $it ){
                if($key == $k){
                    $value[$key] = array_merge($value[$key], $newValue[$key]);
                }
            }
        }

        //         // $value = array_map('array_filter', $value);
        // // $value = array_filter($value,function($value1) { return !empty($value1); });
        // $variants = $product->parseVariant;
        // // dd($variants);
        // if(count($variants)){
        //     $check = false;
        //     foreach ($value as $key => $vl) {
        //         $result = array_diff_assoc($vl, $variants['value'][$key]);
        //         $result2 = array_diff_assoc($variants['value'][$key],$vl);
        //         if(count($result) || count($result2)){
        //             $check  = true;
        //             break;

        // $value = array_map('array_filter', $value);
        // $value = array_filter($value,function($value1) { return !empty($value1); });
        $variants = $product->parseVariant;
        if(count($variants)){
            if(count($variants['title']) != count($title)){
                $variant = [
                    'title' =>  $title,
                    'value' =>  $value
                ];
                $product->variant = serialize($variant);
                $product->save();
            }
            if($variants != $request->variant)
            {
                ProductSKU::where('product_id',$id)->delete();
            }
            

            $variant = [
                'title' =>  $title,
                'value' =>  $value,
            ];
            $product->variant = serialize($variant);
            $product->save();
        }
        flash(__('products.update-info-success'))->success();
        return redirect()->back();
    }



    public function validateDescription(Request $request){
        $request->validate([
            'description'=> 'required',
        ]);
    }

    public function validateSku(Request $request)
    {
        $request->validate([
            'sku' => 'bail|required|array',
            'title' => 'bail|required|array',
            'weight'    => 'required|array',
            'weight.*'     => 'required|min:0',
            'price'     => 'required|array',
            'price.*'     => 'required|min:0',
            'qty'       =>  'required|array',
            'qty.*'     => 'required|min:0',
            'sale' =>'required|min:0|max:90',
            'sale.*' =>'required|min:0|max:90',
        ]);
    }

    public function updateSkus(ProductSkuRequest $request, $id){
        $product = Product::findOrFail($id);
        $skus = $request->sku;
        $weights = $request->weight;
        $prices = $request->price;
        $qty = $request->qty;
        $sale=$request->sale;
        $title=$request->title;
        foreach($skus as $key => $sku){
            $product->skus()->updateOrCreate([
                'sku'=>$sku,
            ],[
                'sku'=>$sku,
                'title'=> $title[$key]??'',
                'weight'=> $weights[$key]??'',
                'price' => $prices[$key]??'',
                'qty'   => $qty[$key]??'',
                'sale'   => $sale[$key]??'',
                'status'    =>  $request->status[$key]??'',
                'variant'=> $request->variant[$key]??'',
            ]);
        }
        // $product->skus()->update([
        //     'weight'=> $request->weight,
        //     'price' => $request->price,
        //     'qty'   => $request->qty,
        // ]);
        flash(__('products.update-sku-success'))->success();
        return redirect()->back();
    }
    public function delete($id)
    {
        $products = Product::findOrFail($id);
        $products->skus()->delete();
        $result = $products->delete();
        flash(__('products.delete-success'))->success();
        if ($result) {
            return response()->json(true, 200);
        }
        return response()->json(false, 422);
    }

    public function deleteTitle($id){
        $title = Product::findOrFail($id);
        $result = $title->delete();
        flash(__('products.delete-success'))->success();
        if ($result) {
            return response()->json(true, 200);
        } else {
            return response(false, 422);
        }
    }

    public function changeStatus2($id) {
        $products = Product::findOrFail($id);
        if($products->status == 1) {
            $products->status = 2;
            flash(__('products.change-status-success'))->success();
        } else {
            $products->status = 1;
            flash(__('products.change-status-success'))->success();
        }
        $result= $products->save();
        // return redirect()->route('products.index');
        if ($result) {
            return response()->json(true, 200);
        }
        return response()->json(false, 422);
    }
    public function toggleStatus($id) {
        $product = Product::findOrFail($id);
        $product->update([
            'status'=> $product->isActive() ? 2 : 1
        ]);
        flash(__('products.change-status-success'))->success();

        return response()->json();
    }


    /**
     * xuất dữ liệu exprort, tìm kiếm
     */
    private function getData($request, $is_export = 0, $is_paginate = 0)
    {
        $products = Product::where(function ($query) use ($request) {
            if ($request->search) {
                $query->where(function ($q) use ($request) {
                    $q->where('id', 'LIKE', "%$request->search%")->orWhere('name', 'LIKE', "%$request->search%");
                });
            }
            if ($request->date) {
                $query->whereDate('updated_at',$request->date);
            }
            if ($request->status) {
                $query->where('status', $request->status);
            }
        })->latest();

        if ($is_export) {
            $products = $products->get();

        } else {
            $products = $products->paginate(15);
        }

        return $products;
    }
    // public function changeStatus($id, Request $request)
    // {
    //     $product = Product::find($id);

    //     $product->status = $request['status'];
    //     $product->save();
    //     return redirect()->route('products.index');
    // }
    public function changeStatus($id) {
        $products = Product::findOrFail($id);
        if($products->status == 1) {
            $products->status = 2;
            flash(__('products.change-status-success'))->success();
        } else {
            $products->status = 1;
            flash(__('products.change-status-success'))->success();
        }
        $products->save();
        return back();
    }



    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $supplier = Supplier::all();
        $category = Category::all();
        $data = [
            'supplier' => $supplier,
            'category' => $category,
        ];
        // $categories = Category::where('parent_id', 0)
        //     ->where('digital', 0)
        //     ->with('childrenCategories')
        //     ->get();

        // return view('backend.product.products.create', compact('categories'));
        return view('backend.products.create',$data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ProductsRequest $request)
    {
        // dd($request->all());
        $product= new Product;
        $product->name=$request->name;
        $product->category_id=$request->category_id;
        $product->supplier_id=$request->supplier_id;
        $product->variant = serialize($request->variant);
        $variants = $request->variant;
        $images=[];
        if(!empty($request->id_files)){
            $id_files = explode(",",$request->id_files);
            if(!empty($id_files)&& count($id_files)){
                foreach($id_files as $value){
                    $url=api_asset($value);
                    if(!empty($url)){
                        $images[]=$url;
                    }
                }
            }
        };
        $product->images = implode(",",$images);
        $product->id_images= ltrim($request->id_files,"0");
        $product->description=$request->description;
        $product->save();
        flash(__('products.create-success'))->success();


        return redirect()->route('products.detail',['id'=>$product->id]);
    }

    public function updateImageProduct(ProductImage $request,$id)
    {
        $product = Product::findOrFail($id);
        $images=[];
        if(!empty($request->id_files)){
            $id_files = explode(",",$request->id_files);
            if(!empty($id_files)&& count($id_files)){
                foreach($id_files as $value){
                    $url=api_asset($value);
                    if(!empty($url)){
                        $images[]=$url;
                    }
                }
            }
        };
        $product->images = implode(",",$images);
        $product->id_images= ltrim($request->id_files,"0");
        $product->save();
        flash(__('products.update-image-success'))->success();
        return back();


    }

    public function validateDetailProduct(Request $request)
    {
        $request->validate([
            'name' => 'required',
            'category_id'=>'required',
            'supplier_id'=>'required',
            'variant.title.*'=>'required',
            'variant.value.*.*'=>'required',
        ]);
        return response()->json();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $product = Product::findOrFail($id);

    }

    public function updateDesc(ProductDescriptionRequest $request, $id)
    {
        $product = Product::findOrFail($id);
        $product->description = $request->description;
        flash(__('products.description-success'))->success();
        $product->save();
        return back();
    }

    public function validatedProduct(ProductsRequest $request)
    {
        return response()->json();
    }

}
