<?php

namespace App\Http\Controllers\FE;

use Illuminate\Http\Request;
use Session;
use Auth;
use Hash;
use App\Category;
use App\Product;
use App\User;
use App\Order;
use App\Setting;
use App\Transaction;
use Carbon\Carbon;
use ImageOptimizer;
use Cookie;
use Illuminate\Support\Str;
use Mail;

class HomeController
{
    public $time;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->time = 0;
        //$this->middleware('auth');
    }

    /**
     * Show the admin dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('frontend.index');
    }

    public function news()
    {
        return view('frontend.news.index');
    }

    public function categoryNews()
    {
        return view('frontend.news.category');
    }

    public function detailNews()
    {
        return view('frontend.news.detail');
    }

    public function products()
    {
        return view('frontend.products.index');
    }

    public function productsDetail()
    {
        return view('frontend.products.detail');
    }
    public function categoryProduct()
    {
        return view('frontend.products.category');
    }

    public function contact()
    {
        return view('frontend.contact');
    }


}
