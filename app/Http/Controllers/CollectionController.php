<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Collection;
use App\Exports\CollectionsExport;
use App\Http\Requests\CollectionRequest;
use App\Http\Requests\CollectionsRequest;
use App\Http\Requests\ImageRequest;
use App\Product;
use Illuminate\Support\Facades\Validator;
use Maatwebsite\Excel\Facades\Excel;

class CollectionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index(Request $request)
    {
        $search = null;
        $data = $this->getData($request,$search,1);
        return view('backend.collections.index',[
            'data'  =>  $data
        ]);
    }

    private function getData($request,$is_export=0,$search = null,$is_paginate = 0)
    {
        $search = $request->search;
        $data = Collection::with('products');
        if($search){
            $data = $data->where(function($q) use($search){
                $q->where('id','LIKE', "%$search%")
                ->orWhere('name', 'LIKE', "%$search%");
            });
        }
        if($request->joined_date){
            $data = $data->whereDate('created_at', $request->joined_date);
        }
        if($request->status){
            $data = $data->where('status', $request->status);
        }
        if($is_export){
            $data = $data->latest()->get();
        }else{
            $data = $data->latest()->paginate(10);
        }
        return $data;
    }

    public function export(Request $request){
        $data= $this->getData($request,1);
        return Excel::download(new CollectionsExport($data),'컬렉션 관리.xlsx');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $collection = new Collection;
        if($request->ajax()){
            if($request->brand_id) {
                $allproduct = Product::where('name','like','%'.$request->get('search','').'%')
                ->where('supplier_id',$request->brand_id)->whereHas('skus',function($query){
                    $query->whereNotNull('product_id')->where('qty','>','0');
                })->get();
            }else {
                $allproduct = Product::where('name','like','%'.$request->get('search','').'%')->whereHas('skus',function($query){
                    $query->whereNotNull('product_id')->where('qty','>','0');
                })->get();
            }
            return view('templates.popup.all-products-create',compact('allproduct'));
        }
        return  view('backend.collections.create',compact('collection'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CollectionsRequest $request)
    {

        $collection = new Collection;
        $collection->name = $request->name;
        $collection->section = $request->section;
        $collection->short_description = $request->short_description;
        $collection->start_date = str_replace("/","-",$request->start_date);
        $collection->end_date = str_replace("/","-",$request->end_date);
        $collection->brand_id = $request->brand_id;
        // add image
        $images = [];
        if(!empty($request->id_files)){
            $id_files = explode(",", $request->id_files);
            if(!empty($id_files) && count($id_files)){
                foreach ($id_files as $value) {
                    $url = api_asset($value);
                    if(!empty($url)){
                        $images[] = $url;
                    }

                }
            }
        }
        $collection->images = implode(",", $images);
        $collection->id_images = ltrim($request->id_files,"0,");
        // collection main image

        $images_main = [];
        if(!empty($request->id_main_files)){
            $id_main_files = explode(",", $request->id_main_files);
            if(!empty($id_main_files) && count($id_main_files)){
                foreach ($id_main_files as $value) {
                    $url = api_asset($value);
                    if(!empty($url)){
                        $images_main[] = $url;
                    }

                }
            }
        }

        $collection->main_image = implode(",", $images_main);
        $collection->main_image_id = ltrim($request->id_main_files,"0,");

        $collection->save();
        $collection->products()->syncWithoutDetaching($request->product);
        flash(__('collections.noti_create'))->success();
        return redirect()->route('collections.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        return 123;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $collection = Collection::findOrFail($id);
        if($collection){
            $collection->delete();
            flash(__('collections.msg_delete_collection'))->success();
            return response()->json(true);
        }
        return response()->json(false, 404);
    }

    public function changeStatus(Request $request,$id) {
        $collection = Collection::findOrFail($id);
        $collection->status = $request->status;
        $collection->save();
        flash(__('collections.msg_status_success'))->success();
        return response()->json(true);
    }

    public function information(Request $request,$id)
    {
        $brand = Collection::with('suppliers')->findOrFail($id);
        if($request->ajax()){
            if($brand->suppliers != null) {
                $allproduct = Product::where('name','like','%'.$request->get('search','').'%')
                ->where('supplier_id',$brand->suppliers->id)->whereHas('skus',function($query){
                    $query->where('qty','>','0');
                })->get();

            }else {
                $allproduct = Product::where('name','like','%'.$request->get('search','').'%')->whereHas('skus',function($query){
                    $query->whereNotNull('product_id')->where('qty','>','0');
                })->get();

            }
            $data = Collection::with('products')->findOrFail($id);
            $product_id = $data->products;
            return view('templates.popup.all-products',compact('product_id','allproduct'));
        }
        if($brand->suppliers != null) {
            $allproduct = Product::where('name','like','%'.$request->get('search','').'%')
            ->where('supplier_id',$brand->suppliers->id)->whereHas('skus',function($query){
                $query->where('qty','>','0');
            })->get();

        }else {
            $allproduct = Product::where('name','like','%'.$request->get('search','').'%')->whereHas('skus',function($query){
                $query->whereNotNull('product_id')->where('qty','>','0');
            })->get();

        }
        $brand_name = json_decode($brand->suppliers);
        $data = Collection::with(['products','products.skus'])->findOrFail($id);
        $products = $data->products;
        $images = explode(",", $data->images);
        return view('backend.collections.information', compact('data','images','products','allproduct','brand_name'));
    }

    public function updateInfo(CollectionRequest $request,$id) {

        $collection = Collection::findOrFail($id);
        if($collection->update($request->validated())) {
            return response()->json(['data' => $collection, 'success' => true, 'message' => 'success'], 200);
        }
        return response()->json(['success' => false, 'message' => 'error'], 422);
    }

    public function addProduct(Request $request, $id) {
        $collection = Collection::findOrFail($id);
        $collection->products()->syncWithoutDetaching($request->product);
        $collection->save();
        if($request->product) {
            flash(__('collections.msg_add_product'))->success();
        }
        return back();
    }
    public function deleteProduct(Request $request,$id) {
        $collection = Collection::findOrFail($id);
        $collection->products()->detach($request->delete_product);
        $collection->save();
        flash(__('collections.msg_delete_product'))->success();
        return back();
    }
    public function validateCollection(CollectionRequest $request) {
        return response()->json();
    }

    public function validateImage(ImageRequest $request) {
        return response()->json();
    }
    public function validated(CollectionsRequest $request) {
        return response()->json();
    }
}
