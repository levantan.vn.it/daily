<?php

namespace App\Http\Controllers;

use App\Banner;
use App\Collection;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;
use App\Exports\BannersExport;
use App\Http\Requests\BannerCollectionRequest;
use App\Http\Requests\BannerRequest;
use App\Http\Requests\BannerCreateRequest;
use App\Http\Requests\BannerImagesRequest;
use App\Http\Requests\BannerInfoRequest;
use Illuminate\Support\Str;

class BannerController extends Controller
{
    public $entity3;
    public function __construct()
    {
        $this->entity3 = "Banner";
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $banners = $this->getData($request);
        return view('backend.banners.index',compact('banners'));
    }

    public function export(Request $request)
    {
        $banners = $this->getData($request, 1);
        return Excel::download(new BannersExport($banners), '배너관리.xlsx');
    }

    private function getData($request, $is_export = 0, $is_paginate = 0){
        $banners = Banner::where(function($query) use($request){
            if($request->search){
                $search = $request->search;
                $query->where(function($q) use($search){
                    $q->where(\DB::raw("CONCAT('#',id)"),'LIKE', '%' .$search. '%' );
                });
            }
            if ($request->date) {
                $query->whereDate('start_date',$request->date);
            }
    
            if($request->status){
                $query->Where('status', $request->status);
            }
        })->latest();
        if($is_export){
            $banners = $banners->get();
        }else{
            $banners = $banners->paginate(10);
        }
        return $banners;
    }

    public function detail($id)
    {
        $banners = Banner::with('collection')->findOrFail($id);
        $collections = Collection::get(['id']);
        $data = [
            'banners'=> $banners,
            'collections' => $collections,
        ];
        return view('backend.banners.detail',$data);
    }

    public function updateInfor(BannerRequest $request, $id)
    {
        $banner = Banner::findOrFail($id);
        $banner->update($request->validated());
        flash(__('banners.update-info'))->success(); 
        return redirect()->back();
    }
    public function updateCollection(BannerCollectionRequest $request, $id)
    {
        $banner = Banner::findOrFail($id);
        $banner->update(['collection_id'=> $request->collection_id]);   
        $banner->update($request->validated());

        flash(__('banners.collection-success'))->success(); 
        return redirect()->back();
    }

    public function changeStatus($id) {
        $banners = Banner::findOrFail($id);
        if($banners->status == 1) {
            $banners->status = 2;
            flash(__('banners.change-status-success'))->success();
        } else {
            $banners->status = 1;
            flash(__('banners.change-status-success'))->success();
        }
        $result= $banners->save();
        if($result){
            return response()->json(true,200);
        }
        return response()->json(false,422);
    }

    public function changeStatus1($id){
        $banners = Banner::findOrFail($id);
        $banners->status = 1;
        flash(__('banners.change-status-success'))->success();
        $result=$banners->save();
        if($result){
            return response()->json(true,200);
        }
        return response()->json(false,422);
    }
    
    public function changeStatus2($id){
        $banners = Banner::findOrFail($id);
        $banners->status = 2;
        flash(__('banners.change-status-success'))->success();
        $result=$banners->save();
        if($result){
            return response()->json(true,200);
        }
        return response()->json(false,422);
    }

    public function changeStatus3($id){
        $banners = Banner::findOrFail($id);
        $banners->status = 3;
        flash(__('banners.change-status-success'))->success();
        $result=$banners->save();
        if($result){
            return response()->json(true,200);
        }
        return response()->json(false,422);
    }

    public function changeStatus4($id){
        $banners = Banner::findOrFail($id);
        $banners->status = 4;
        flash(__('banners.change-status-success'))->success();
        $result=$banners->save();
        if($result){
            return response()->json(true,200);
        }
        return response()->json(false,422);
    }

    public function delete($id)
    {

        $banners = Banner::findOrFail($id);
        $result = $banners->delete();
        flash(__('banners.delete-success'))->success();
        if($result){
            return response()->json(true,200);
        }
        return response()->json(false,422);
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create( Request $request)
    {
        
        $collections=Collection::get(['id']);
        $data=[
            'collections'=>$collections,
        ];
        return view('backend.banners.create',$data);
    }

    public function validateBannerInfor(Request $request) {
        $request->validate([
            'start_date'=>'required|date',
            'end_date'  => 'required|date|after:start_date',
            'position'  => 'required',
        ]);
        return response()->json();
    }



    public function validateBannerCollection(Request $request){
        $request->validate([
            'title' => 'required|string|max:255',
            'collection_id' => 'required|exists:collections,id',
        ]);
        return response()->json();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    public function updateImageBanner(BannerImagesRequest $request,$id)
    {
        $banners = Banner::findOrFail($id);
        $images=[];
        if(!empty($request->id_files)){
            $id_files = explode(",",$request->id_files);
            if(!empty($id_files)&& count($id_files)){
                foreach($id_files as $value){
                    $url=api_asset($value);
                    if(!empty($url)){
                        $images[]=$url;
                    }
                }
            }
        };
        $banners->images = implode(",",$images);
        $banners->id_images= ltrim($request->id_files,"0");
        $banners->save();
        flash(__('orders.sample'))->success();
        return back();
    }

    public function store(BannerCreateRequest $request)
    {
        // $images='';
        // $id_files= $request->id_files;
        // if(!empty($request->id_files)){
        //     $images = api_asset($id_files); 
        // }
        // $banner=$request->validated();
        // $banner['images']=$images;
        // $banner['id_images']=$images;

        // Banner::create($banner);
        $banners= new Banner;
        $images=[];
        if(!empty($request->id_files)){
            $id_files = explode(",",$request->id_files);
            if(!empty($id_files)&& count($id_files)){
                foreach($id_files as $value){
                    $url=api_asset($value);
                    if(!empty($url)){
                        $images[]=$url;
                    }
                }
            }
        };
        $banners->images = implode(",",$images);
        $banners->id_images= ltrim($request->id_files,"0");
        $banners->title=$request->title;
        $banners->collection_id=$request->collection_id;
        $banners->start_date=$request->start_date;
        $banners->end_date=$request->end_date;
        $banners->position=$request->position;
        $banners->save();
        flash(__('banners.create-success'))->success(); 
        return redirect()->route('banners.index');
    }

    // public function checkStore(BannerInfoRequest $request)
    // {
    //     return response()->json(true,200);
    // }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
    public function validated(BannerCreateRequest $request) {
        return response()->json();
    }
}
