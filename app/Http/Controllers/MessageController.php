<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\User;
use App\Message;
use App\Notifications\MessageNotification;
use App\Product;
use App\ProductSKU;
use App\TeamMember;
use Auth;
class MessageController extends Controller
{
    //


    public function index()
    {
        return view('backend.message.index');
    }

    /**
     * Get detail user for message
     *
     * @param int $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function detail($id)
    {
        $user          = User::find($id);
        return response()->json($user, 200);
    }

    /**
     * Get list user for message
     *
     * @param int $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function listOfUser(Request $request)
    {
        $user = [];
        $search = $request->get('search');
        if (!empty($search)) {
            $user = User::where('fullname', 'LIKE', "%$search%")
                ->orWhere('email', 'LIKE', "%$search%")
                ->orWhere('type', 'LIKE', "%$search%")
                ->latest()->get();
        } else {
            $user = User::latest()->get();
        }
        return response()->json($user);
    }
    public function sendMessge($id, Request $request)
    {
        $user = User::find($id);
        $type = Message::DEFAULT_TYPE;
        
        $message = [
            'sender_id' => $request->sender_id."",
            'receiver_id' => $id."",
            'product_id' => $request->product_id."",
            'conversation_id' => $request->conversation_id."",
            'message' => $request->message."",
            'notify_enabled' => $request->notify_enabled.""
        ];
        $user->notify(new MessageNotification((object) $message));
        return response()->json($message);
    }

    /**
     *  get the user to use in reactjs
     *
     * @return array
     */
    public function getUser()
    {
        return response()->json(
            ['user' => [
            'id' =>  Auth::user()->id,
            'name' => Auth::user()->name,
            "avatar" => my_asset(Auth::user()->avatar),
        ],
        'userID' => Auth::user()->id
        ], 200);
    }
    public function detailProduct($id) {
        $product = Product::findOrFail($id);
        $image = explode(",",$product->images)[0];
        $product_sku = ProductSKU::where('product_id',$id)->get();
        $price = collect($product_sku)->min('price');
        $price = number_format($price);
        $product->images = my_asset($image);
        return response()->json([
            'product' => $product,
            'product_sku' => $product_sku,
            'price' => $price
        ]);
    }

    public function detailMember($id) {
        $member = TeamMember::find($id);
        $member->avatar = my_asset($member->avatar);
        return response()->json($member);
    }


}
