<?php

namespace App\Http\Controllers;

use App\Exports\TeamMembersExport;
use App\Http\Controllers\Controller;
use App\Http\Requests\ChangePasswordRequest;
use App\Http\Requests\ImageRequest;
use App\Http\Requests\MemberRequest;
use App\Http\Requests\MemberRoleRequest;
use App\Http\Requests\TeamMemmberRequest;
use App\Mail\SendPwMail;
use App\Rules\MatchOldPassword;
use App\TeamMember;
use Illuminate\Http\Request;
use Excel;
use Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Str;

class TeamMembersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index(Request $request)
    {
        $searchBy = null;
        $members = $this->getList($request,$searchBy,1); 
        return view('backend.members.index',compact('members','searchBy'));
    }


    private function getList($request,$searchBy = null,$is_paginate = 0) {
        $members = TeamMember::orderBy('id','desc');
        if($request->has('search')) {
            $searchBy = $request->search;
            $members = $members->where(function($member) use($searchBy){
                $member->where('name','like','%'.$searchBy.'%')
                ->orWhere('id','like','%'.$searchBy.'%')
                ->orWhere('email','like','%'.$searchBy.'%');
            });
        }
        if($request->joined_date) {
            $members = $members->whereDate('created_at',$request->joined_date);
        }
        if($request->status) {
            $select = $request->status;
            $members = $members->where(function($query) use ($select){
                $query->where('status','like','%'.$select.'%');
            });
        }
        if($is_paginate) {
            $members = $members->paginate(10);
        }else {
            $members = $members->get();
        }
        return $members;
    }

    public function detail()
    {
        return view('backend.members.detail');
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $members = TeamMember::first();
        return view('backend.members.create',compact('members'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(TeamMemmberRequest $request)
    {
        // $member = new TeamMember;
        $images = NULL;
        if(!empty($request->id_files)){
            $url = api_asset($request->id_files);
            if(!empty($url)){
                $images = $url;
            }
        }
        $pw = Str::random(8);
        $date_of_issue = $request->date_of_issue;
        $birthday = $request->birthday;
        $newRequest = $request->validated();
        $newRequest['password'] = Hash::make($pw);
        $newRequest['date_of_issue'] = str_replace("/","-",$date_of_issue);
        $newRequest['birthday'] = str_replace("/","-",$birthday);
        $newRequest['avatar'] = $images;
        $newRequest['id_images'] = $request->id_files;
        $newRequest['status'] = 2;
        TeamMember::create($newRequest);
        $data = [
            'password' => $pw,
            'email' => $request->email,
            'name' => $request->name
        ];
        \Mail::to($request->email)->send(new SendPwMail($data));
        flash(__('members.msg_invite_success'))->success();
        return redirect()->route('members.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request,$id)
    {
        $member = TeamMember::findOrFail($id);
        return view('backend.members.detail',compact('member'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function updateInfor(MemberRequest $request, $id)
    {
        $member = TeamMember::findOrFail($id);
        if($member->update($request->validated())) {
            return response()->json(['success' => true, 'message' => 'success'], 200);
        }
        return response()->json(['success' => false, 'message' => 'error'], 422);
    }

    public function updateRole(MemberRoleRequest $request, $id)
    {
        $member = TeamMember::findOrFail($id);
        $member->role_id = $request->role_id;
        $member->created_at = $request->created_at;
        if($member->save()) {
            return response()->json(['success' => true, 'message' => 'success'], 200);
        }
        return response()->json(['success' => false, 'message' => 'error'], 422);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $member = TeamMember::findOrFail($id);
        $member->delete();
        flash(__('members.msg_delete_success'))->success();
        return response()->json('status', 200);
    }

    public function changeStatus(Request $request,$id) {
        $member = TeamMember::findOrFail($id);
        $member->status = $request->status;
        $member->save();
        flash(__("members.msg_status_success"))->success();
        return response()->json('status', 200);
    }
    public function blockMember($id) {
        $member = TeamMember::findOrFail($id);
        if($member->status == 1) {
            $member->status = 3;
        }elseif($member->status == 2) {
            $member->status = 3;
        } else {
            $member->status = 2;
        }
        $member->save();
        flash(__("members.msg_status_success"))->success();
        return response()->json('status', 200);
    }
    public function export(Request $request)
    {

        $members = $this->getList($request, null, 0);
        return Excel::download(new TeamMembersExport($members), '어드민 관리.xlsx');
    }

    public function changePassword(ChangePasswordRequest $request){
        $user = Auth::user();
        if(!Hash::check($request->current_password,$user->password)){
            flash(__('members.pw_not_match'))->error();
            return back();
        }
        $user->password = Hash::make($request->new_password);
        $user->save();
        flash(__('members.msg_pw_success'))->success();
        return back();
    }
    public function validateMemberInfor(MemberRequest $request) {
        $request->validate([
            'name' => 'required|string|max:20',
            'birthday' => 'required|before:today',
            'phone' => 'required|numeric|min:10|regex:/[0-9]{6,20}/',
            'email' => 'required|email|unique:team_members',
            'address' => 'required|string',
            'id_number' => 'numeric',
            'date_of_issue' => 'required',
        ]);
        return response()->json(true,200);
    }
    public function validateMemberRole(MemberRoleRequest $request) {
        return response()->json(true,200);
    }
    public function validateImage(ImageRequest $request) {
        return response()->json();
    }
    public function validated(TeamMemmberRequest $request) {
        return response()->json();
    }
}
