<?php

namespace App\Http\Controllers;

use App\Banner;
use Illuminate\Http\Request;
use App\Http\Controllers\OTPVerificationController;
use App\Http\Controllers\ClubPointController;
use App\Http\Controllers\AffiliateController;
use App\Order;
use App\OrderProduct;
use App\Product;
use App\ProductStock;
use App\Color;
use App\OrderDetail;
use App\CouponUsage;
use App\OtpConfiguration;
use App\User;
use App\BusinessSetting;
use App\DeliveryBook;
use App\Exports\OrderExport;
use App\Http\Requests\OrderInfoEditRequest;
use App\Http\Requests\OrderRequest;
use Maatwebsite\Excel\Facades\Excel;
use Auth;
use Session;
use DB;
use PDF;
use Mail;
use App\Mail\InvoiceEmailManager;
use App\ProductSKU;
use CoreComponentRepository;
use Illuminate\Auth\Events\Failed;
use SebastianBergmann\Environment\Console;

class OrderController extends Controller
{
    public $value;
    public function __construct()
    {
        $this->value = "Banner";
    }
    /**
     * Display a listing of the resource to seller.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $orders = $this->getData($request);

        return view('backend.orders.index', compact('orders'));


    }

    // get data
    private function getData($request, $is_export = 0, $is_paginate = 0)
    {
        $orders = Order::where(function ($query) use ($request) {
            if ($request->search) {
                $search = $request->search;
                $query->where(function ($t) use ($search) {
                    $t->where(\DB::raw("CONCAT('#OD',id)"), 'LIKE', '%' . $search . '%');
                });
            }
            if ($request->created_at) {
                $query->whereDate('created_at', $request->created_at);
            }
            if ($request->status) {
                $query->where('status', $request->status);
            }
        })->latest();

        if ($is_export) {
            $orders = $orders->get();
        } else {
            $orders = $orders->paginate(15);
        }
        return  $orders;
    }

    public function getDataProduct($request)
    {
        $orders = Order::where(function ($query) use ($request){
            if($request->search){
                $search=$request->search;
                $query->where(function ($t) use ($search){
                    $t->where('displayname','LIKE','%', $search.'%');
                });
            }
        });
        return $orders;
    }

    public function toggleStatus($id) {
        $banner = Banner::findOrFail($id);
        $banner->update([
            'status'=> $banner->isActive() ? 2 : 1
        ]);
        flash(__('orders.status-success'))->success();

        return response()->json();
    }

    public function export(Request $request)
    {
        $orders = $this->getData($request, 1, 0);
        return Excel::download(new OrderExport($orders), '주문관리.xlsx');
    }

    public function updateInfoOrder(Request $request,$id){
        $order = Order::findOrFail($id);

        $request->validate([
            'displayname' => 'required|max:200',
            'store_name' => 'required|max:200',
            'phone' => 'required|max:11',
            'email' => 'required|email|max:200',
            'delivery_address' => 'required|max:200',
        ]);
        // return redirect()->route('products.index');
        if ($order) {
            $order->displayname = $request->displayname;
            $order->store_name = $request->store_name;
            $order->phone = $request->phone;
            $order->email = $request->email;
            $order->delivery_address = $request->delivery_address;

            $order->save();
            flash(__('orders.info-success'))->success();
        };
        return redirect()->back();
    }

    public function changeStatusAllCancel($id){
        $order = Order::findOrFail($id);
        $order->status=6;
        $result=$order->save();
        flash(__('orders.status-success'))->success();
        if($result){
            return response()->json(true,200);
        }
        return response()->json(false,422);
    }

    public function updateDelivery(Request $request, $id)
    {
        $order = Order::findOrFail($id);
        $request->validate([
            'delivery_id'=>'required|integer',
            'shipper_name' => 'required|string|max:255',
            'shipper_phone' => 'required|max:11',
            'delivery_date' => 'date'
        ]);
        // return redirect()->route('products.index');
        if ($order) {
            $order->delivery_id= $request->delivery_id;
            $order->shipper_name = $request->shipper_name;
            $order->shipper_phone = $request->shipper_phone;
            $order->delivery_date = $request->delivery_date;
            if ($order->status == 2) {
                $order->status = 3;
            }
            $order->delivery_confirm_date = now();
            $order->save();
            flash(__('orders.delivery-success'))->success();
        }
        return redirect()->back();
    }

    public function validateOrder(Request $request) {
        $request->validate([
            'user_id' => 'required|integer',
            'phone'=> 'required|max:11',
            'displayname'=>'required|max:255',
            'receiver'=>'required|max:255',
            'delivery_address'=> 'required',
        ]);
        return response()->json();
    }

    public function validateProduct(Request $request)
    {
        $request->validate([
            'id.*'=>'required|min:1',
            'id'=>'required|min:1',
        ]);
        return response()->json();
    }

    public function validateDelivery(Request $request){
        $request->validate([
            'delivery_method'=>'required',
            'delivery_fee' => 'required',
        ]);
        return response()->json();
    }

    // public function changestatusAdminConfirm($id){
    //     $order= Order::findOrFail($id);
    //     if($order->confirm_admin==0)
    //     {
    //         $order->confirm_admin=1;
    //     }
    //     $result = $order->update();
    //     // flash('orders.sample')->success();
    //     if($result)
    //     {
    //         return response()->json(true,200);
    //     }
    //     return response()->json(false,422);
    // }
    public function changeStatusOrder($id)
    {
        $order = Order::findOrFail($id);

        if ($order->status == 2) {
            $order->status = 3;
        }
        if ($order->status == 3) {
            $order->status = 4;
        }
        if ($order->status == 6) {
            $order->status = 2;
        }
        $order->delivery_confirm_date = now();
        $result = $order->update();
        flash(__('orders.status-success'))->success();
        // return redirect()->route('products.index');
        if ($result) {
            return response()->json(true, 200);
        }
        return response()->json(false, 422);
    }

    public function deleteOrder($id)
    {
        $order = Order::findOrFail($id);
        $result = $order->delete();
        flash(__('orders.delete-success'))->success();

        if ($result) {
            return response()->json(true, 200);
        } else {
            return response(false, 422);
        }
    }

    public function changeStatusUnpaid($id)
    {
        $order= Order::findOrFail($id);
        $order->status=1;
        $result=$order->update();
        flash(__('orders.status-success'))->success();
        if ($result) {
            return response()->json(true, 200);
        } else {
            return response(false, 422);
        }
    }

    public function changeStatusCancel($id)
    {
        $order=Order::findOrFail($id);
        if($order->status==6)
        {
            $order->status=7;
        }
        $order->delivery_confirm_date = now();
        $result=$order->update();
        flash(__('orders.delete-success'))->success();
        if($result){
            return response()->json(true,200);
        }
        return response()->json(false,422);
    }

    public function changestatusAdminConfirm($id)
    {
        $order=Order::findOrFail($id);
        $order->confirm_admin=1;
        $order->delivery_confirm_date = now();
        $result=$order->update();
        flash(__('orders.status-success'))->success();
        if($result){
            return response()->json(true,200);
        }
        return response()->json(false,422);
    }





    public function detail($id)
    {
        $order = Order::with('products')->findOrFail($id);
        $images = explode(",", $order->images);
        $invoice_images = explode(",",$order->invoice_images);
        $data = [
            'order' => $order,
            'images' => $images,
            'invoice_images' => $invoice_images,
        ];

        return view('backend.orders.detail', $data);
    }



    public function create(Request $request)
    {
        if ($request->ajax()) {
            if ($request->has('user_id')) {
                $user = User::with('deliveryBooks')->find($request->user_id);
                return response()->json($user);
            }
            if ($request->has('phone')){
                $user = User::findOrFail($request->user_id);
                return response()->json($user);
            }
            if ($request->has('receiver')){
                $delivery_book= DeliveryBook::findOrFail($request->user_id);
                return response()->json($delivery_book);
            }
            if($request->has('search')){
                $allproduct = ProductSKU::with('product')->leftJoin('products', 'products.id','product_sku.product_id')
                ->where('products.name','like','%'.$request->get('search','').'%')
                ->whereNotNull('products.id')
                ->get('product_sku.*');
                // $product= Product::where('name','like','%'.$request->get('search','').'%')->get();
                return view('templates.popup.all-product-order',compact('allproduct'));
            }
            $users = User::where('id','like', "%$request->q%")->paginate(10);
            return response()->json($users);
            // return view('templete.popup.order-add-products',compact('allProduct'))->json($users);
        }
        $allproduct = ProductSKU::with('product')->leftJoin('products', 'products.id','product_sku.product_id')
            ->where('products.name','like','%'.$request->get('search','').'%')
            ->whereNotNull('products.id')
            ->get('product_sku.*');
        $order = Order::whereNotNull('delivery_method')->get();
        $users = User::paginate(10);
        $data = [
            'users' => $users,
            'order' => $order,
            'allproduct'=> $allproduct
        ];
        // $order = new Order;
        // $order->user_id = $request->user_id;
        // $order->phone = $request->phone;
        // $order->save();
        // flash('Push notification success')->success();
        // return redirect()->route('orders.index');


        return view('backend.orders.create', $data);
    }
    public function updateImgReason(Request $request,$id)
    {
        $order = Order::findOrFail($id);
        $images = [];
        if(!empty($request->id_files_shipping)){
            $id_files_shipping = explode(",", $request->id_files_shipping);
            if(!empty($id_files_shipping) && count($id_files_shipping)){
                foreach ($id_files_shipping as $value) {
                    $url = api_asset($value);
                    if(!empty($url)){
                        $images[] = $url;
                    }
                }
            }
        }
        
        $order->invoice_images = implode(",", $images);
        $order->invoice_id_images = ltrim($request->id_files_shipping,"0,");

        $file_pdf = [];
        if(!empty($request->id_files_pdf)){
            $id_files_pdf = explode(",", $request->id_files_pdf);
            if(!empty($id_files_pdf) && count($id_files_pdf)){
                foreach ($id_files_pdf as $value) {
                    $url = api_asset($value);
                    if(!empty($url)){
                        $file_pdf[] = $url;
                    }
                }
            }
        }

        $order->upload_file_pdf = implode(",", $file_pdf);
        $order->id_upload_file_pdf = ltrim($request->id_files_pdf,"0,");
        // if($request->hasFile('upload_file_pdf'))
        // {
        //     $pathFile=upload_image($request->upload_file_pdf);
        //     $order->upload_file_pdf=$pathFile;
        // }
        $order->save();
        flash(__('orders.file-pdf-success'))->success();
        return redirect()->back();
    }

    public function updateInfor(Request $request,$id){
        $order = Order::findOrfail($id);
        if($order){
            $order->user_id=$request->user_id;
            $order->phone=$request->phone;
            $order->save();
            flash(__('orders.info-success'))->success();
        }
        return back();
    }
    public function updateReasonCancel(Request $request,$id){
        $order = Order::findOrFail($id);
        $request->validate([
            'reason_cancel'=>'required',
        ]);

        if($order){
            $order->reason_cancel= $request->reason_cancel;
            $order-> status = 6;
            $order->save();
            flash(__('orders.reason-success'))->success();
        }
        return back();
    }
    public function updateDeliveryMethod(Request $request,$id){
        $order = Order::findOrFail($id);
        if($order){
            $order->delivery_method= $request->delivery_method;
            $order->delivery_fee= $request->delivery_fee;
            $order->save();
        }
        return back();
    }

    /**
     * Display a single sale to admin.
     *
     * @return \Illuminate\Http\Response
     */


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(OrderRequest $request)
    {
        $order = new Order;
        $order->user_id= $request->user_id;
        $order->displayname=$request->displayname;
        $order->phone= $request->phone;
        $order->delivery_address= $request->delivery_address;
        $order->receiver=$request->receiver;
        $order->delivery_method= $request->delivery_method;
        $order->delivery_fee= $request->delivery_fee;
        $order->save();
        // foreach($products as $key=>$value){
        //     $products[$key] = [
        //         $value=>[
        //             'qty'=>1,
        //             'variant'
        //         ]
        //     ]
        // }
        $dataOrders = [];
        ProductSKU::whereIn('id', $request->id)->each(function($sku) use (&$dataOrders){
            // $variants = [
            //     'title' =>'',
            //     'value' => unserialize($sku->variant),
            // ] ;
            // dd(unserialize($sku->variant));
            $dataOrders[] = [
                'qty'=>1,
                'variant'=> $sku->variant ?? '',
                'subtotal'=> $sku->price,
                'product_id'=>$sku->product_id,
                'product_sku_id'=>$sku->id,
                // 'order_id'=> $order->id,
            ];
            // dd($dataOrders);
            // OrderProduct::create([
            //     'qty'=>1,
            //     'variant'=> $sku->variant,
            //     'subtotal'=> $sku->price,
            //     'product_id'=>$sku->id,
            //     'order_id'=> $order->id
            // ]);
        });
        // dd('variant'=> serialize($sku->variant));
        // dd($dataOrders);
        $order->OrderProduct()->createMany($dataOrders);
        // $product = Product::with('skus')->find($request->id);
        // foreach( ($product ?? []) as $item){
        //     OrderProduct::create([
        //         'qty'=>1,
        //         'variant'=> 1,
        //         'subtotal'=>$item->minPrice,
        //         'product_id'=>$item->id,
        //         'order_id' => $order->id
        //     ]);
        // }
        // $order->products()->syncWithoutDetaching($request->id);
    
        flash(__('orders.create-success'))->success();
        return redirect()->route('orders.index');
    }

    public function refund(Request $request)
    {
        $order = Order::findOrFail($request->id);
        $trans = $order->transaction()->first();
        if(empty($trans)){
            return response()->json([
                'success' => false,
                'message' => __('orders.no_payment')
            ], 500);
            flash(__('orders.no_payment'))->error();
            return back();
        }
        // Acquire access token
        $curl = curl_init();
        $postData = [
            'imp_key'   => config('iamport.imp_apikey'),
            'imp_secret'    =>  config('iamport.imp_secret')
        ];
        $postData = json_encode($postData);
        curl_setopt_array($curl, array(
            CURLOPT_URL => "https://api.iamport.kr/users/getToken",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => $postData,
            CURLOPT_HTTPHEADER => [
                'Content-Type: application/json'
            ]
        ));
        $getToken = curl_exec($curl);
        $err = curl_error($curl);
        curl_close($curl);

        if ($err) {
            return response()->json([
                'success' => false,
                'message' => __('orders.no_payment')
            ], 500);
            flash(__('orders.no_payment'))->error();
            return back();
        }
        $getToken = json_decode($getToken, true);
        if (empty($getToken['response']['access_token'])) {
            return response()->json([
                'success' => false,
                'message' => __('orders.refund_faild')
            ], 500);
            flash(__('orders.no_payment'))->error();
            return back();
        }
        $access_token = $getToken['response']['access_token'];
        // Query payment information from Iamport with imp_uid
        $curl = curl_init();
        $postData = [
            'reason'   => $order->reason_cancel,
            'imp_uid'    =>  $trans->transaction_id,
            'amount'    =>  $order->total
        ];
        $postData = json_encode($postData);
        curl_setopt_array($curl, array(
            CURLOPT_URL => "https://api.iamport.kr/payments/cancel",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "post",
            CURLOPT_POSTFIELDS => $postData,
            CURLOPT_HTTPHEADER => [
                "Content-Type: application/json",
                'Authorization: ' . $access_token
            ]
        ));
        $getPaymentData = curl_exec($curl);
        $getPaymentData = json_decode($getPaymentData, true);
        $err = curl_error($curl);
        curl_close($curl);
        if ($err) {
            return response()->json([
                'success' => false,
                'message' => __('orders.refund_faild')
            ], 500);
            flash(__('orders.refund_faild'))->error();
            return back();
        }
        if (empty($getPaymentData['response'])) {
            return response()->json([
                'success' => false,
                'message' => $getPaymentData['message'] ?? __('orders.refund_faild')
            ], 500);
            flash($getPaymentData['message'] ?? __('orders.refund_faild'))->error();
            return back();
        }
        $order->status = Order::REFUND;
        $order->save();
        return response()->json([
            'success' => true,
            'message' => __('orders.refund_success')
        ], 200);
        flash(__('orders.refund_success'))->success();
        return back();
    }

    public function validatedOrder(OrderRequest $request ){
        return response()->json();
    }
}
