<?php

namespace App\Http\Controllers\Api;

use App\Http\Resources\ProductDetailResource;
use App\Http\Resources\ProductResource;
use App\Order;
use App\OrderProduct;
use App\Product;
use App\ProductSKU;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Tymon\JWTAuth\Facades\JWTAuth;

class ProductController extends Controller
{
    /**
     * Get list product
     * @param Request $request
     * @return JsonResource
     */
    public function index(Request $request)
    {
        $products = Product::with(['supplier', 'productSku', 'skus' => function ($query) {
            $query->activeInStock();
        }])->when(true, function ($query) use ($request) {
            $query->activeInStock(true);
            if ($request->category_id) {
                $query->whereIn('category_id', is_array($request->category_id) ? $request->category_id : [$request->category_id]);
            }

            if ($request->min_price && $request->min_price >= 0) {
                $query->forSkuMinPrice($request->min_price)->where('product_sku.price', '>=', intval($request->min_price));
            }

            if ($request->max_price && $request->max_price > 0) {
                $query->where('product_sku.price', '<=', intval($request->max_price));
            }

            if ($request->order) {
                $query->addSelect([
                    'product_price' => ProductSKU::select('price')
                        ->whereColumn('product_id', 'products.id')
                        ->where('price', '>=', $request->get('min_price', 0))
                        ->where('product_sku.status', ProductSKU::STATUS_ACTIVE)
                        ->whereNotNull('sku')
                        ->whereNotNull('variant')
                        ->orderBy('price', 'asc')
                        ->take(1)
                ])->orderBy('product_price', preg_match('/(asc|desc)/i', $request->sort) ? $request->sort : 'asc');
            }

            $brand = null;

            if ($request->keyword) {
                $query->where('products.name', 'like', '%' . $request->keyword . '%');
            }

            if ($request->brand && is_array($request->brand)) {
                $brand = array_unique($request->brand);
                $brand = array_filter($brand, function ($b) {
                    return $b;
                });
            }

            if (!empty($brand)) {
                $query->whereIn('products.supplier_id', $request->brand);
            }
        })->latest('products.created_at')
            ->paginate(request('pageSize'));

        return ProductResource::collection($products);
    }

    /**
     * Get Product Detail
     * @param int|string $id
     * @return JsonResource
     */
    public function show($id)
    {
        $product = Product::with(['supplier', 'productSku', 'skus' => function ($query) {
            $query->where('product_sku.status', 1)->whereNotNull('variant');
        }])->where('products.status', 1)->withCount('authLike')->findOrFail($id);

        return ProductDetailResource::make($product);
    }

    /**
     * Like Product
     * @param int $id
     */
    public function likeProductAction($id)
    {
        $product = Product::where('products.status', 1)->findOrFail($id);
        $user = JWTAuth::parseToken()->authenticate();
        if ($user->likeProducts()->where('products.id', $id)->exists()) {
            $user->likeProducts()->detach($product);
            $data = [
                'action' => 'unlike'
            ];
        } else {
            $user->likeProducts()->syncWithoutDetaching($product);
            $data = [
                'action' => 'like'
            ];
        }
        return response()->json([
            'success' => true,
            'status' => 200,
            'data' => $data
        ]);
    }

    /**
     * Get like of user
     */
    public function getLikeProduct()
    {
        $user = JWTAuth::parseToken()->authenticate();
        $products = $user->likeProducts()->with(['supplier', 'productSku', 'skus' => function ($query) {
            $query->activeInStock();
        }])->whereHas('skus', function ($query) {
            $query->select('product_id')->activeInStock();
        })->paginate(request('pageSize'));

        return ProductResource::collection($products);
    }

    /**
     * Get Bought Product
     */
    public function getBoughtProduct()
    {
        $user = JWTAuth::parseToken()->authenticate();
        $products = Product::whereIn(
            'id',
            OrderProduct::select('product_id')
                ->join('orders', 'orders.id', 'order_product.order_id')
                ->where('orders.user_id', $user->id)->where('orders.status', Order::COMPLETED)
        )->with(['supplier', 'productSku', 'skus' => function ($query) {
            $query->activeInStock();
        }])->whereHas('skus', function ($query) {
            $query->select('product_id')->activeInStock();
        })->paginate(request('pageSize'));
        return ProductResource::collection($products);
    }

    /**
     * Amount of Orders are processing
     * @return JsonResponse
     */
    public function countOrdersProcessing()
    {
        $user = JWTAuth::parseToken()->authenticate();
        $count = Order::where('user_id', $user->id)->whereIn('status', Order::LIST_PROCESS)->count();
        return response()->json([
            'success' => true,
            'status' => 200,
            'data' => [
                'total' => $count
            ]
        ], 200);
    }

    public function getRangePrice()
    {
        return response()->json([
            'success' => true,
            'status' => 200,
            'data' => [
                'min' => ProductSKU::activeInStock()->where('status', 1)->min('price'),
                'max' => ProductSKU::activeInStock()->where('status', 1)->max('price')
            ]
        ]);
    }
}
