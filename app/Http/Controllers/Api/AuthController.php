<?php /** @noinspection PhpUndefinedClassInspection */

namespace App\Http\Controllers\Api;

use App\Models\Setting;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;
use App\User;
use App\UserDeviceFcms;
use JWTAuth;
use App\File;
use App\EntityImage;
use Illuminate\Support\Facades\Storage;
use App\Http\Resources\UserCollection;
class AuthController extends Controller
{
    public function signup(Request $request)
    {
        $request->validate([
            'displayname' => 'bail|required|string|max:100',
            'email' => 'bail|required|string|email|unique:users|max:100',
            'password' => 'bail|required|string|min:10|max:20|confirmed',
            'phone' => 'bail|required|string|max:20|unique:users'
        ]);
        try {
            $user = new User([
                'displayname' => $request->displayname,
                'email' => $request->email,
                'password' => bcrypt($request->password),
                'phone' => $request->phone,
                'status'    =>  1,
                'verify_phone'  =>  '1'
            ]);
            $user->save();
            $token = auth()->guard('api')->login($user);
            return $this->loginSuccess($token, $user);
        } catch (Exception $e) {
            return response()->json([
                'message' => 'error'
            ],500);
        }catch(\Illuminate\Database\QueryException $e){
            return response()->json([
                'message' => 'error'
            ],500);
        }

    }

    public function changePassword(Request $request)
    {
        $request->validate([
            'token' => 'bail|required|string',
            'password' => 'bail|required|string|min:10|max:20|confirmed',
            'phone' => 'bail|required|string|max:20'
        ]);
        $user = User::where('phone',$request->phone)->first();
        if(empty($user)){
            return response()->json(['message' => __('auth.unauthorized'), 'user' => null,'error'=>'phone'], 401);
        }
        $tokens = $user->deviceFCMs()->pluck('token')->toArray();
        if(!in_array($request->token, $tokens)){
            if(empty($user)){
                return response()->json(['message' => __('auth.unauthorized'), 'user' => null,'error'=>'token'], 401);
            }
        }
        $user->password = bcrypt($request->password);
        $user->save();
        $token = auth()->guard('api')->login($user);
        return $this->loginSuccess($token, $user);
        return response()->json([
            'message' => __('auth.change_password_success'),
            'data'  =>  $user
        ], 200);
    }

    public function checkPhone(Request $request)
    {
        $user = User::where('phone',$request->phone)->first();
        if(empty($user)){
            return response()->json(['message' => __('auth.phone_not_register'), 'data' => 'not_register'], 200);
        }
        return response()->json([
            'message' => __('auth.phone_register'),
            'data'  =>  'register'
        ], 200);
    }
    public function checkSNS(Request $request)
    {
        $request->validate([
            'id' => 'bail|required',
            'social' => 'bail|required|in:kakao,naver',
        ]);
        $sns_id_field = $request->social.'_id';
        $user = User::where($sns_id_field,$request->id)->first();
        if(empty($user)){
            return response()->json(['message' => __('auth.sns_not_register'), 'data' => 'not_register'], 200);
        }
        return response()->json([
            'message' => __('auth.sns_register'),
            'data'  =>  'register'
        ], 200);
    }


    public function checkEmail(Request $request)
    {
        $user = User::where('email',$request->email)->first();
        if(empty($user)){
            return response()->json(['message' => __('auth.email_not_register'), 'data' => 'not_register'], 200);
        }
        return response()->json([
            'message' => __('auth.email_register'),
            'data'  =>  'register'
        ], 200);
    }

    public function login(Request $request)
    {
        $request->validate([
            'email' => 'bail|required|string|email|max:100',
            'password' => 'bail|required|string|min:6|max:20',
        ]);
        $credentials = request(['email', 'password']);
        $token = auth()->guard('api')->attempt($credentials);
        if (!$token)
            return response()->json(['message' => __('auth.unauthorized'), 'user' => null], 401);
        $user = auth()->guard('api')->user();
        if(empty($user->verify_phone)){
            return response()->json(['message' => __('auth.verify_account'), 'user' => null,'error'=>'verify_phone'], 401);
        }
        if($user->status != 1){
            return response()->json(['message' => __('auth.user_deactive'), 'user' => null,'error'=>'user_deactive'], 401);
        }
        return $this->loginSuccess($token, $user);
    }

    public function logout(Request $request)
    {
        auth()->guard('api')->logout();
        return response()->json([
            'message' => __('auth.success_logout')
        ]);
    }

    public function loginSNS(Request $request)
    {
        $request->validate([
            'id' => 'bail|required',
            'social' => 'bail|required|in:kakao,naver',
        ]);
        $sns_id_field = $request->social.'_id';
        $user = User::where($sns_id_field,$request->id)->first();
        if(empty($user)){
            $request->validate([
                'phone' => 'required|string|max:20|unique:users'
            ]);
            $user = new User([
                'phone' => $request->phone,
                'status'    =>  1,
                'verify_phone'  =>  1,
                $sns_id_field => $request->id
            ]);
            $user->save();
        }
        $token = auth()->guard('api')->login($user);
        return $this->loginSuccess($token, $user);
    }

    protected function loginSuccess($token, $user)
    {

        return response()->json([
            'token' => $token,
            'token_type' => 'Bearer',
            'user' => new UserCollection($user)
        ]);
    }

    public function createBusiness(Request $request){
        $request->validate([
            'business_name' => 'bail|required|max:100',
            'tax_invoice' => 'bail|required|max:100',
            'representative' => 'bail|required|max:100',
            'store_name' => 'bail|required|max:100',
            'store_address' => 'bail|required|max:255',
            'post_code' => 'bail|nullable|max:100',
            'avatar' => 'bail|nullable|image|file|max:20480',
            'store_detailed_address' => 'bail|nullable|max:255',
        ]);
        $user = JWTAuth::parseToken()->authenticate();
        try {
            $user->business_name = $request->business_name;
            $user->tax_invoice = $request->tax_invoice;
            $user->representative = $request->representative;
            $user->store_name = $request->store_name;
            $user->store_address = $request->store_address;
            $user->post_code = $request->post_code;
            $user->store_detailed_address = $request->store_detailed_address;
            if($request->hasFile('avatar')){
                if(!empty($user->avatar)){
                    Storage::delete($user->avatar);
                }

                $user->avatar = $request->file('avatar')->store('uploads/avatar');
            }
            $documents = [];
            $id_files = [];
            if($request->hasFile('business_document')){
                $type = array(
                    "jpg"=>"image",
                    "jpeg"=>"image",
                    "png"=>"image",
                    "svg"=>"image",
                    "webp"=>"image",
                    "gif"=>"image",
                    "mp4"=>"video",
                    "mpg"=>"video",
                    "mpeg"=>"video",
                    "webm"=>"video",
                    "ogg"=>"video",
                    "avi"=>"video",
                    "mov"=>"video",
                    "flv"=>"video",
                    "swf"=>"video",
                    "mkv"=>"video",
                    "wmv"=>"video",
                    "wma"=>"audio",
                    "aac"=>"audio",
                    "wav"=>"audio",
                    "mp3"=>"audio",
                    "zip"=>"archive",
                    "rar"=>"archive",
                    "7z"=>"archive",
                    "doc"=>"document",
                    "txt"=>"document",
                    "docx"=>"document",
                    "pdf"=>"document",
                    "csv"=>"document",
                    "xml"=>"document",
                    "ods"=>"document",
                    "xlr"=>"document",
                    "xls"=>"document",
                    "xlsx"=>"document"
                );
                $files = $request->file('business_document');
                foreach ($files as $file) {
                    $upload = new File;
                    $upload->file_original_name = null;
                    $arr = explode('.', $file->getClientOriginalName());

                    for($i=0; $i < count($arr)-1; $i++){
                        if($i == 0){
                            $upload->file_original_name .= $arr[$i];
                        }
                        else{
                            $upload->file_original_name .= ".".$arr[$i];
                        }
                    }
                    $upload->file_name = $file->store('uploads/business_document');
                    $upload->member_id = $user->id;
                    $upload->user_id = $user->id;
                    $upload->extension = strtolower($file->getClientOriginalExtension());
                    if(isset($type[$upload->extension])){
                        $upload->type = $type[$upload->extension];
                    }
                    else{
                        $upload->type = "others";
                    }
                    $upload->file_size = $file->getSize();
                    $upload->save();
                    $documents[] = $upload->file_name;
                    $id_files[] = $upload->id;

                }
            }
            if (!empty($documents)) {
                $documents = implode(",", $documents);
                $user->document = $documents;
                $document_id = implode(",", $id_files);
                $user->document_id = $document_id;
            }
            $user->save();
            $user->deliveryBooks()->create([
                'location_name' =>  $request->store_name,
                'address'   =>  $request->store_address,
                'receiver'  =>  $request->business_name,
                'phone' =>  $user->phone,
                'post_code'   =>  $request->post_code,
                'detailed_address'   =>  $request->store_detailed_address,
                'type'  =>  1
            ]);
            return response()->json([
                'message' => 'OK',
                'data'  =>  new UserCollection($user)
            ]);
        } catch (Exception $e) {
            return response()->json([
                'message' => 'error'
            ],500);
        }catch(\Illuminate\Database\QueryException $e){
            return response()->json([
                'message' => 'error'
            ],500);
        }
    }


}
