<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\TeamMember;
use App\User;
use App\Order;
use App\Transaction as transactions;
use Excel;
use App\Exports\UserExport;
use App\Transaction;
use ImageOptimizer;
use Auth;
use App\File as FileModel;
use App\EntityImage;
use App\Http\Requests\UserRequest;
use App\Http\Requests\UserRequestBusiness;
class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index(Request $request)
    {
        $sort_search = null;
        $customers = $this->getData($request,$sort_search, 1);
        return view('backend.users.index', compact('customers','sort_search'));
    }

    private function getData($request,$sort_search = null, $is_paginate = 0){
        $customers = User::orderBy('id', 'desc');
        if ($request->has('search')){
            $sort_search = $request->search;
            $customers = $customers->where(function($user) use ($sort_search){
                $user
                ->Where(\DB::raw("CONCAT('#',id)"), 'like', '%'.$sort_search.'%')
                ->orwhere('displayname', 'like', '%'.$sort_search.'%')
                ->orWhere(\DB::raw("CONCAT('+82',phone)"),'like', '%'.$sort_search.'%')
                ->orWhere('email', 'like', '%'.$sort_search.'%');
            });
        }
        if($request->joined_date){
            $customers = $customers->whereDate('created_at', $request->joined_date);
        }
        if( $request->status ){
            $customers = $customers->Where('status', $request->status);
        }
        if($is_paginate){
            $customers = $customers->paginate();
        }else{
            $customers = $customers->get();
        }
        return $customers;
    }

    public function changeStatus($id, Request $request)
    {
        $customer = User::findOrFail($id);
        $customer->status = $request['status'];
        flash(__('users.messenger_status'))->success();
        $customer->save();
        return back();
    }

    // public function changeStatus($id)
    // {
    //     $customer = User::findOrFail($id);
    //     $customer->update([
    //         'status'=>$customer->isActive() ? 2 : 1
    //     ]);
    //       flash(__('suppliers.noti_status'))->success();
    //     return view('backend.users.index',with('id',$id));
    // }

    public function changeStatus2($id, Request $request)
    {
        $customer = User::find($id);
        $customer->status = $request['status'];
        flash(__('users.messenger_status'))->success();
        $customer->save();
        return redirect()->route('users.detail',$id);
    }


    public function updateimg(UserRequestBusiness $request, $id){
        $customer = User::findOrFail($id);
        $user = Auth::user();
        $customer->business_name = $request->business_name;
        $customer->tax_invoice = $request->tax_invoice;
        $customer->representative = $request->representative;
        $customer->store_name = $request->store_name;
        $customer->store_address = $request->store_address;
        $documents = [];
        if(!empty($request->id_files)){
            $id_files = explode(",", $request->id_files);
            if(!empty($id_files) && count($id_files)){
                foreach ($id_files as $value) {
                    $url = api_asset($value);
                    if(!empty($url)){
                        $documents[] = $url;
                    }
                }
            }
        }
        $customer->document = implode(",", $documents);
        $customer->document_id = ltrim($request->id_files,"1,");
        // $user= User::where('id', $id)->update($request->validated());
        flash(__('users.messenger_business'))->success();
        $customer->save();
        return redirect()->route('users.detail',$id);
    }
    
    // public function index(Request $request)
    // {
    //     $sort_search = null;
    //     $customers = User::orderBy('created_at', 'desc');
    //     if ($request->has('search')){
    //         $sort_search = $request->search;
    //         $customers = $customers->where(function($user) use ($sort_search){
    //             $user->where('name', 'like', '%'.$sort_search.'%')->orWhere('email', 'like', '%'.$sort_search.'%');
    //         });
    //     }
    //     $customers = $customers->paginate(15);
    //     return view('backend.users.index', compact('customers','sort_search'));
    // }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
    //     $customers = User::find($id);
    //     return view('backend.users.detail')->with(compact('customers'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UserRequest $request, $id)
    {

        $data =$request->validated();
        $user= User::where('id', $id)->update($data);
        flash(__('users.messenger_information'))->success();
        return redirect()->route('users.detail',$id);
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $customers = User::findOrFail($id);
        $customers->delete();
        flash(__('users.messenger_del_user'))->success();
        return response()->json('status', 200);
    }


    public function ban($id) {
        $customer = User::findOrFail($id);

        if($customer->status == 2) {
            $customer->status = 1;
            flash(translate('Customer UnBanned Successfully'))->success();
        } else {
            $customer->status = 2;
            flash(translate('Customer Banned Successfully'))->success();
        }
        $customer->user->save();
        return back();
    }

    public function transactions(Request $request){
        $data = transactions::where(function ($query) use ($request) {
            if ($request->search) {
                $query->where(function ($q) use ($request) {
                    $q->where(\DB::raw("CONCAT('#',id)"), 'like', '%'.$request->search.'%')
                    ->orWhere(\DB::raw("CONCAT('#',order_id)"), 'like', '%'.$request->search.'%');
                });
            }
        });
        $data = $data->paginate(15);
        return view('backend.users.transactions', compact('data'));
    }

    public function detail($id){
        $customers = User::findOrFail($id);
        $documents =  $customers->document?explode(",", $customers->document):null;
        $orders = Order::where('user_id', $customers->id)->with( ['products', 'products.skus'])->get();
        return view('backend.users.detail',[
            'customers' => $customers,
            'documents' =>  $documents,
            'orders' => $orders
        ]);
        // return view('backend.users.detail');
    }

    public function export(Request $request)
    {
        $customers = $this->getData($request, null, 0);
        return Excel::download(new UserExport($customers), '회원관리.xlsx');
    }

   

}
