<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Session;
use Auth;
use Hash;
use App\Category;
use App\Product;
use App\User;
use App\Order;
use App\Setting;
use App\Transaction;
use Carbon\Carbon;
use ImageOptimizer;
use Cookie;
use Illuminate\Support\Str;
use Mail;

class HomeController extends Controller
{
    public $time;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->time = 0;
        //$this->middleware('auth');
    }

    /**
     * Show the admin dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function admin_dashboard()
    {
        $myipd = get_client_ip();
        $url = 'http://www.geoplugin.net/json.gp?ip='.$myipd;
        $details    =   ip_details($url);
        $v = json_decode($details);
        if(empty($v->geoplugin_countryCode) || $v->geoplugin_countryCode == 'VN'){
            date_default_timezone_set('Asia/Ho_Chi_Minh');
            $this->time = 7;
        }else{
            date_default_timezone_set($v->geoplugin_timezone);
            $this->time = 9;
        }

        $top_products = Product::where('status','1')->orderBy('total_sale','desc')->take(5)->get();
        $top_orders = Order::where('status','5')->orderBy('total','desc')->take(5)->get();
        $top_trans = Transaction::where('status','1')->orderBy('money','desc')->take(5)->get();
        $totalOrder = Order::where('status','5')->count();
        $totalRevenue = Order::where('status','5')->sum('total');

        $dataOrder = Order::where('status','5')->whereDate('created_at',"<=",Date('Y-m-d'))->whereDate('created_at',">=",Date('Y-m-d',strtotime('-11 months')))->get() ?? [];
        
        //Order base on month
        $dataOrderMonth =$this->getMonth($dataOrder,true);
        $labelsMonth = array_keys($dataOrderMonth);
        $dataOrderMonth = array_values($dataOrderMonth);
        $orderMonthTotal = array_sum($dataOrderMonth);

        //Revenue base on month
        $dataRevenueMonth =$this->getMonth($dataOrder,false);
        $dataRevenueMonth = array_values($dataRevenueMonth);
        $revenueMonthTotal = array_sum($dataRevenueMonth);

        //Order base on week
        $dataOrderWeek = $this->getDate($dataOrder,true);
        $labelsWeek = array_keys($dataOrderWeek);
        $dataOrderWeek = array_values($dataOrderWeek);
        $orderWeekToTal = array_sum($dataOrderWeek);

        //Revenue base on week
        $dataRevenueWeek =$this->getDate($dataOrder,false);
        $dataRevenueWeek = array_values($dataRevenueWeek);
        $revenueWeekTotal = array_sum($dataRevenueWeek);

        //Order base on hour
        $dataOrderHour = $this->getHour($dataOrder,true);
        $labelsHour = array_keys($dataOrderHour);
        $dataOrderHour = array_values($dataOrderHour);
        $orderHourTotal = array_sum($dataOrderHour);

        //Revenue base on hour
        $dataRevenueHour =$this->getHour($dataOrder,false);
        $dataRevenueHour = array_values($dataRevenueHour);
        $revenueHourTotal = array_sum($dataRevenueHour);


        $dataOrderWeek = json_encode($dataOrderWeek);
        $labelsWeek = json_encode($labelsWeek);
        $dataOrderMonth = json_encode($dataOrderMonth);
        $labelsMonth = json_encode($labelsMonth);
        $dataOrderHour = json_encode($dataOrderHour);
        $labelsHour = json_encode($labelsHour);

        $dataRevenueHour = json_encode($dataRevenueHour);
        $dataRevenueWeek = json_encode($dataRevenueWeek);
        $dataRevenueMonth = json_encode($dataRevenueMonth);
        
        return view('backend.dashboard',compact('top_products','top_orders','top_trans',
        'totalOrder','totalRevenue','dataOrderWeek','labelsWeek','dataOrderMonth',
        'labelsMonth','labelsHour','dataOrderHour','dataRevenueWeek','dataRevenueHour','dataRevenueMonth',
        'orderMonthTotal','revenueMonthTotal','orderWeekToTal','revenueWeekTotal','revenueHourTotal','orderHourTotal'));
    }

    private function getHour($data,$type) {

        $start_time = Date('Y-m-d ' . '00:00:00');
        $end_time = Date('Y-m-d H:i:s', strtotime($start_time. ' +2 hours'));

        $time_before = Date('Y-m-d H:i:s',strtotime($start_time. ' -'.$this->time.' hours'));
        $end_before = Date('Y-m-d H:i:s', strtotime($end_time. ' -'.$this->time.' hours'));
        // dd($start_time,$end_time);
        $dataHour = [];
        for($i = 0; $i<= 23 ; $i+=2 ) {
            if($i){
                $start_time = $end_time;
                $end_time = Date('Y-m-d H:i:s', strtotime($end_time . ' +2 hours'));

                $time_before = $end_before;
                $end_before = Date('Y-m-d H:i:s', strtotime($end_before. ' +2 hours'));
            } 
            if($type) {
                $x = $data->whereBetween('created_at', [$time_before, $end_before])->count();
            }else {
                $x = $data->whereBetween('created_at', [$time_before, $end_before])->sum('total');
            }
            
            $dataHour[Date('H:i',strtotime($start_time)).'-'.Date('H:i A',strtotime($end_time))] = $x;
        }
        return $dataHour;
    }

    private function getDate($data,$type) {        
        $start_time = Date('Y-m-d ' . '00:00:00',strtotime(' -6 days'));
        $end_time = Date('Y-m-d H:i:s', strtotime($start_time. ' +1 days'));

        $time_before = Date('Y-m-d H:i:s',strtotime($start_time.' -'.$this->time.' hours'));
        $end_before = Date('Y-m-d H:i:s', strtotime($end_time. ' -'.$this->time.' hours'));

        $dataWeek = [];
        for($i = 0; $i<= 6 ; $i+=1 ) {
            if($i){
                $start_time = $end_time;
                $end_time = Date('Y-m-d H:i:s', strtotime($end_time . ' +1 days'));

                $time_before = $end_before;
                $end_before = Date('Y-m-d H:i:s', strtotime($end_before. ' +1 days'));
            } 
            if($type) {
                $x = $data->whereBetween('created_at', [$time_before, $end_before])->count();
            } else {
                $x = $data->whereBetween('created_at', [$time_before, $end_before])->sum('total');
            }
            
            $dataWeek[Date('Y-m-d',strtotime($start_time.' +'.$this->time.' hours'))] = $x;
        }
        return $dataWeek;
    }

    private function getMonth($data,$type) { 
        $start_time = Date('Y-m-01' . ' 00:00:00',strtotime(' -11 months'));
        $end_time = Date('Y-m-d H:i:s', strtotime($start_time. ' +1 months'));
        $time_before = Date('Y-m-d H:i:s',strtotime($start_time.' -'.$this->time.' hours'));
        $end_before = Date('Y-m-d H:i:s', strtotime($end_time. ' -'.$this->time.' hours'));
        $dataMonth = [];
        for($i = 0; $i<= 11 ; $i+=1 ) {
            if($i){
                $start_time = $end_time;
                $end_time = Date('Y-m-d H:i:s', strtotime($end_time . ' +1 month'));

                $time_before = $end_before;
                $end_before = Date('Y-m-d H:i:s', strtotime($end_before. ' +1 month'));
            }        
            if($type) {
                $x = $data->whereBetween('created_at', [$time_before, $end_before])->count();
            }else {
                $x = $data->whereBetween('created_at', [$time_before, $end_before])->sum('total');
            }
            
            $dataMonth[Date('Y-m',strtotime($start_time))] = $x;
        }
        return $dataMonth;
    }


}
