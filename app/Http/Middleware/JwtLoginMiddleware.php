<?php

namespace App\Http\Middleware;

 use Closure;
use JWTAuth;
use Exception;
use Tymon\JWTAuth\Http\Middleware\BaseMiddleware;

class JwtLoginMiddleware extends BaseMiddleware
{

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        try {
            $user = JWTAuth::parseToken()->authenticate();
            if(empty($user)){
                if($request->expectsJson()){
                    return response()->json(['message' => __('auth.token_invalid')],401);
                }
                return abort(401, __('auth.unauthorized'));
            }
        } catch (Exception $e) {
            if($request->expectsJson()){
                if ($e instanceof \Tymon\JWTAuth\Exceptions\TokenInvalidException){
                    return response()->json(['message' => __('auth.token_invalid')],401);
                }else if ($e instanceof \Tymon\JWTAuth\Exceptions\TokenExpiredException){
                    return response()->json(['message' => __('auth.token_expired')],401);
                }else{
                    return response()->json(['message' => __('auth.token_not_found')],401);
                }
            }
            return abort(401, __('auth.unauthorized'));
        }
        return $next($request);
    }

}

