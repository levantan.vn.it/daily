<?php

namespace App\Http\Middleware;

use Closure;
use Auth;

class IsAdmin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(\Auth::check()) {
            if(\Auth::user()->status == 3) {
                \Auth::logout();
                flash( __('members.login_msg_block'))->error();
                return redirect()->to('/login');
            }
            return $next($request);
        } 
        else{
            abort(404);
        }
    }
}
