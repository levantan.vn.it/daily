<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class DistanceRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'distance.*.delivery_from'=> 'required|integer',
            'distance.*.delivery_to'=> 'required|integer',
            'distance.*.price'=> 'required|integer'
        ];
    }

    public function messages()
    {
        return [
            'required' => '이 항목은 필수입력 입니다.',
        ];
    }
}
