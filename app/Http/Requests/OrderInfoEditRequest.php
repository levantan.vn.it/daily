<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class OrderInfoEditRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'displayname' => 'required',
            'user_id' => 'required',
            'phone' => 'required',
            'email' => 'required',
            'delivery_address' => 'required',
            'note' => 'required',
        ];
    }

    public function messages(){
        return [
            'end_date.after' => '이름 필드는 필수입니다.',
            'start_date.required' => '시작일 필드는 필수입니다.',
            'end_date.required' => '종료일 필드는 필수입니다.',
            'position.required' => '위치 필드는 필수입니다.',
        ];
    }
}
