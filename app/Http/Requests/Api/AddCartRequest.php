<?php

namespace App\Http\Requests\Api;

use App\ProductSKU;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class AddCartRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'product_id' => ['bail', 'required', Rule::exists('products', 'id')->whereNull('deleted_at')],
            'product_sku_id' => [
                'bail',
                'required',
                Rule::exists('product_sku', 'id')
                    ->where('product_id', $this->product_id)
                    ->where('product_sku.status', ProductSKU::STATUS_ACTIVE)
                    ->whereNotNull('sku')
                    ->whereNull('deleted_at')
                    ->whereNotNull('variant')

            ],
            'qty' => 'bail|required|integer|min:1|max:4294967295',
            'delivery_condition_id' =>  'bail|nullable'
        ];
    }

    public function messages()
    {
        return [
            'product_id.exists' => __('api_not_found.Product'),
            'product_sku_id.exists' => __('api_not_found.ProductSKU')
        ];
    }
}
