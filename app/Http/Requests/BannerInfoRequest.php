<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class BannerInfoRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'end_date' => '종료일',
            'start_date' => '시작일',
            'title' => '표제',
        ];
    }

    public function attributes()
    {
        return [
            'end_date' => '종료일',
            'start_date' => '시작일',
            'title' => '표제',
        ];
    }
}
