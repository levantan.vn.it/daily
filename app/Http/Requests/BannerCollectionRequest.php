<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class BannerCollectionRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => 'required|string|max:255',
            'collection_id' => 'required|exists:collections,id',
        ];
    }
    public function messages(){
        return [
            'title.required' => '이름 필드는 필수입니다.',
            'collection_id.required' => '컬렉션 ID 필드는 필수입니다.'
        ];
    }
}
