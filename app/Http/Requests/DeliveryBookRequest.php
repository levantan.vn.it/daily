<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class DeliveryBookRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'location_name' => 'bail|required|string|max:100',
            'address' => 'bail|required|string|max:255',
            'detailed_address' => 'bail|nullable|string|max:255',
            'receiver' => 'bail|required|string|max:100',
            'post_code'=>'bail|nullable',
            'phone' => 'bail|required|regex:/\(?([0-9]{3})\)?([ .-]?)([0-9]{3})\2([0-9]{3,})/|max:20',
            'type' => 'bail|required|in:0,1',
        ];
    }
}
