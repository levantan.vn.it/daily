<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class DistanceFixedAreaRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'area.*.district_id'=> 'required|array',
            'area.*.price'=> 'required|integer'
        ];
    }

    public function messages()
    {
        return [
            'required' => '이 필드는 비워 둘 수 없습니다.',
        ];
    }
}
