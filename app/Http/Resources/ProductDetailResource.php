<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class ProductDetailResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return is_null($this->resource) ? [] : [
            'id' => $this->id,
            'name' => $this->name,
            'brand_name'    => optional($this->supplier)->name,
            'brand_id'    => $this->supplier_id,
            'description' => $this->description,
            'photo' => get_asset_from_list($this->images),
            'variant' => !empty($this->variant) ? unserialize($this->variant) : [],
            'sku' => ProductSkuResource::collection($this->skus),
            'liked' => boolval($this->auth_like_count ?? false),
            'total_sale'    => $this->total_sale,
            'deleted_at' => $this->deleted_at
        ];
    }

    public function __construct($resource)
    {
        $this->resource = $resource;
        $this->additional([
            'success' => true,
            'status' => 200
        ]);
    }
}
