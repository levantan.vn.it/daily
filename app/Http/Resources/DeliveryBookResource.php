<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class DeliveryBookResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return is_null($this->resource) ? null : [
            'id' => $this->id,
            'location_name' => $this->location_name,
            'address' => $this->address,
            'detailed_address'    =>  $this->detailed_address,
            'receiver' => $this->receiver,
            'phone' => $this->phone,
            'type' => $this->type,
            'post_code' => $this->post_code,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at
        ];
    }

    public function __construct($resource)
    {
        $this->resource = $resource;
        $this->additional([
            'success' => true,
            'status' => 200
        ]);
    }

    public static function collection($resource)
    {
        return parent::collection($resource)->additional([
            'success' => true,
            'status' => 200
        ]);
    }
}
