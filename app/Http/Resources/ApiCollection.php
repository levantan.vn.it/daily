<?php

namespace App\Http\Resources;

trait ApiCollection
{
    public function with($request)
    {
        return [
            'success' => true,
            'status' => 200
        ];
    }
}
