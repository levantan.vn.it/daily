<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class File extends Model
{
    protected $table = 'files';
    protected $appends = ['file_url'];
    /**
    * The attributes that are mass assignable.
    *
    * @var array
    */
    protected $fillable = [
        'file_original_name', 'file_name', 'user_id', 'extension', 'type', 'file_size','member_id'
    ];

    public function member()
    {
    	return $this->belongsTo(TeamMember::class);
    }

    public function getFileUrlAttribute()
    {
        return my_asset($this->file_name);
    }
}
