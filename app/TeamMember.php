<?php

namespace App;
use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User as Authenticatable;
use App\Notifications\EmailVerificationNotification;

class TeamMember extends Authenticatable
{
    use Notifiable;
    protected $table = 'team_members';
    protected $fillable = [
        'name',
        'email',
        'phone',
        'birthday',
        'password',
        'avatar',
        'address',
        'status',
        'id_number',
        'date_of_issue',
        'role_id',
        'id_images'
    ];

    public function role() {
        return $this->belongsTo(Role::class);
    }
    public function statusText() {
        if($this->status == 1) {
            return __('members.active');
        }elseif($this->status == 2) {
            return __('members.inactive');
        }else{
            return __('members.blocked');
        }
        return "";
    }
    public function checkStatus() {
        if($this->status == 1) {
            return '3';
        }elseif($this->status == 2) {
            return '3';
        }else{
                return '1';
        }
        return "";
        
    }
    public function allRoles() {
        $roles = Role::all();
        if($roles) {
            $rs = json_decode($roles);
        }

        return $rs;
    }
}
