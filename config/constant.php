<?php

return [

    "QUICHEF" => "Fine Table",
    "CONST_MAILADDR_ADMIN" => env('CONST_MAILADDR_ADMIN', null),

    /**
     * set admin id and admin name to firebase
     */
    'ID_ADMIN_FIREBASE' => 1,
    'NAME_ADMIN_FIREBASE' => 'ADMIN',
    "AVATAR_ADMIN_FIREBASE" => "img/logo.png",


    // Notifications
    'ADMIN_ACTION' => 'ADMIN_ACTION',
    'NEW_MESSAGE' => 'NEW_MESSAGE',
    'ORDER_UNPAID' => 'ORDER_UNPAID',
    'ORDER_PAID' => 'ORDER_PAID',
    'ORDER_DELIVERING' => 'ORDER_DELIVERING',
    'ORDER_ARRIVED' => 'ORDER_ARRIVED',
    'ORDER_CANCELED' => 'ORDER_CANCELED',
    'ORDER_REFUND'  =>  'ORDER_REFUND'
];
